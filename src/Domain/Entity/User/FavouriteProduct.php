<?php

namespace App\Domain\Entity\User;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class FavouriteProduct extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Product")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User\User", inversedBy="favouriteProducts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?User $user = null;

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }
}