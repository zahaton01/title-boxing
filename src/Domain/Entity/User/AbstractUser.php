<?php

namespace App\Domain\Entity\User;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\CreationDateTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 * @ORM\Table("users")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap(value={
 *     App\Domain\Entity\User\User::TYPE  = "App\Domain\Entity\User\User",
 *     App\Domain\Entity\User\Admin::TYPE = "App\Domain\Entity\User\Admin"
 * })
 */
abstract class AbstractUser extends AbstractEntity implements UserInterface
{
    use CreationDateTrait;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected string $login = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $password = '';

    /**
     * @ORM\Column(type="json")
     */
    protected array $roles = [];

    public function __construct()
    {
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getRoles()
    {
        return array_unique($this->roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function addRole(string $role)
    {
        $this->roles[] = $role;
        $this->roles   = array_unique($this->roles);

        return $this;
    }

    public function getUsername()
    {
        return (string) $this->login;
    }

    public function getPassword()
    {
        return (string) $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getSalt()
    {

    }

    public function eraseCredentials()
    {

    }
}
