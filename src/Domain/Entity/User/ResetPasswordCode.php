<?php

namespace App\Domain\Entity\User;

use App\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ResetPasswordCode extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User\User", inversedBy="resetPasswordCodes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private User $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $code;

    public function __construct(User $user, string $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }
}