<?php

namespace App\Domain\Entity\User;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Anton Zakharuk <zahaton01@gmail.com>
 *
 * Research & Development
 *
 * @ORM\Entity()
 * @UniqueEntity(fields={"email"})
 */
class User extends AbstractUser
{
    public const TYPE = 0;

    public const GENDER_MALE = 0;
    public const GENDER_FEMALE = 1;
    public const GENDER_UNKNOWN = 2;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $surname = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeInterface $birthday = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $gender;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $email = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $phone = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $city = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $address = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\User\FavouriteProduct", mappedBy="user", cascade="all")
     */
    private Collection $favouriteProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Commerce\Order", mappedBy="user")
     * @ORM\OrderBy({"creationDate": "DESC"})
     */
    private Collection $orders;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": 1})
     */
    private bool $isEmailConfirmed = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\User\ConfirmEmailAddressCode", mappedBy="user", cascade="all")
     */
    private Collection $confirmEmailCodes;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\User\ResetPasswordCode", mappedBy="user", cascade="all")
     */
    private Collection $resetPasswordCodes;

    public function __construct()
    {
        parent::__construct();
        $this->gender = self::GENDER_UNKNOWN;
        $this->favouriteProducts = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->confirmEmailCodes = new ArrayCollection();
        $this->resetPasswordCodes = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): void
    {
        $this->birthday = $birthday;
    }

    public function getGender(): int
    {
        return $this->gender;
    }

    public function setGender(int $gender): void
    {
        $this->gender = $gender;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    public function getFavouriteProducts(): Collection
    {
        return $this->favouriteProducts;
    }

    public function getOrders()
    {
        return $this->orders;
    }

    public function isEmailConfirmed(): bool
    {
        return $this->isEmailConfirmed;
    }

    public function setIsEmailConfirmed(bool $isEmailConfirmed): void
    {
        $this->isEmailConfirmed = $isEmailConfirmed;
    }

    public function getConfirmEmailCodes()
    {
        return $this->confirmEmailCodes;
    }

    public function getResetPasswordCodes()
    {
        return $this->resetPasswordCodes;
    }
}
