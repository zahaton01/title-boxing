<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait MetaTrait
{
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $pageTitle = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $metaTitle = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $metaDescription = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $metaKeywords = null;

    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    public function setPageTitle(?string $pageTitle): void
    {
        $this->pageTitle = $pageTitle;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }
}