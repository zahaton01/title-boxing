<?php

namespace App\Domain\Entity\Commerce;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\CreationDateTrait;
use App\Domain\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @ORM\Table("orders")
 */
class Order extends AbstractEntity
{
    use CreationDateTrait;

    public const STATUS_NEW = 'new';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_DELIVERING = 'on_deliver';
    public const STATUS_DONE = 'done';

    public const PAYMENT_TYPE_CASH = 'cash';
    public const PAYMENT_TYPE_ONLINE = 'online';

    public const DELIVERY_TYPE_COURIER = 'courier';
    public const DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE = 'nova_poshta_warehouse';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $surname = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $deliveryCity = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $deliveryAddress = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $novaPoshtaCity = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $novaPoshtaWarehouse = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $status;

    /**
     * @ORM\Column(type="string")
     */
    private string $paymentType;

    /**
     * @ORM\Column(type="string")
     */
    private string $deliveryType;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $phone = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="json")
     */
    private array $cart = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User\User", inversedBy="orders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?User $user = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $orderNumber = null;

    public function __construct()
    {
        $this->creationDate = new \DateTimeImmutable();
        $this->status = self::STATUS_NEW;
        $this->paymentType = self::PAYMENT_TYPE_CASH;
        $this->deliveryType = self::DELIVERY_TYPE_COURIER;
    }

    public function __clone()
    {
        $this->id = null;
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): void
    {
        $this->surname = $surname;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?string $deliveryCity): void
    {
        $this->deliveryCity = $deliveryCity;
    }

    public function getDeliveryAddress(): ?string
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?string $deliveryAddress): void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getNovaPoshtaCity(): ?string
    {
        return $this->novaPoshtaCity;
    }

    public function setNovaPoshtaCity(?string $novaPoshtaCity): void
    {
        $this->novaPoshtaCity = $novaPoshtaCity;
    }

    public function getNovaPoshtaWarehouse(): ?string
    {
        return $this->novaPoshtaWarehouse;
    }

    public function setNovaPoshtaWarehouse(?string $novaPoshtaWarehouse): void
    {
        $this->novaPoshtaWarehouse = $novaPoshtaWarehouse;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getPaymentType(): string
    {
        return $this->paymentType;
    }

    public function setPaymentType(string $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    public function getDeliveryType(): string
    {
        return $this->deliveryType;
    }

    public function setDeliveryType(string $deliveryType): void
    {
        $this->deliveryType = $deliveryType;
    }

    public function getCart(): array
    {
        return $this->cart;
    }

    public function setCart(array $cart): void
    {
        $this->cart = $cart;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function isOnlinePayment()
    {
        return $this->paymentType === self::PAYMENT_TYPE_ONLINE;
    }

    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(?string $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }
}