<?php

namespace App\Domain\Entity\Commerce;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class Brand extends AbstractEntity
{
    use LocaleTrait;
    use IsVisibleTrait;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private ?MediaFile $image = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Commerce\BrandTranslation", mappedBy="brand", cascade={"all"})
     */
    private Collection $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\Product", mappedBy="brand")
     */
    private Collection $products;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function getImage(): ?MediaFile
    {
        return $this->image;
    }

    public function setImage(?MediaFile $image): void
    {
        $this->image = $image;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }

    public function getProducts()
    {
        return $this->products;
    }
}