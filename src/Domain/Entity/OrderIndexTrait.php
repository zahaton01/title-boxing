<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait OrderIndexTrait
{
    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    public int $orderIndex = 0;

    public function getOrderIndex(): int
    {
        return $this->orderIndex;
    }

    public function setOrderIndex(int $orderIndex): void
    {
        $this->orderIndex = $orderIndex;
    }
}