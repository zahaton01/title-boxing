<?php

namespace App\Domain\Entity\Menu;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\OrderIndexTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class MenuLink extends AbstractEntity
{
    public const STATIC_PARENT_DESKTOP_HEADER = 'desktop_header';
    public const STATIC_PARENT_DESKTOP_HEADER_CATEGORIES = 'desktop_header_categories';
    public const STATIC_PARENT_FOOTER = 'footer';
    public const STATIC_PARENT_MOBILE_DROPDOWN = 'mobile_dropdown';

    use LocaleTrait;
    use IsVisibleTrait;
    use TranslatableMenuLinkFields;
    use OrderIndexTrait;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $staticParent = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Domain\Entity\Menu\MenuLinkImage", cascade={"all"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?MenuLinkImage $image = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Menu\MenuLink", inversedBy="subLinks")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?MenuLink $parent = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Menu\MenuLink", mappedBy="parent")
     * @ORM\OrderBy({"orderIndex": "ASC"})
     */
    private Collection $subLinks;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Menu\MenuLinkTranslation", mappedBy="link", cascade={"all"})
     */
    private Collection $translations;

    public function __construct()
    {
        $this->subLinks = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    public function getStaticParent(): ?string
    {
        return $this->staticParent;
    }

    public function setStaticParent(?string $staticParent): void
    {
        $this->staticParent = $staticParent;
    }

    public function getImage(): ?MenuLinkImage
    {
        return $this->image;
    }

    public function setImage(?MenuLinkImage $image): void
    {
        $this->image = $image;
    }

    public function getParent(): ?MenuLink
    {
        return $this->parent;
    }

    public function setParent(?MenuLink $parent): void
    {
        $this->parent = $parent;
    }

    public function getSubLinks(): Collection
    {
        return $this->subLinks;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (null !== $this->getStaticParent() && null !== $this->getParent()) {
            $context->buildViolation('You cannot set parent on link with static parent')
                ->atPath('parent')
                ->addViolation();
        }
    }
}