<?php

namespace App\Domain\Entity\Menu;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class MenuLinkImage extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private ?MediaFile $file = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Menu\MenuLinkImageTranslation", mappedBy="image", cascade={"all"})
     */
    private Collection $translations;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getFile(): ?MediaFile
    {
        return $this->file;
    }

    public function setFile(?MediaFile $file): void
    {
        $this->file = $file;
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }
}