<?php

namespace App\Domain\Entity\Menu;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class MenuLinkTranslation extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableMenuLinkFields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Menu\MenuLink", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?MenuLink $link = null;

    public function getLink(): ?MenuLink
    {
        return $this->link;
    }

    public function setLink(?MenuLink $link): void
    {
        $this->link = $link;
    }
}