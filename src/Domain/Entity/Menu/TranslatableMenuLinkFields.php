<?php

namespace App\Domain\Entity\Menu;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait TranslatableMenuLinkFields
{
    /**
     * @ORM\Column(type="string")
     */
    private string $url = '';

    /**
     * @ORM\Column(type="string")
     */
    private string $name = '';

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}