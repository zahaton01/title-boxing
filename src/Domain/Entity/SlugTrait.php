<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait SlugTrait
{
    /**
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Assert\Regex(
     *     pattern="/^[\w-]+$/",
     *     message="Invalid slug"
     * )
     */
    private string $slug = '';

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }
}