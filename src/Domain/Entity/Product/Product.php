<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\Commerce\Brand;
use App\Domain\Entity\CreationDateTrait;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use App\Domain\Entity\OrderIndexTrait;
use App\Domain\Entity\SlugTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @UniqueEntity(fields={"slug"})
 */
class Product extends AbstractEntity
{
    public const MARKING_NONE = 'none';
    public const MARKING_NEW = 'new';
    public const MARKING_TOP = 'top';
    public const MARKING_SALE = 'sale';
    public const MARKING_SOON = 'soon';
    public const MARKING_PRE_ORDER = 'pre_order';

    use LocaleTrait;
    use CreationDateTrait;
    use MetaTrait;
    use SlugTrait;
    use IsVisibleTrait;
    use TranslatableProductFields;
    use OrderIndexTrait;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $price = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $salePrice = null;

    /**
     * @ORM\Column(type="string", nullable=false, options={"default": "none"})
     */
    private string $marking;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductTranslation", mappedBy="product", cascade={"all"})
     */
    private Collection $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductImage", mappedBy="product", cascade={"all"})
     */
    private Collection $images;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductSpecification", mappedBy="product", cascade={"all"})
     */
    private Collection $specifications;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Entity\Product\ProductCategory", inversedBy="products")
     */
    private Collection $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductComment", mappedBy="product", cascade="all")
     */
    private Collection $comments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Commerce\Brand", inversedBy="products")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?Brand $brand = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $vendorCode = null;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->specifications = new ArrayCollection();
        $this->creationDate = new \DateTimeImmutable();
        $this->marking = self::MARKING_NONE;
        $this->comments = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getSalePrice(): ?float
    {
        return $this->salePrice;
    }

    public function setSalePrice(float $salePrice): void
    {
        $this->salePrice = $salePrice;
    }

    public function getMarking(): string
    {
        return $this->marking;
    }

    public function setMarking(string $marking): void
    {
        $this->marking = $marking;
    }

    /**
     * @return ProductTranslation[]|Collection
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getPreviewImage(): ?ProductImage
    {
        $images = array_filter($this->getImages()->toArray(), function ($image) {
            return $image->isPreview();
        });

        return count($images) > 0 ? current($images) : null;
    }

    /**
     * @return ProductSpecification[]|Collection
     */
    public function getSpecifications()
    {
        return $this->specifications;
    }

    /**
     * @return ProductCategory[]|Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(ProductCategory $productCategory): void
    {
        if (!$this->categories->contains($productCategory)) {
            $this->categories->add($productCategory);
        }
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function isOnSale(): bool
    {
        return $this->marking === self::MARKING_SALE;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): void
    {
        $this->brand = $brand;
    }

    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    public function setVendorCode(?string $vendorCode): void
    {
        $this->vendorCode = $vendorCode;
    }
}