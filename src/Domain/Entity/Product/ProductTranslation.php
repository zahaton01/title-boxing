<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductTranslation extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableProductFields;
    use MetaTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Product", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }
}