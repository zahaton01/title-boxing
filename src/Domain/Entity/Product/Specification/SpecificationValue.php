<?php

namespace App\Domain\Entity\Product\Specification;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class SpecificationValue extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableSpecificationValueFields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Specification\Specification", inversedBy="values")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Specification $specification = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\Specification\SpecificationValueTranslation", mappedBy="value", cascade={"all"})
     */
    private Collection $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    public function setSpecification(Specification $specification): void
    {
        $this->specification = $specification;
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }
}