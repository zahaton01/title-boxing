<?php

namespace App\Domain\Entity\Product\Specification;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class SpecificationValueTranslation extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableSpecificationValueFields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Specification\SpecificationValue", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?SpecificationValue $value;

    public function getValue(): ?SpecificationValue
    {
        return $this->value;
    }

    public function setValue(SpecificationValue $value): void
    {
        $this->value = $value;
    }
}