<?php

namespace App\Domain\Entity\Product\Specification;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class SpecificationTranslation extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableSpecificationFields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Specification\Specification", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Specification $specification = null;

    public function getSpecification(): ?Specification
    {
        return $this->specification;
    }

    public function setSpecification(Specification $specification): void
    {
        $this->specification = $specification;
    }
}