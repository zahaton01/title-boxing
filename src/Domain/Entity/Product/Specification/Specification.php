<?php

namespace App\Domain\Entity\Product\Specification;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Product\ProductCategory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class Specification extends AbstractEntity
{
    use LocaleTrait;
    use IsVisibleTrait;
    use TranslatableSpecificationFields;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Entity\Product\ProductCategory")
     */
    private Collection $categories;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\Specification\SpecificationTranslation", mappedBy="specification", cascade={"all"})
     */
    private Collection $translations;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\Specification\SpecificationValue", mappedBy="specification", cascade={"all"})
     */
    private Collection $values;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->values = new ArrayCollection();
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addCategory(ProductCategory $category): void
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function addValue(SpecificationValue $specificationValue)
    {
        if (!$this->values->contains($specificationValue)) {
            $this->values->add($specificationValue);
        }
    }
}