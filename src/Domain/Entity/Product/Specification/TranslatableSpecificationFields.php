<?php

namespace App\Domain\Entity\Product\Specification;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait TranslatableSpecificationFields
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $name = '';

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}