<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductImageTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\ProductImage", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?ProductImage $image = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private ?MediaFile $file = null;

    public function getImage(): ?ProductImage
    {
        return $this->image;
    }

    public function setImage(?ProductImage $image): void
    {
        $this->image = $image;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }

    public function getFile(): ?MediaFile
    {
        return $this->file;
    }

    public function setFile(MediaFile $file): void
    {
        $this->file = $file;
    }
}