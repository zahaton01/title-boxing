<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Infrastructure\Availability;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ProductSpecificationValue extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Specification\SpecificationValue")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?SpecificationValue $relatedSpecificationValue = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $priceDifference = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $availability;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Entity\Product\ProductSpecification", mappedBy="values")
     */
    private Collection $specifications;

    public function __construct()
    {
        $this->availability = Availability::AVAILABLE;
    }

    public function getRelatedSpecificationValue(): ?SpecificationValue
    {
        return $this->relatedSpecificationValue;
    }

    public function setRelatedSpecificationValue(?SpecificationValue $relatedSpecificationValue): void
    {
        $this->relatedSpecificationValue = $relatedSpecificationValue;
    }

    public function getPriceDifference(): ?float
    {
        return $this->priceDifference;
    }

    public function setPriceDifference(?float $priceDifference): void
    {
        $this->priceDifference = $priceDifference;
    }

    public function getAvailability(): string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): void
    {
        $this->availability = $availability;
    }

    public function getSpecifications(): Collection
    {
        return $this->specifications;
    }
}