<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductCategoryTranslation extends AbstractEntity
{
    use LocaleTrait;
    use TranslatableProductCategoryFields;
    use MetaTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\ProductCategory", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?ProductCategory $category = null;

    public function getCategory(): ?ProductCategory
    {
        return $this->category;
    }

    public function setCategory(ProductCategory $category): void
    {
        $this->category = $category;
    }
}