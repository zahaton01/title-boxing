<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\Product\Specification\Specification;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductSpecification extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Specification\Specification")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private Specification $relatedSpecification;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Entity\Product\ProductSpecificationValue", cascade="all", inversedBy="specifications")
     */
    private Collection $values;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Product", inversedBy="specifications")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * @return ProductSpecificationValue[]|Collection
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function getRelatedSpecification(): Specification
    {
        return $this->relatedSpecification;
    }

    public function setRelatedSpecification(Specification $relatedSpecification): void
    {
        $this->relatedSpecification = $relatedSpecification;

        /**
         * Clearing values in case they are not related to new specification
         */
        $this->values->clear();
    }

    public function addValue(ProductSpecificationValue $value)
    {
        if (!$this->values->contains($value)) {
            $this->values->add($value);
        }
    }
}