<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\CreationDateTrait;
use App\Domain\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ProductComment extends AbstractEntity
{
    use CreationDateTrait;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $authorName = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Product", inversedBy="comments")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\User\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?User $author = null;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $text = '';

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $stars = 0;

    public function __construct()
    {
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getAuthorName(): string
    {
        return $this->authorName;
    }

    public function setAuthorName(string $authorName): void
    {
        $this->authorName = $authorName;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): void
    {
        $this->author = $author;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    public function getStars(): int
    {
        return $this->stars;
    }

    public function setStars(int $stars): void
    {
        $this->stars = $stars;
    }
}