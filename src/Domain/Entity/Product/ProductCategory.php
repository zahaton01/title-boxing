<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use App\Domain\Entity\SlugTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductCategory extends AbstractEntity
{
    use LocaleTrait;
    use SlugTrait;
    use TranslatableProductCategoryFields;
    use IsVisibleTrait;
    use MetaTrait;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductCategoryTranslation", mappedBy="category", cascade={"all"})
     */
    private Collection $translations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\ProductCategory", inversedBy="subCategories")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?ProductCategory $category = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductCategory", mappedBy="category")
     */
    private Collection $subCategories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Domain\Entity\Product\Product", mappedBy="categories")
     */
    private Collection $products;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->subCategories = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getCategory(): ?ProductCategory
    {
        return $this->category;
    }

    public function setCategory(?ProductCategory $category): void
    {
        $this->category = $category;
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function getSubCategories(): Collection
    {
        return $this->subCategories;
    }

    /**
     *  @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }
}