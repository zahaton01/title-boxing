<?php

namespace App\Domain\Entity\Product;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class ProductImage extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $isPreview = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\Product", inversedBy="images")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Product $product = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private ?MediaFile $file = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Product\ProductImageTranslation", mappedBy="image")
     */
    private Collection $translations;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Product\ProductSpecificationValue")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?ProductSpecificationValue $specificationValue = null;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    public function isPreview(): bool
    {
        return $this->isPreview;
    }

    public function setIsPreview(bool $isPreview): void
    {
        $this->isPreview = $isPreview;
    }

    public function getFile(): ?MediaFile
    {
        return $this->file;
    }

    public function setFile(MediaFile $file): void
    {
        $this->file = $file;
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }

    public function getSpecificationValue(): ?ProductSpecificationValue
    {
        return $this->specificationValue;
    }

    public function setSpecificationValue(?ProductSpecificationValue $specificationValue): void
    {
        $this->specificationValue = $specificationValue;
    }
}