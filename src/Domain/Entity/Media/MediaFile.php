<?php

namespace App\Domain\Entity\Media;

use App\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @UniqueEntity(fields={"relativePath"})
 */
class MediaFile extends AbstractEntity
{
    public const IMAGE = 'image';

    /**
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private string $relativePath;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $mediaType;

    public function getRelativePath(): string
    {
        return $this->relativePath;
    }

    public function setRelativePath(string $relativePath): void
    {
        $this->relativePath = $relativePath;
    }

    public function getMediaType(): string
    {
        return $this->mediaType;
    }

    public function setMediaType(string $mediaType): void
    {
        $this->mediaType = $mediaType;
    }

    public static function image()
    {
        $image = new self();
        $image->setMediaType(self::IMAGE);

        return $image;
    }
}