<?php

namespace App\Domain\Entity\Site;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use App\Domain\Entity\OrderIndexTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class MainBanner extends AbstractEntity
{
    use LocaleTrait;
    use IsVisibleTrait;
    use OrderIndexTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?MediaFile $file;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Site\MainBannerTranslation", mappedBy="banner", cascade="all")
     */
    private Collection $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getFile(): ?MediaFile
    {
        return $this->file;
    }

    public function setFile(?MediaFile $file): void
    {
        $this->file = $file;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }

    public function getTranslations(): Collection
    {
        return $this->translations;
    }
}