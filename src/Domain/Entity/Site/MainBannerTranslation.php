<?php

namespace App\Domain\Entity\Site;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\Media\MediaFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class MainBannerTranslation extends AbstractEntity
{
    use LocaleTrait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Media\MediaFile")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private ?MediaFile $file = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $altText = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Site\MainBanner", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?MainBanner $banner = null;

    public function getFile(): ?MediaFile
    {
        return $this->file;
    }

    public function setFile(?MediaFile $file): void
    {
        $this->file = $file;
    }

    public function getAltText(): ?string
    {
        return $this->altText;
    }

    public function setAltText(?string $altText): void
    {
        $this->altText = $altText;
    }

    public function getBanner(): ?MainBanner
    {
        return $this->banner;
    }

    public function setBanner(?MainBanner $banner): void
    {
        $this->banner = $banner;
    }
}