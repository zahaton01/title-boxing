<?php

namespace App\Domain\Entity\Site;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\IsVisibleTrait;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use App\Domain\Entity\SlugTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @UniqueEntity(fields={"slug"})
 */
class Page extends AbstractEntity
{
    public const STATIC_TYPE_HOME = 'static_home';
    public const STATIC_TYPE_CATALOG = 'static_catalog';
    public const STATIC_TYPE_CART = 'static_cart';
    public const STATIC_TYPE_CHECKOUT = 'static_checkout';
    public const STATIC_TYPE_CHECKOUT_THANK_YOU = 'static_checkout_thank_you';
    public const STATIC_TYPE_CHECKOUT_ONLINE_ORDER_THANK_YOU = 'static_checkout_online_order_thank_you';
    public const STATIC_TYPE_PROFILE_LIKED_PRODUCTS = 'static_profile_liked_products';
    public const STATIC_TYPE_PROFILE_ORDERS = 'static_profile_orders';
    public const STATIC_TYPE_PROFILE_CHANGE_INFO = 'static_profile_change_info';
    public const STATIC_TYPE_PROFILE_CHANGE_PASSWORD = 'static_profile_change_password';
    public const STATIC_TYPE_PROFILE_INFO = 'static_profile_info';
    public const STATIC_TYPE_LOGIN = 'static_login';
    public const STATIC_TYPE_REGISTRATION = 'static_registration';
    public const STATIC_TYPE_RESET_PASSWORD = 'static_reset_password';
    public const STATIC_TYPE_BRANDS = 'static_brands';

    use LocaleTrait;
    use MetaTrait;
    use IsVisibleTrait;
    use SlugTrait;
    use TranslatablePageFields;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" = 0})
     */
    private bool $isStatic = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $staticType = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Domain\Entity\Site\PageTranslation", mappedBy="page", cascade={"all"})
     */
    private Collection $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function isStatic(): bool
    {
        return $this->isStatic;
    }

    public function setIsStatic(bool $isStatic): void
    {
        $this->isStatic = $isStatic;
    }

    public function getStaticType(): ?string
    {
        return $this->staticType;
    }

    public function setStaticType(?string $staticType): void
    {
        $this->staticType = $staticType;
    }

    public function getTranslations()
    {
        return $this->translations;
    }
}