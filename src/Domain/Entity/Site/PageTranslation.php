<?php

namespace App\Domain\Entity\Site;

use App\Domain\Entity\AbstractEntity;
use App\Domain\Entity\LocaleTrait;
use App\Domain\Entity\MetaTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class PageTranslation extends AbstractEntity
{
    use LocaleTrait;
    use MetaTrait;
    use TranslatablePageFields;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Entity\Site\Page", inversedBy="translations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Page $page = null;

    public function getPage(): Page
    {
        return $this->page;
    }

    public function setPage(Page $page): void
    {
        $this->page = $page;
    }
}