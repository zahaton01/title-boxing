<?php

namespace App\Domain\Entity\Site;

use App\Domain\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class StaticVar extends AbstractEntity
{
    public const KEY_PHONE_1 = 'phone_1';
    public const KEY_PHONE_2 = 'phone_2';
    public const KEY_PHONE_3 = 'phone_3';
    public const KEY_INSTAGRAM_LINK = 'instagram_link';

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private string $externalKey = '';

    /**
     * @ORM\Column(type="text")
     */
    private string $value = '';

    public function __toString()
    {
        return $this->value;
    }

    public function getExternalKey(): string
    {
        return $this->externalKey;
    }

    public function setExternalKey(string $externalKey): void
    {
        $this->externalKey = $externalKey;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}