<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Product\ProductCategoryTranslation;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductCategoryFixtures extends Fixture
{
    public const CATEGORY_GLOVES = 'category_gloves';
    public const CATEGORY_ACCESSORIES = 'category_accessories';
    public const CATEGORY_CLOTHES = 'category_clothes';

    public function load(ObjectManager $manager)
    {
        $category1 = new ProductCategory();
        $category1->setName('Перчатки');
        $category1->setLocale(Locales::RU);
        $category1->setSlug('gloves');
        $category1->setMetaTitle('Перчатки');
        $category1->setPageTitle('Перчатки');
        $manager->persist($category1);

        $category1UkTranslation = new ProductCategoryTranslation();
        $category1UkTranslation->setName('Рукавички');
        $category1UkTranslation->setLocale(Locales::UK);
        $category1UkTranslation->setCategory($category1);
        $category1UkTranslation->setMetaTitle('Рукавички');
        $category1UkTranslation->setPageTitle('Рукавички');
        $manager->persist($category1UkTranslation);

        for ($i = 1; $i <= 10; $i++) {
            $subCategory = new ProductCategory();
            $subCategory->setName("<{$category1->getName()}> подкатегория $i");
            $subCategory->setCategory($category1);
            $subCategory->setSlug("{$category1->getSlug()}-sub-cat-slug-$i");
            $subCategory->setLocale(Locales::RU);
            $subCategory->setPageTitle("<{$category1->getName()}> подкатегория $i");
            $subCategory->setMetaTitle("<{$category1->getName()}> подкатегория $i");
            $manager->persist($subCategory);

            $ukTranslation = new ProductCategoryTranslation();
            $ukTranslation->setName("<{$category1UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setCategory($subCategory);
            $ukTranslation->setMetaTitle("<{$category1UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setPageTitle("<{$category1UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setLocale(Locales::UK);
            $manager->persist($ukTranslation);
        }

        $this->addReference(self::CATEGORY_GLOVES, $category1);

        $category2 = new ProductCategory();
        $category2->setName('Аксессуары');
        $category2->setLocale(Locales::RU);
        $category2->setSlug('accessories');
        $category2->setPageTitle('Аксессуары');
        $category2->setMetaTitle('Аксессуары');
        $manager->persist($category2);

        $category2UkTranslation = new ProductCategoryTranslation();
        $category2UkTranslation->setName('Аксесуари');
        $category2UkTranslation->setLocale(Locales::UK);
        $category2UkTranslation->setCategory($category2);
        $category2UkTranslation->setMetaTitle('Аксесуари');
        $category2UkTranslation->setPageTitle('Аксесуари');
        $manager->persist($category2UkTranslation);

        for ($i = 1; $i <= 10; $i++) {
            $subCategory = new ProductCategory();
            $subCategory->setName("<{$category2->getName()}> подкатегория $i");
            $subCategory->setCategory($category2);
            $subCategory->setSlug("{$category2->getSlug()}-sub-cat-slug-$i");
            $subCategory->setLocale(Locales::RU);
            $subCategory->setPageTitle("<{$category2->getName()}> подкатегория $i");
            $subCategory->setMetaTitle("<{$category2->getName()}> подкатегория $i");
            $manager->persist($subCategory);

            $ukTranslation = new ProductCategoryTranslation();
            $ukTranslation->setName("<{$category2UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setCategory($subCategory);
            $ukTranslation->setLocale(Locales::UK);
            $ukTranslation->setMetaTitle("<{$category2UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setPageTitle("<{$category2UkTranslation->getName()}> подкатегорія $i");
            $manager->persist($ukTranslation);
        }

        $this->addReference(self::CATEGORY_ACCESSORIES, $category2);

        $category3 = new ProductCategory();
        $category3->setName('Одежда');
        $category3->setLocale(Locales::RU);
        $category3->setSlug('clothes');
        $category3->setPageTitle('Одежда');
        $category3->setMetaTitle('Одежда');
        $manager->persist($category3);

        $category3UkTranslation = new ProductCategoryTranslation();
        $category3UkTranslation->setName('Одяг');
        $category3UkTranslation->setLocale(Locales::UK);
        $category3UkTranslation->setCategory($category3);
        $category3UkTranslation->setPageTitle('Одяг');
        $category3UkTranslation->setMetaTitle('Одяг');
        $manager->persist($category3UkTranslation);

        for ($i = 1; $i <= 10; $i++) {
            $subCategory = new ProductCategory();
            $subCategory->setName("<{$category3->getName()}> подкатегория $i");
            $subCategory->setCategory($category3);
            $subCategory->setSlug("{$category3->getSlug()}-sub-cat-slug-$i");
            $subCategory->setLocale(Locales::RU);
            $subCategory->setMetaTitle("<{$category3->getName()}> подкатегория $i");
            $subCategory->setPageTitle("<{$category3->getName()}> подкатегория $i");
            $manager->persist($subCategory);

            $ukTranslation = new ProductCategoryTranslation();
            $ukTranslation->setName("<{$category3UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setCategory($subCategory);
            $ukTranslation->setLocale(Locales::UK);
            $ukTranslation->setPageTitle("<{$category3UkTranslation->getName()}> подкатегорія $i");
            $ukTranslation->setMetaTitle("<{$category3UkTranslation->getName()}> подкатегорія $i");
            $manager->persist($ukTranslation);
        }

        $this->addReference(self::CATEGORY_CLOTHES, $category3);

        $manager->flush();
    }
}