<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Entity\Product\Product;
use App\Domain\Entity\User\User;
use App\Domain\Repository\ProductRepository;
use App\Domain\Repository\UserRepository;
use App\Infrastructure\ExternalService\NovaPoshtaClient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    private ProductRepository $productRepository;
    private UserRepository $userRepository;
    private NovaPoshtaClient $novaPoshtaClient;

    public function __construct(
        ProductRepository $productRepository,
        UserRepository $userRepository,
        NovaPoshtaClient $novaPoshtaClient
    ){
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
        $this->novaPoshtaClient = $novaPoshtaClient;
    }

    public function load(ObjectManager $manager)
    {
        $products = $this->productRepository->findAll();
        $users = $this->userRepository->findAll();

        $statuses = [
            Order::STATUS_NEW,
            Order::STATUS_DONE,
            Order::STATUS_DELIVERING,
            Order::STATUS_CONFIRMED
        ];

        $paymentTypes = [
            Order::PAYMENT_TYPE_ONLINE,
            Order::PAYMENT_TYPE_CASH
        ];

        $novaCities = [];
        $novaWarehouses = [];

        $novaCitiesRefs = [];
        $novaCitiesData = $this->novaPoshtaClient->getCities();
        foreach ($novaCitiesData as $index => $novaCitiesDatum) {
            $novaCitiesRefs[] = $novaCitiesDatum['Ref'];

            if ($index % 2 === 0) {
                $novaCities[] = $novaCitiesDatum['Description'];
            } else {
                $novaCities[] = $novaCitiesDatum['DescriptionRu'];
            }

            if ($index > 10) {
                break;
            }
        }

        foreach ($novaCitiesRefs as $index => $novaCityRef) {
            foreach ($this->novaPoshtaClient->getWarehouses($novaCityRef) as $warehouse) {
                $novaWarehouses[] = $index % 2 === 0 ? $warehouse['Description'] : $warehouse['DescriptionRu'];
            }
        }

        for ($i = 1; $i <= 500; $i++) {
            $order = new Order();
            $order->setStatus($statuses[array_rand($statuses)]);
            $order->setPaymentType($paymentTypes[array_rand($paymentTypes)]);
            $order->setName(UserFixtures::getRandomName());
            $order->setPhone(UserFixtures::getRandomPhone());
            $order->setEmail(null);
            $order->setSurname(UserFixtures::getRandomSurname());
            $order->setDeliveryAddress(UserFixtures::getRandomAddress());
            $order->setDeliveryCity(UserFixtures::getRandomCity());
            $order->setCart($this->getCart(...$products));

            if ($i % 3 === 0) {
                /** @var User $user */
                $user = $users[array_rand($users)];

                $order->setUser($user);
                $order->setName($user->getName());
                $order->setPhone($user->getPhone());
                $order->setEmail($user->getEmail());
                $order->setSurname($user->getSurname());
                $order->setDeliveryAddress($user->getAddress());
                $order->setDeliveryCity($user->getCity());
            }

            if ($i % 2 === 0) {
                $order->setDeliveryType(Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE);
                $order->setNovaPoshtaCity($novaCities[array_rand($novaCities)]);
                $order->setNovaPoshtaWarehouse($novaWarehouses[array_rand($novaWarehouses)]);
            }

            $randDays = rand(1, 999);
            $order->setCreationDate(new \DateTimeImmutable("now - $randDays days"));

            $manager->persist($order);
        }

        $manager->flush();
    }

    private function getCart(Product ...$products): array
    {
        $randKeys = array_rand($products, rand(2, 8));
        $selectedProducts = [];

        foreach ($randKeys as $key) {
            $selectedProducts[] = $products[$key];
        }

        $data = [];
        /** @var Product $product */
        foreach ($selectedProducts as $product) {
            $cartItem = [];
            $cartItem['id'] = $product->getId();
            $cartItem['quantity'] = rand(1, 10);
            $cartItem['product_specification_values'] = [];

            foreach ($product->getSpecifications() as $specification) {
                $cartItem['product_specification_values'][] = $specification->getValues()->get(0)->getId();
            }

            $data[] = $cartItem;
        }

        return $data;
    }

    public function getDependencies()
    {
        return [
            ProductFixtures::class,
            UserFixtures::class
        ];
    }
}