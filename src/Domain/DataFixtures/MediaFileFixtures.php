<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Media\MediaFile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MediaFileFixtures extends Fixture
{
    public const MENU_LINK_IMAGE_1 = 'menu_link_image_1';

    public const MAIN_BANNER_IMAGE_1 = 'main_banner_image_1';

    public const PRODUCT_IMAGE_1 = 'product_image_1';
    public const PRODUCT_IMAGE_2 = 'product_image_2';
    public const PRODUCT_IMAGE_3 = 'product_image_3';
    public const PRODUCT_IMAGE_4 = 'product_image_4';
    public const PRODUCT_IMAGE_5 = 'product_image_5';
    public const PRODUCT_IMAGE_6 = 'product_image_6';
    public const PRODUCT_IMAGE_7 = 'product_image_7';
    public const PRODUCT_IMAGE_8 = 'product_image_8';
    public const PRODUCT_IMAGE_9 = 'product_image_9';
    public const PRODUCT_IMAGE_10 = 'product_image_10';

    public const BRAND_IMAGE_1 = 'brand_image_1';
    public const BRAND_IMAGE_2 = 'brand_image_2';
    public const BRAND_IMAGE_3 = 'brand_image_3';
    public const BRAND_IMAGE_4 = 'brand_image_4';
    public const BRAND_IMAGE_5 = 'brand_image_5';
    public const BRAND_IMAGE_6 = 'brand_image_6';
    public const BRAND_IMAGE_7 = 'brand_image_7';
    public const BRAND_IMAGE_8 = 'brand_image_8';
    public const BRAND_IMAGE_9 = 'brand_image_9';
    public const BRAND_IMAGE_10 = 'brand_image_10';

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 1; $i++) {
            $menuLinkImage = MediaFile::image();
            $menuLinkImage->setRelativePath("/images/fixtures-images/menu-links/$i.png");

            $this->addReference(constant(MediaFileFixtures::class . '::MENU_LINK_IMAGE_' . $i), $menuLinkImage);
            $manager->persist($menuLinkImage);
        }

        for ($i = 1; $i <= 1; $i++) {
            $bannerImage = MediaFile::image();
            $bannerImage->setRelativePath("/images/fixtures-images/main-banner/$i.png");

            $this->addReference(constant(MediaFileFixtures::class . '::MAIN_BANNER_IMAGE_' . $i), $bannerImage);
            $manager->persist($bannerImage);
        }

        for ($i = 1; $i <= 10; $i++) {
            $productImage = MediaFile::image();
            $productImage->setRelativePath("/images/fixtures-images/products/$i.jpg");
            $this->addReference(constant(MediaFileFixtures::class . '::PRODUCT_IMAGE_' . $i), $productImage);

            $manager->persist($productImage);
        }

        for ($i = 1; $i <= 10; $i++) {
            $brandImage = MediaFile::image();
            $brandImage->setRelativePath("/images/fixtures-images/brands/$i.jpg");
            $this->addReference(constant(MediaFileFixtures::class . '::BRAND_IMAGE_' . $i), $brandImage);

            $manager->persist($brandImage);
        }

        $manager->flush();
    }
}