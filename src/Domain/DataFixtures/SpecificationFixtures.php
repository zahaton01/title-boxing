<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationTranslation;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Domain\Entity\Product\Specification\SpecificationValueTranslation;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class SpecificationFixtures extends Fixture implements DependentFixtureInterface
{
    public const SPECIFICATION_COLOR = 'specification_color';
    public const SPECIFICATION_SIZE = 'specification_size';

    public function load(ObjectManager $manager)
    {
        $color = new Specification();
        $color->setLocale(Locales::RU);
        $color->setName('Цвет');
        $color->addCategory($this->getReference(ProductCategoryFixtures::CATEGORY_GLOVES));
        $color->addCategory($this->getReference(ProductCategoryFixtures::CATEGORY_ACCESSORIES));
        $this->addReference(self::SPECIFICATION_COLOR, $color);
        $manager->persist($color);

        $uk = new SpecificationTranslation();
        $uk->setLocale(Locales::UK);
        $uk->setName('Колір');
        $uk->setSpecification($color);
        $manager->persist($uk);

        for ($i = 1; $i <= 4; $i++) {
            $val = new SpecificationValue();
            $val->setLocale(Locales::RU);
            $val->setName('Цвет ' . $i);
            $val->setSpecification($color);
            $manager->persist($val);

            $uk = new SpecificationValueTranslation();
            $uk->setName("Колір $i");
            $uk->setLocale(Locales::UK);
            $uk->setValue($val);
            $manager->persist($uk);
        }

        $size = new Specification();
        $size->setLocale(Locales::RU);
        $size->setName('Размер');
        $size->addCategory($this->getReference(ProductCategoryFixtures::CATEGORY_GLOVES));
        $size->addCategory($this->getReference(ProductCategoryFixtures::CATEGORY_CLOTHES));
        $this->addReference(self::SPECIFICATION_SIZE, $size);
        $manager->persist($size);

        $uk = new SpecificationTranslation();
        $uk->setLocale(Locales::UK);
        $uk->setName('Розмір');
        $uk->setSpecification($size);
        $manager->persist($uk);

        for ($i = 1; $i <= 4; $i++) {
            $val = new SpecificationValue();
            $val->setLocale(Locales::RU);
            $val->setName('Размер ' . $i);
            $val->setSpecification($size);
            $manager->persist($val);

            $uk = new SpecificationValueTranslation();
            $uk->setName("Розмір $i");
            $uk->setLocale(Locales::UK);
            $uk->setValue($val);
            $manager->persist($uk);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [ProductCategoryFixtures::class];
    }
}