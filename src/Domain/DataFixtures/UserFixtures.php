<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\User\Admin;
use App\Domain\Entity\User\User;
use App\Infrastructure\Security\Roles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserFixtures extends Fixture
{
    private EncoderFactoryInterface $encoderFactory;

    public function __construct(
      EncoderFactoryInterface $encoderFactory
    ) {
        $this->encoderFactory = $encoderFactory;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new Admin();
        $admin->setLogin('admin');
        $admin->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('admin', null));
        $admin->setRoles([Roles::ADMIN]);

        $manager->persist($admin);

        for ($i = 1; $i <= 50; $i++) {
            $user = new User();
            $user->setRoles([Roles::USER]);
            $user->setLogin("test_$i@example.com");
            $user->setPassword($this->encoderFactory->getEncoder(Admin::class)->encodePassword('123456', null));
            $user->setEmail("test_$i@example.com");
            $user->setName(UserFixtures::getRandomName());
            $user->setSurname(UserFixtures::getRandomSurname());
            $user->setAddress(UserFixtures::getRandomAddress());
            $user->setBirthday(new \DateTimeImmutable('06.03.2001'));
            $user->setGender(User::GENDER_MALE);
            $user->setPhone(UserFixtures::getRandomPhone());
            $user->setCity(UserFixtures::getRandomCity());
            $user->setIsEmailConfirmed(true);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public static function getRandomName(): string
    {
        $names = ['Liam', 'Noah', 'Oliver', 'William', 'Elijah', 'Olivia', 'Emma', 'Ava', 'Sophia'];

        return $names[array_rand($names)];
    }

    public static function getRandomSurname(): ?string
    {
        $surnames = ['Melnyk', 'Shevchenko', 'Boyko', 'Kovalenko', 'Bondarenko', 'Tkachenko', null];

        return $surnames[array_rand($surnames)];
    }

    public static function getRandomAddress(): string
    {
        $addresses = [
            '777 Brockton Avenue, Abington MA 2351',
            '30 Memorial Drive, Avon MA 2322',
            '250 Hartford Avenue, Bellingham MA 2019',
            '700 Oak Street, Brockton MA 2301',
            '66-4 Parkhurst Rd, Chelmsford MA 1824',
            '591 Memorial Dr, Chicopee MA 1020',
            '55 Brooksby Village Way, Danvers MA 1923',
            '137 Teaticket Hwy, East Falmouth MA 2536'
        ];

        return $addresses[array_rand($addresses)];
    }

    public static function getRandomPhone(): string
    {
        $phones = ['202-555-0166', '202-555-0168', '202-555-0198', '202-555-0166', '+1-202-555-0166', '+1-202-555-0168'];

        return $phones[array_rand($phones)];
    }

    public static function getRandomCity(): string
    {
        $cities = ['Los Angeles', 'Chicago', 'New York', 'San Antonio', 'San Diego', 'Dallas', 'Charlotte'];

        return $cities[array_rand($cities)];
    }
}