<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Commerce\Brand;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class BrandFixtures extends Fixture implements DependentFixtureInterface
{
    public const BRAND_1 = 'brand_1';
    public const BRAND_2 = 'brand_2';
    public const BRAND_3 = 'brand_3';
    public const BRAND_4 = 'brand_4';
    public const BRAND_5 = 'brand_5';
    public const BRAND_6 = 'brand_6';
    public const BRAND_7 = 'brand_7';
    public const BRAND_8 = 'brand_8';
    public const BRAND_9 = 'brand_9';
    public const BRAND_10 = 'brand_10';

    public function load(ObjectManager $manager)
    {
        $names = [
            'TITLE Black',
            'Adidas',
            'TITLE Classic',
            'Adidas',
            'BOOM BOOM Boxing',
            'Fighting',
            'Golden Boy Boxing',
            'Muhammad Ali',
            'Pro Mex',
            'Rival',
            'USA Boxing'
        ];

        for ($i = 1; $i <= 10; $i++) {
            $brand = new Brand();
            $brand->setLocale(Locales::RU);
            $brand->setName($names[$i]);
            $brand->setImage($this->getReference(constant(MediaFileFixtures::class . '::BRAND_IMAGE_' . $i)));

            $manager->persist($brand);
            $this->addReference(constant(BrandFixtures::class . '::BRAND_' . $i), $brand);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [MediaFileFixtures::class];
    }
}