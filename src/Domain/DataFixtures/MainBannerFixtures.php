<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Site\MainBanner;
use App\Domain\Entity\Site\MainBannerTranslation;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MainBannerFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $banner = new MainBanner();
            $banner->setLocale(Locales::RU);
            $banner->setFile($this->getReference(MediaFileFixtures::MAIN_BANNER_IMAGE_1));
            $banner->setAltText('Ru text _ ' . $i);

            $manager->persist($banner);

            $ukTranslation = new MainBannerTranslation();
            $ukTranslation->setLocale(Locales::UK);
            $ukTranslation->setAltText('Uk text _ ' . $i);
            $ukTranslation->setFile($this->getReference(MediaFileFixtures::MAIN_BANNER_IMAGE_1));
            $ukTranslation->setBanner($banner);

            $manager->persist($ukTranslation);
        }

        $manager->flush();
    }
}