<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Product\ProductComment;
use App\Domain\Entity\Product\ProductImage;
use App\Domain\Entity\Product\ProductSpecification;
use App\Domain\Entity\Product\ProductSpecificationValue;
use App\Domain\Entity\Product\Specification\Specification;
use App\Infrastructure\Availability;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories = [
            $this->getReference(ProductCategoryFixtures::CATEGORY_CLOTHES),
            $this->getReference(ProductCategoryFixtures::CATEGORY_ACCESSORIES),
            $this->getReference(ProductCategoryFixtures::CATEGORY_GLOVES),
        ];

        $markings = [
            Product::MARKING_NONE,
            Product::MARKING_NEW,
            Product::MARKING_SALE,
            Product::MARKING_SOON,
            Product::MARKING_TOP
        ];

        $images = [
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_1),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_2),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_3),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_4),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_5),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_6),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_7),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_8),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_9),
            $this->getReference(MediaFileFixtures::PRODUCT_IMAGE_10),
        ];

        $specifications = [
            $this->getReference(SpecificationFixtures::SPECIFICATION_COLOR),
            $this->getReference(SpecificationFixtures::SPECIFICATION_SIZE),
        ];

        $brands = [
            $this->getReference(BrandFixtures::BRAND_1),
            $this->getReference(BrandFixtures::BRAND_2),
            $this->getReference(BrandFixtures::BRAND_3),
            $this->getReference(BrandFixtures::BRAND_4),
            $this->getReference(BrandFixtures::BRAND_5),
            $this->getReference(BrandFixtures::BRAND_6),
            $this->getReference(BrandFixtures::BRAND_7),
            $this->getReference(BrandFixtures::BRAND_8),
            $this->getReference(BrandFixtures::BRAND_9),
            $this->getReference(BrandFixtures::BRAND_10),
        ];

        $name = $this->sliceString(file_get_contents('https://loripsum.net/api/plaintext/1/short'));
        $description = file_get_contents('https://loripsum.net/api/3/medium/decorate/link/ul/ol/bq');
        $commentText = file_get_contents('https://loripsum.net/api/3/short/plaintext');
        $names = ['Thomas', 'Liam', 'Olivia', 'Noah', 'Emma', 'William', 'Sophia'];

        for ($i = 1; $i <= 250; $i++) {
            if ($i % 10 === 0) {
                $name = $this->sliceString(file_get_contents('https://loripsum.net/api/plaintext/1/short'));
            }

            $product = new Product();
            $product->setLocale(Locales::RU);
            $product->setName($this->shuffleWords($name));
            $product->setSlug(uniqid('product_'));
            $product->setPrice(rand(39, 2499));
            $product->setDescription($description);
            $product->setPageTitle($name);
            $product->setPageTitle($name);
            $product->setMetaTitle($name);

            $marking = $markings[array_rand($markings)];
            $product->setMarking($marking);

            if ($marking === Product::MARKING_SALE) {
                $product->setSalePrice((int) $product->getPrice() * 0.8);
            }

            /** @var ProductCategory $category */
            $category = $categories[array_rand($categories)];
            $product->addCategory($category);
            if ($i % 5 === 0) {
                $subCategories = [];
                foreach ($category->getSubCategories() as $subCategory) {
                    $subCategories[] = $subCategory;
                }

                $product->addCategory($subCategories[array_rand($subCategories)]);
            }

            $manager->persist($product);

            if ($i % 5 === 0) {
                $product->setBrand($brands[array_rand($brands)]);
            }

            $productImage = new ProductImage();
            $productImage->setLocale(Locales::RU);
            $productImage->setProduct($product);
            $productImage->setIsPreview(true);
            $productImage->setFile($images[array_rand($images)]);
            $manager->persist($productImage);

            for ($j = 0; $j <= 5; $j++) {
                $productImage = new ProductImage();
                $productImage->setLocale(Locales::RU);
                $productImage->setProduct($product);
                $productImage->setIsPreview(false);
                $productImage->setFile($images[array_rand($images)]);
                $manager->persist($productImage);
            }

            if ($i % 2 === 0) {
                $productSpecification = new ProductSpecification();
                $productSpecification->setProduct($product);

                /** @var Specification $relatedSpecification */
                $relatedSpecification = $specifications[array_rand($specifications)];
                $productSpecification->setRelatedSpecification($relatedSpecification);

                $specValues = [];
                foreach ($relatedSpecification->getValues() as $value) {
                    $specValues[] = $value;
                }

                foreach (array_rand($specValues, rand(2, count($specValues))) as $item) {
                    $productSpecificationValue = new ProductSpecificationValue();
                    $productSpecificationValue->setRelatedSpecificationValue($specValues[$item]);

                    if ($i % 5 === 0) {
                        $productSpecificationValue->setAvailability(Availability::NOT_AVAILABLE);
                        $productSpecificationValue->setPriceDifference(rand(349, 12499));
                    }

                    $productSpecification->addValue($productSpecificationValue);
                }

                $manager->persist($productSpecification);
            }

            if ($i % 2 === 0) {
                for ($j = 0; $j < 25; $j++) {
                    $comment = new ProductComment();
                    $comment->setAuthorName($names[array_rand($names)]);
                    $comment->setText($this->shuffleWords($commentText));
                    $comment->setProduct($product);
                    $comment->setStars(rand(0, 5));

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MediaFileFixtures::class,
            ProductCategoryFixtures::class,
            SpecificationFixtures::class,
            BrandFixtures::class
        ];
    }

    private function shuffleWords(string $input)
    {
        $words = explode( " ", $input);
        shuffle($words);
        return implode(" ", $words);
    }

    private function sliceString(string $input)
    {
        if (mb_strlen($input) > 200) {
            return substr($input, 0, 200);
        }

        return $input;
    }
}