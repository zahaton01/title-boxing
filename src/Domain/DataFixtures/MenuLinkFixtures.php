<?php

namespace App\Domain\DataFixtures;

use App\Domain\Entity\Menu\MenuLink;
use App\Domain\Entity\Menu\MenuLinkImage;
use App\Domain\Entity\Menu\MenuLinkTranslation;
use App\Infrastructure\Locales;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MenuLinkFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $desktopHeaderLink1 = new MenuLink();
        $desktopHeaderLink1->setLocale(Locales::RU);
        $desktopHeaderLink1->setName('Перчатки');
        $desktopHeaderLink1->setUrl('/ru/catalog');
        $desktopHeaderLink1->setStaticParent(MenuLink::STATIC_PARENT_DESKTOP_HEADER);
        $desktopHeaderLink1->setOrderIndex(0);
        $manager->persist($desktopHeaderLink1);

        $uk = new MenuLinkTranslation();
        $uk->setLocale(Locales::UK);
        $uk->setName('Рукавички');
        $uk->setUrl('/uk/catalog');
        $uk->setLink($desktopHeaderLink1);
        $manager->persist($uk);

        $desktopHeaderLink2 = new MenuLink();
        $desktopHeaderLink2->setLocale(Locales::RU);
        $desktopHeaderLink2->setName('Топ продаж');
        $desktopHeaderLink2->setUrl('/ru/catalog');
        $desktopHeaderLink2->setStaticParent(MenuLink::STATIC_PARENT_DESKTOP_HEADER);
        $desktopHeaderLink2->setOrderIndex(2);
        $manager->persist($desktopHeaderLink2);

        $uk = new MenuLinkTranslation();
        $uk->setLocale(Locales::UK);
        $uk->setName('Топ продажів');
        $uk->setUrl('/uk/catalog');
        $uk->setLink($desktopHeaderLink2);
        $manager->persist($uk);

        $desktopHeaderLink4 = new MenuLink();
        $desktopHeaderLink4->setLocale(Locales::RU);
        $desktopHeaderLink4->setName('Контакты');
        $desktopHeaderLink4->setUrl('#');
        $desktopHeaderLink4->setStaticParent(MenuLink::STATIC_PARENT_DESKTOP_HEADER);
        $desktopHeaderLink4->setOrderIndex(5);
        $manager->persist($desktopHeaderLink4);

        for ($i = 1; $i <= 10; $i++) {
            $categoriesLink = new MenuLink();
            $categoriesLink->setLocale(Locales::RU);
            $categoriesLink->setName('Категория ' . $i);
            $categoriesLink->setUrl('/ru/catalog');
            $categoriesLink->setStaticParent(MenuLink::STATIC_PARENT_DESKTOP_HEADER_CATEGORIES);
            $categoriesLink->setOrderIndex($i);

            $image = new MenuLinkImage();
            $image->setLocale(Locales::RU);
            $image->setFile($this->getReference(MediaFileFixtures::MENU_LINK_IMAGE_1));
            $categoriesLink->setImage($image);

            $ukTranslation = new MenuLinkTranslation();
            $ukTranslation->setUrl('/uk/catalog');
            $ukTranslation->setName('Категорія ' . $i);
            $ukTranslation->setLocale(Locales::UK);
            $ukTranslation->setLink($categoriesLink);
            $manager->persist($ukTranslation);

            if ($i % 2 === 0) {
                for ($j = 1; $j <= 10; $j++) {
                    $subCategoryLink = new MenuLink();
                    $subCategoryLink->setLocale(Locales::RU);
                    $subCategoryLink->setName('Подкатегория ' . $j);
                    $subCategoryLink->setUrl('/ru/catalog');
                    $subCategoryLink->setParent($categoriesLink);
                    $subCategoryLink->setOrderIndex($j);

                    $manager->persist($subCategoryLink);

                    $ukSubTranslation = new MenuLinkTranslation();
                    $ukSubTranslation->setUrl('/uk/catalog');
                    $ukSubTranslation->setName('Підкатегорія ' . $i);
                    $ukSubTranslation->setLocale(Locales::UK);
                    $ukSubTranslation->setLink($subCategoryLink);
                    $manager->persist($ukSubTranslation);
                }
            }

            $manager->persist($categoriesLink);
        }

        for ($i = 1; $i <= 3; $i++) {
            $footerLinkCategory = new MenuLink();
            $footerLinkCategory->setLocale(Locales::RU);
            $footerLinkCategory->setOrderIndex($i - 1);
            $footerLinkCategory->setName('Категория футер ' . $i);
            $footerLinkCategory->setStaticParent(MenuLink::STATIC_PARENT_FOOTER);
            $footerLinkCategory->setUrl('#');

            for ($j = 1; $j <= 5; $j++) {
                $subLink = new MenuLink();
                $subLink->setLocale(Locales::RU);
                $subLink->setOrderIndex($j - 1);
                $subLink->setName('Подкатегория ' . $j);
                $subLink->setParent($footerLinkCategory);
                $subLink->setUrl('#');

                $manager->persist($subLink);
            }

            $manager->persist($footerLinkCategory);
        }

        $mobileHeaderLink1 = new MenuLink();
        $mobileHeaderLink1->setLocale(Locales::RU);
        $mobileHeaderLink1->setName('Каталог');
        $mobileHeaderLink1->setUrl('/ru/catalog');
        $mobileHeaderLink1->setStaticParent(MenuLink::STATIC_PARENT_MOBILE_DROPDOWN);
        $mobileHeaderLink1->setOrderIndex(0);
        $manager->persist($mobileHeaderLink1);

        $uk = new MenuLinkTranslation();
        $uk->setLocale(Locales::UK);
        $uk->setName('Каталог');
        $uk->setUrl('/uk/catalog');
        $uk->setLink($mobileHeaderLink1);
        $manager->persist($uk);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [MediaFileFixtures::class];
    }
}