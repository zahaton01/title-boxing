<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Product\ProductSpecificationValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductSpecificationValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSpecificationValue::class);
    }
}