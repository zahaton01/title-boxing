<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Query|Product[]
     */
    public function getListBy(
        ?ProductCategory $category = null,
        ?int $minPrice = null,
        ?int $maxPrice = null,
        array $specifications = [],
        array $specificationValues = [],
        array $brands = [],
        ?string $searchPhrase = null,
        ?string $marking = null,
        ?bool $isVisible = null,
        $paginated = true
    ) {
        $qb = $this->createQueryBuilder('p');
        $qb->leftJoin('p.specifications', 'ps');
        $qb->leftJoin('ps.relatedSpecification', 'pss');
        $qb->leftJoin('ps.values', 'psv');
        $qb->leftJoin('psv.relatedSpecificationValue', 'psrv');
        $qb->leftJoin('p.brand', 'pb');
        $qb->leftJoin('p.translations', 'pt');
        $qb->leftJoin('p.categories', 'pc');
        $qb->addOrderBy('p.orderIndex', 'ASC');

        if (null !== $isVisible) {
            $qb->andWhere('p.isVisible = :is_visible');
            $qb->setParameter('is_visible', $isVisible);
        }

        if (null !== $category) {
            $qb->andWhere('pc.id = :category_id');
            $qb->setParameter('category_id', $category->getId());
        }

        if (null !== $minPrice) {
            $qb->andWhere('p.price >= :min_price or p.salePrice >= :min_price');
            $qb->setParameter('min_price', $minPrice);
        }

        if (null !== $maxPrice) {
            $qb->andWhere('p.price <= :max_price or p.salePrice <= :max_price');
            $qb->setParameter('max_price', $maxPrice);
        }

        if (count($specifications) > 0) {
            $qb->andWhere('pss.id IN (:specifications)');
            $qb->setParameter('specifications', $specifications);
        }

        if (count($specificationValues) > 0) {
            $qb->andWhere('psrv.id IN (:specification_values)');
            $qb->setParameter('specification_values', $specificationValues);
        }

        if (count($brands) > 0) {
            $qb->andWhere('pb.id IN (:brands)');
            $qb->setParameter('brands', $brands);
        }

        if (!empty($searchPhrase)) {
            $searchString = trim(mb_strtolower($searchPhrase));

            $qb->andWhere('trim(lower(p.name)) LIKE :search_string or trim(lower(pt.name)) LIKE :search_string');
            $qb->setParameter('search_string', "%$searchString%");
        }

        if (!empty($marking)) {
            $qb->andWhere('p.marking = :marking');
            $qb->setParameter('marking', $marking);
        }

        if ($paginated) {
            return $qb->getQuery();
        }

        return $qb->getQuery()->getResult();
    }

    public function getRecommendedProducts(Product $product)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->where('p.marking NOT IN (:markings)');
        $qb->setParameter('markings', [Product::MARKING_SOON, Product::MARKING_PRE_ORDER]);

        $vendorParts = explode('-', $product->getVendorCode());
        if (count($vendorParts) > 1) {
            $qb->andWhere('p.vendorCode like :vendor_code');
            $qb->setParameter('vendor_code', "{$vendorParts[0]}%");
        }

        $qb->setMaxResults(10);

        return $qb->getQuery()->getResult();
    }
}