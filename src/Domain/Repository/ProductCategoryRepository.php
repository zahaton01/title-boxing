<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Product\ProductCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductCategory::class);
    }

    public function getList(?string $search = null)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->leftJoin('c.translations', 'ct');
        $qb->orderBy('c.id', 'DESC');

        if (null !== $search && $search !== '') {
            $qb->andWhere('lower(c.name) LIKE :search_string or lower(ct.name) LIKE :search_string');
            $qb->setParameter('search_string', "%$search%");
        }

        return $qb->getQuery();
    }
}