<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getListBy(?string $search = null, $paginated = true)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->orderBy('u.creationDate', 'DESC');

        if (!empty($search)) {
            $qb->andWhere('
                lower(u.name) LIKE :search_string 
                or lower(u.surname) LIKE :search_string 
                or lower(u.phone) LIKE :search_string
                or lower(u.login) LIKE :search_string
                or lower(u.email) LIKE :search_string
            ');
            $qb->setParameter('search_string', "%$search%");
        }

        if ($paginated) {
            return $qb->getQuery();
        }

        return $qb->getQuery()->getResult();
    }
}