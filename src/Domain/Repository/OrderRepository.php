<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Commerce\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getListBy(
        ?string $search = null,
        ?\DateTimeInterface $startDate = null,
        ?\DateTimeImmutable $endDate = null,
        ?string $status = null,
        ?string $orderNumber = null,
        $paginated = true
    ) {
        $qb = $this->createQueryBuilder('o');
        $qb->orderBy('o.creationDate', 'DESC');
        $qb->leftJoin('o.user', 'ou');

        if (!empty($search)) {
            $qb->andWhere('
                lower(o.name) LIKE :search_string 
                or lower(o.surname) LIKE :search_string 
                or lower(o.phone) LIKE :search_string
            ');
            $qb->setParameter('search_string', "%$search%");
        }

        if (!empty($orderNumber)) {
            $qb->andWhere('o.orderNumber LIKE :order_number');
            $qb->setParameter('order_number', "%$orderNumber%");
        }

        if (null !== $startDate) {
            $qb->andWhere('o.creationDate >= :start_date');
            $qb->setParameter('start_date', $startDate);
        }

        if (null !== $endDate) {
            $qb->andWhere('o.creationDate <= :end_date');
            $qb->setParameter('end_date', $endDate);
        }

        if (!empty($status)) {
            $qb->andWhere('o.status = :status');
            $qb->setParameter('status', $status);
        }

        if ($paginated) {
            return $qb->getQuery();
        }

        return $qb->getQuery()->getResult();
    }
}