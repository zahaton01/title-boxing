<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Site\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function getList(?string $search = null)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->leftJoin('p.translations', 'pt');
        $qb->orderBy('p.id', 'DESC');

        if (null !== $search && $search !== '') {
            $qb->andWhere('lower(p.name) LIKE :search_string or lower(pt.name) LIKE :search_string');
            $qb->setParameter('search_string', "%$search%");
        }

        return $qb->getQuery();
    }
}