<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Product\ProductComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class ProductCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductComment::class);
    }

    public function getList(string $productId): Query
    {
        $qb = $this->createQueryBuilder('c');
        $qb->leftJoin('c.product', 'cp');
        $qb->andWhere('cp.id = :product_id');
        $qb->setParameter('product_id', $productId);

        $qb->orderBy('c.creationDate', 'DESC');

        return $qb->getQuery();
    }
}