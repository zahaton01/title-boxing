<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Product\Specification\Specification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class SpecificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Specification::class);
    }

    /**
     * @return Specification[]
     */
    public function getListByCategories(array $categoryIds = [])
    {
        $qb = $this->createQueryBuilder('s');
        $qb->leftJoin('s.categories', 'sc');

        $qb->where('sc.id IN (:categories)');
        $qb->setParameter('categories', $categoryIds);

        $qb->andWhere('s.isVisible = :is_visible');
        $qb->setParameter('is_visible', true);

        return $qb->getQuery()->getResult();
    }

    public function getList(?string $search = null)
    {
        $qb = $this->createQueryBuilder('s');
        $qb->leftJoin('s.translations', 'st');
        $qb->orderBy('s.id', 'DESC');

        if (null !== $search && $search !== '') {
            $qb->andWhere('lower(s.name) LIKE :search_string or lower(st.name) LIKE :search_string');
            $qb->setParameter('search_string', "%$search%");
        }

        return $qb->getQuery();
    }
}