<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Menu\MenuLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MenuLinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuLink::class);
    }

    public function getHighLevelMenuLinks()
    {
        $qb = $this->createQueryBuilder('l');
        $qb->where('l.staticParent IN (:high_level_parents)');
        $qb->setParameter('high_level_parents', [
            MenuLink::STATIC_PARENT_FOOTER,
            MenuLink::STATIC_PARENT_DESKTOP_HEADER_CATEGORIES,
            MenuLink::STATIC_PARENT_DESKTOP_HEADER,
            MenuLink::STATIC_PARENT_MOBILE_DROPDOWN
        ]);

        $qb->andWhere('l.isVisible = :is_visible');
        $qb->setParameter('is_visible', true);
        $qb->orderBy('l.orderIndex', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function getList(?string $search = null, ?string $staticParent = null): Query
    {
        $qb = $this->createQueryBuilder('l');
        $qb->orderBy('l.id', 'DESC');

        if (null !== $search && $search !== '') {
            $qb->andWhere('lower(l.name) LIKE :search_string or lower(l.url) LIKE :search_string');
            $qb->setParameter('search_string', "%$search%");
        }

        if (!empty($staticParent)) {
            $qb->andWhere('l.staticParent = :static_parent');
            $qb->setParameter('static_parent', $staticParent);
        }

        return $qb->getQuery();
    }
}