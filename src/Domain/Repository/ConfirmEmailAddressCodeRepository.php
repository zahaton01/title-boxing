<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User\ConfirmEmailAddressCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ConfirmEmailAddressCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfirmEmailAddressCode::class);
    }
}