<?php

namespace App\Cli\User;

use App\Domain\Entity\User\Admin;
use App\Infrastructure\Security\Roles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class CreateAdminCommand extends Command
{
    private EntityManagerInterface $em;
    private ContainerInterface $container;

    public function __construct(EntityManagerInterface $em, ContainerInterface $container)
    {
        parent::__construct();

        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName('user-ui:create:admin')
            ->addArgument('login', InputArgument::REQUIRED, 'Login of the account')
            ->addArgument('password', InputArgument::REQUIRED, 'Password of the account');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Creating...');

        $encoder = $this->container->get('security.password_encoder');

        $user = new Admin();
        $user->setLogin($input->getArgument('login'));
        $user->setRoles([Roles::ADMIN]);
        $user->setPassword($encoder->encodePassword($user, $input->getArgument('password')));

        $this->em->persist($user);
        $this->em->flush();

        $output->writeln('Done');

        return 1;
    }
}