<?php

namespace App\Cli;

use App\Domain\Entity\Site\Page;
use App\Domain\Repository\PageRepository;
use App\Infrastructure\Locales;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class CreatePagesCommand extends Command
{
    private EntityManagerInterface $em;
    private PageRepository $pageRepository;

    public function __construct(EntityManagerInterface $em, PageRepository $pageRepository)
    {
        parent::__construct();

        $this->em = $em;
        $this->pageRepository = $pageRepository;
    }

    public function configure()
    {
        $this->setName('pages:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reflection = new \ReflectionClass(Page::class);
        foreach ($reflection->getConstants() as $key => $constant) {
            if (str_starts_with($key, 'STATIC_TYPE')) {
                if ($this->pageRepository->findOneBy(['staticType' => $constant])) {
                    continue;
                }

                $page = new Page();
                $page->setIsStatic(true);
                $page->setStaticType($constant);
                $page->setSlug(uniqid());
                $page->setName(ucfirst(explode('_', $constant)[1]));
                $page->setLocale(Locales::RU);

                $this->em->persist($page);
            }
        }

        $this->em->flush();

        return 1;
    }
}