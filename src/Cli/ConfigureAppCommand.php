<?php

namespace App\Cli;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;

class ConfigureAppCommand extends Command
{
    private static array $commandsToExecute = [
        'static-vars:create',
        'pages:create'
    ];

    private ParameterBagInterface $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        parent::__construct();

        $this->parameterBag = $parameterBag;
    }

    public function configure()
    {
        $this->setName('app:configure');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $projectDir = $this->parameterBag->get('kernel.project_dir');
        $commandBin = "$projectDir/bin/console";

        foreach (static::$commandsToExecute as $cmd) {
            $io->section(
                sprintf(
                    "[%s] Running command `%s`...",
                    (new \DateTime())->format(\DateTime::RFC3339),
                    $cmd
                )
            );

            $process = new Process(['php', '--define', 'memory_limit=2048M', $commandBin, $cmd]);
            $process->setTimeout(null);
            try {
                $process->run();
                $io->text($process->getOutput());
            } catch (\Throwable $e) {
                $io->error($e->getMessage());
            }
        }

        return 1;
    }
}