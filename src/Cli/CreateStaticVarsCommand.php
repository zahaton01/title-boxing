<?php

namespace App\Cli;

use App\Domain\Entity\Site\StaticVar;
use App\Domain\Repository\StaticVarRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class CreateStaticVarsCommand extends Command
{
    private EntityManagerInterface $em;
    private StaticVarRepository $staticVarRepository;

    public function __construct(EntityManagerInterface $em, StaticVarRepository $staticVarRepository)
    {
        parent::__construct();

        $this->em = $em;
        $this->staticVarRepository = $staticVarRepository;
    }

    public function configure()
    {
        $this->setName('static-vars:create');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reflection = new \ReflectionClass(StaticVar::class);
        foreach ($reflection->getConstants() as $key => $constant) {
            if (str_starts_with($key, 'KEY')) {
                if ($this->staticVarRepository->findOneBy(['externalKey' => $constant])) {
                    continue;
                }

                $variable = new StaticVar();
                $variable->setValue('');
                $variable->setExternalKey($constant);

                $this->em->persist($variable);
            }
        }

        $this->em->flush();

        return 1;
    }
}