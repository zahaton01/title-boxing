<?php

namespace App\Http\Response\Normalizer;

use App\Domain\Entity\Product\ProductComment;

class ProductCommentNormalizer
{
    public function normalize(ProductComment $comment): array
    {
        $_ = [];

        $_['user'] = $comment->getAuthor() ? [] : null;
        if (is_array($_['user'])) {
            $_['user']['id'] = $comment->getAuthor()->getId();
            $_['user']['username'] = $comment->getAuthor()->getUsername();
        }

        $_['id'] = $comment->getId();
        $_['author_name'] = $comment->getAuthorName();
        $_['text'] = $comment->getText();
        $_['stars'] = $comment->getStars();

        return $_;
    }

    public function normalizeMany(ProductComment ...$comments): array
    {
        $normalized = [];
        foreach ($comments as $comment) {
            $normalized[] = $this->normalize($comment);
        }

        return $normalized;
    }
}