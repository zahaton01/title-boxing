<?php

namespace App\Http\Response\Normalizer;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Infrastructure\Service\Cart\Cart;

class CartNormalizer extends AbstractTranslatedEntityNormalizer
{
    public function normalize(Cart $cart): array
    {
        $_ = [
            'cart_products' => [],
            'total_price' => $cart->getTotalPrice()
        ];

        foreach ($cart->getProducts() as $cartProduct) {
            /** @var Product $product */
            $product = $this->trans($cartProduct->getProduct());

            $img = $product->getPreviewImage()
                ? ($product->getPreviewImage()->getFile() ? $product->getPreviewImage()->getFile()->getRelativePath() : '')
                : '';

            $normalized = [
                'related_product' => [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'img' => $img,
                    'slug' => $product->getSlug()
                ],
                'quantity' => (int) $cartProduct->getQuantity(),
                'total_price' => $cartProduct->getTotalPrice(),
                'specification_values' => [],
                'one_item_price' => $cartProduct->getOneItemPrice()
            ];

            foreach ($cartProduct->getSpecificationValues() as $productSpecificationValue) {
                /** @var SpecificationValue $relatedValue */
                $relatedValue = $this->trans($productSpecificationValue->getRelatedSpecificationValue());
                /** @var Specification $relatedSpecification */
                $relatedSpecification = $this->trans($productSpecificationValue->getRelatedSpecificationValue()->getSpecification());

                $normalized['specification_values'][] = [
                    'id' => $productSpecificationValue->getId(),
                    'availability' => $productSpecificationValue->getAvailability(),
                    'price_difference' => $productSpecificationValue->getPriceDifference(),
                    'related_value' => [
                        'id' => $relatedValue->getId(),
                        'name' => $relatedValue->getName()
                    ],
                    'related_specification' => [
                        'id' => $relatedSpecification->getId(),
                        'name' => $relatedSpecification->getName()
                    ]
                ];
            }

            $_['cart_products'][] = $normalized;
        }

        return $_;
    }
}