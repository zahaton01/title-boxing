<?php

namespace App\Http\Response\Normalizer;

use App\Domain\Entity\Product\ProductSpecification;
use App\Domain\Entity\Product\Specification\Specification;

class ProductSpecificationNormalizer extends AbstractTranslatedEntityNormalizer
{
    public function normalize(ProductSpecification $productSpecification): array
    {
        $_ = [];

        $_['id'] = $productSpecification->getId();
        $_['related_specification'] = [];

        /** @var Specification $relatedSpecification */
        $relatedSpecification = $this->trans($productSpecification->getRelatedSpecification());
        $_['related_specification']['id'] = $relatedSpecification->getId();
        $_['related_specification']['name'] = $relatedSpecification->getName();

        $_['specification_values'] = [];
        foreach ($productSpecification->getValues() as $value) {
            $productSpecificationValue = [];
            $productSpecificationValue['id'] = $value->getId();
            $productSpecificationValue['price_difference'] = $value->getPriceDifference();
            $productSpecificationValue['availability'] = $value->getAvailability();
            $productSpecificationValue['related_value'] = [];

            $relatedSpecificationValue = $this->trans($value->getRelatedSpecificationValue());
            $productSpecificationValue['related_value']['id'] = $relatedSpecificationValue->getId();
            $productSpecificationValue['related_value']['name'] = $relatedSpecificationValue->getName();

            $_['specification_values'][] = $productSpecificationValue;
        }

        return $_;
    }

    public function normalizeMany(ProductSpecification ...$productSpecifications): array
    {
        $normalized = [];
        foreach ($productSpecifications as $productSpecification) {
            $normalized[] = $this->normalize($productSpecification);
        }

        return $normalized;
    }
}