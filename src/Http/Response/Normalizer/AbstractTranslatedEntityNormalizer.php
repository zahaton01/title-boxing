<?php

namespace App\Http\Response\Normalizer;

use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class AbstractTranslatedEntityNormalizer
{
    private ?Request $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    protected function trans($entity)
    {
        $translator = new EntityTranslator($this->request->getLocale());

        return $translator->trans($entity);
    }
}