<?php

namespace App\Http\Response\Forms;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait FormMetaFields
{
    protected function addMetaFields(FormBuilderInterface $builder, TranslatorInterface $translator)
    {
        return $builder
            ->add('pageTitle', TextType::class, [
                'label' => $translator->trans('Page title'),
                'required' => false
            ])
            ->add('metaTitle', TextType::class, [
                'label' => $translator->trans('Meta title'),
                'required' => false
            ])
            ->add('metaDescription', TextType::class, [
                'label' => $translator->trans('Meta description'),
                'required' => false
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => $translator->trans('Meta keywords'),
                'required' => false
            ]);
    }
}