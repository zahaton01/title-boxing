<?php

namespace App\Http\Response\Forms\Types\Order;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Entity\User\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class AdminOrderType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true,
                ]
            )
            ->add(
                'surname',
                TextType::class,
                [
                    'label' => $this->translator->trans('Surname'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Surname')
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'phone',
                TelType::class,
                [
                    'label' => $this->translator->trans('Phone'),
                    'attr' => [
                        'placeholder' => '+38 (050) 858-55-11'
                    ],
                    'required' => true,
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => $this->translator->trans('Email'),
                    'attr' => [
                        'placeholder' => 'test@example.com'
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'deliveryCity',
                TextType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('City')
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'deliveryAddress',
                TextType::class,
                [
                    'label' => $this->translator->trans('Address'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Address')
                    ],
                    'required' => false,
                ]
            )
            ->add(
                'novaPoshtaCity',
                TextType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'required' => false,
                ]
            )
            ->add(
                'novaPoshtaWarehouse',
                TextType::class,
                [
                    'label' => $this->translator->trans('Warehouse'),
                    'required' => false,
                ]
            )
            ->add(
                'paymentType',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Payment type'),
                    'required' => true,
                    'choices' => [
                        $this->translator->trans('Cash') => Order::PAYMENT_TYPE_CASH,
                        $this->translator->trans('Online') => Order::PAYMENT_TYPE_ONLINE,
                    ]
                ]
            )
            ->add(
                'deliveryType',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Delivery type'),
                    'required' => true,
                    'choices' => [
                        $this->translator->trans('Courier') => Order::DELIVERY_TYPE_COURIER,
                        $this->translator->trans('Nova Poshta warehouse') => Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE,
                    ]
                ]
            )
            ->add('user', EntityType::class, [
                'label' => $this->translator->trans('user'),
                'class' => User::class,
                'choice_label' => function (User $user) {
                    return $user->getName() . ' ' . $user->getSurname() . " ({$user->getId()})";
                },
                'group_by' => function($choice, $key, $value) {
                    return mb_strtoupper(mb_substr((string) $choice->getName(), 0, 1)); // group by first letter
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
            ])
            ->add(
                'status',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('status'),
                    'required' => true,
                    'choices' => [
                        $this->translator->trans(Order::STATUS_NEW) => Order::STATUS_NEW,
                        $this->translator->trans(Order::STATUS_DELIVERING) => Order::STATUS_DELIVERING,
                        $this->translator->trans(Order::STATUS_DONE) => Order::STATUS_DONE,
                        $this->translator->trans(Order::STATUS_CONFIRMED) => Order::STATUS_CONFIRMED,
                    ]
                ]
            )
            ->add(
                'comment',
                TextareaType::class,
                [
                    'label' => $this->translator->trans('Comment'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Comment')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => Order::class
            ]
        );
    }
}