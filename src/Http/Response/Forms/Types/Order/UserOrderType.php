<?php

namespace App\Http\Response\Forms\Types\Order;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Entity\User\Admin;
use App\Domain\Entity\User\User;
use App\Infrastructure\ExternalService\NovaPoshtaClient;
use App\Infrastructure\Locales;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class UserOrderType extends AbstractType
{
    private string $locale;
    private TranslatorInterface $translator;
    private Security $security;
    private NovaPoshtaClient $novaPoshtaClient;

    public function __construct(
        TranslatorInterface $translator,
        Security $security,
        NovaPoshtaClient $novaPoshtaClient,
        RequestStack $requestStack
    ) {
        $this->translator = $translator;
        $this->security = $security;
        $this->novaPoshtaClient = $novaPoshtaClient;
        $this->locale = $requestStack->getCurrentRequest()->getLocale();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User|null $user */
        $user = $this->security->getUser();

        $mappedNovaCities = [];
        $novaCities = $this->novaPoshtaClient->getCities();

        foreach ($novaCities as $novaCity) {
            $cityName = $novaCity['Description'];

            if ($this->locale === Locales::RU) {
                $cityName = $novaCity['DescriptionRu'];
            }

            $mappedNovaCities[$cityName] = $novaCity['Ref'];
        }

        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true,
                    'data' => ($user && !$user instanceof Admin) ? $user->getName() : null
                ]
            )
            ->add(
                'surname',
                TextType::class,
                [
                    'label' => $this->translator->trans('Surname'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Surname')
                    ],
                    'required' => false,
                    'data' => ($user && !$user instanceof Admin) ? $user->getSurname() : null
                ]
            )
            ->add(
                'phone',
                TelType::class,
                [
                    'label' => $this->translator->trans('Phone'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => '+38 (050) 858-55-11'
                    ],
                    'required' => true,
                    'data' => ($user && !$user instanceof Admin) ? $user->getPhone() : null
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => $this->translator->trans('Email'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => 'test@example.com'
                    ],
                    'required' => false,
                    'data' => ($user && !$user instanceof Admin) ? $user->getEmail() : null
                ]
            )
            ->add(
                'deliveryCity',
                TextType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('City')
                    ],
                    'required' => false,
                    'data' => ($user && !$user instanceof Admin) ? $user->getCity() : null
                ]
            )
            ->add(
                'deliveryAddress',
                TextType::class,
                [
                    'label' => $this->translator->trans('Address'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Address')
                    ],
                    'required' => false,
                    'data' => ($user && !$user instanceof Admin) ? $user->getAddress() : null
                ]
            )
            ->add(
                'novaPoshtaCity',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'choices' => $mappedNovaCities
                ]
            )
            ->add(
                'novaPoshtaWarehouse',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Warehouse'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'choices' => []
                ]
            )
            ->add(
                'paymentType',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Payment type'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => true,
                    'choices' => [
                        $this->translator->trans('Cash') => Order::PAYMENT_TYPE_CASH,
                        $this->translator->trans('Online') => Order::PAYMENT_TYPE_ONLINE,
                    ]
                ]
            )
            ->add(
                'deliveryType',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Delivery type'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => true,
                    'choices' => [
                        $this->translator->trans('Courier') => Order::DELIVERY_TYPE_COURIER,
                        $this->translator->trans('Nova Poshta warehouse') => Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE,
                    ]
                ]
            )
            ->add(
                'comment',
                TextareaType::class,
                [
                    'label' => $this->translator->trans('Comment'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Comment')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Confirm order'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        $builder->get('novaPoshtaWarehouse')->resetViewTransformers();
        $builder->get('novaPoshtaCity')->resetViewTransformers();

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => Order::class
            ]
        );
    }
}