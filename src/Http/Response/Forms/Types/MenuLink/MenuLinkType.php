<?php

namespace App\Http\Response\Forms\Types\MenuLink;

use App\Domain\Entity\Menu\MenuLink;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MenuLinkType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExisted($builder->getData()),
                'required' => true
            ])
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'url',
                TextType::class,
                [
                    'label' => $this->translator->trans('Url'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Url')
                    ],
                    'help' => $this->translator->trans('Relative path (/uk/catalog) or absolute path (https://titleboxing.com/uk/catalog)'),
                    'required' => true
                ]
            )
            ->add(
                'orderIndex',
                IntegerType::class,
                [
                    'label' => $this->translator->trans('Order index'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Order index')
                    ],
                    'help' => $this->translator->trans('Sort priority'),
                    'required' => false,
                    'empty_data' => 0
                ]
            )
            ->add(
                'isVisible',
                CheckboxType::class,
                [
                    'label' => $this->translator->trans('Is visible'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Is visible')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'staticParent',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Static parent'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Static parent')
                    ],
                    'help' => $this->translator->trans('Parent element of the menu link. ex. header menu.'),
                    'required' => false,
                    'choices' => [
                        $this->translator->trans(MenuLink::STATIC_PARENT_DESKTOP_HEADER) => MenuLink::STATIC_PARENT_DESKTOP_HEADER,
                        $this->translator->trans(MenuLink::STATIC_PARENT_DESKTOP_HEADER_CATEGORIES) => MenuLink::STATIC_PARENT_DESKTOP_HEADER_CATEGORIES,
                        $this->translator->trans(MenuLink::STATIC_PARENT_FOOTER) => MenuLink::STATIC_PARENT_FOOTER,
                        $this->translator->trans(MenuLink::STATIC_PARENT_MOBILE_DROPDOWN) => MenuLink::STATIC_PARENT_MOBILE_DROPDOWN,
                    ]
                ]
            )
            ->add('parent', EntityType::class, [
                'label' => $this->translator->trans('Parent link'),
                'class' => MenuLink::class,
                'choice_label' => function (MenuLink $link) {
                    return $link->getName() . " ({$link->getUrl()})";
                },
                'group_by' => function($choice, $key, $value) {
                    return mb_strtoupper(mb_substr((string) $choice->getName(), 0, 1)); // group by first letter
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('l')
                        ->orderBy('l.name', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ])
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => MenuLink::class
            ]
        );
    }
}