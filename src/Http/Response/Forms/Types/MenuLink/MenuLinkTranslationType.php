<?php

namespace App\Http\Response\Forms\Types\MenuLink;

use App\Domain\Entity\Menu\MenuLinkTranslation;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MenuLinkTranslationType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExistedForTranslation($builder->getData(), $options['menu_link']),
                'required' => true
            ])
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'url',
                TextType::class,
                [
                    'label' => $this->translator->trans('Url'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Url')
                    ],
                    'help' => $this->translator->trans('Relative path (/uk/catalog) or absolute path (https://titleboxing.com/uk/catalog)'),
                    'required' => true
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => MenuLinkTranslation::class,
                'menu_link' => null
            ]
        );
    }
}