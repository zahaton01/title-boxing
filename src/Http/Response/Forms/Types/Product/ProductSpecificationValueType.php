<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Product\ProductSpecificationValue;
use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Infrastructure\Availability;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductSpecificationValueType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('relatedSpecificationValue', EntityType::class, [
                'label' => $this->translator->trans('Related specification value'),
                'class' => SpecificationValue::class,
                'choice_label' => function (SpecificationValue $specificationValue) {
                    return $specificationValue->getName();
                },
                'query_builder' => function (EntityRepository $er) use($options) {
                    return $er->createQueryBuilder('sv')
                        ->orderBy('sv.name', 'ASC')
                        ->andWhere('sv.specification = :specification')
                        ->setParameter('specification', $options['related_specification']);
                },
                'required' => true,
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ])
            ->add('availability', ChoiceType::class, [
                'label' => $this->translator->trans('Availability'),
                'required' => true,
                'choices' => [
                    $this->translator->trans(Availability::AVAILABLE) => Availability::AVAILABLE,
                    $this->translator->trans(Availability::NOT_AVAILABLE) => Availability::NOT_AVAILABLE
                ],
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ])
            ->add('priceDifference', IntegerType::class, [
                'label' => $this->translator->trans('Price difference'),
                'help' => $this->translator->trans('Positive or negative integer'),
                'required' => false,
                'empty_data' => null
            ]);


        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => ProductSpecificationValue::class,
                'related_specification' => null
            ]
        );
    }
}