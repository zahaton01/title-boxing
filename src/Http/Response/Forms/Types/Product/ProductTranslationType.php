<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Product\ProductTranslation;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use App\Http\Response\Forms\FormMetaFields;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductTranslationType extends AbstractType
{
    use FormMetaFields;

    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExistedForTranslation($builder->getData(), $options['product']),
                'required' => true
            ])
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'description',
                TextType::class,
                [
                    'label' => $this->translator->trans('Description'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Description')
                    ],
                    'required' => false
                ]
            );

        $builder = $this->addMetaFields($builder, $this->translator);

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => ProductTranslation::class,
                'product' => null
            ]
        );
    }
}