<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Media\MediaFile;
use App\Domain\Entity\Product\ProductImageTranslation;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductImageTranslationType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExistedForTranslation($builder->getData(), $options['image']),
                'required' => true
            ])
            ->add(
                'altText',
                TextType::class,
                [
                    'label' => $this->translator->trans('Alternative text'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Alternative text')
                    ],
                    'required' => false
                ]
            )
            ->add('file', EntityType::class, [
                'label' => $this->translator->trans('File'),
                'class' => MediaFile::class,
                'choice_label' => function (MediaFile $file) {
                    return $file->getRelativePath();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.relativePath', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-file-selectize'
                ]
            ]);

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => ProductImageTranslation::class,
                'image' => null
            ]
        );
    }
}