<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Commerce\Brand;
use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductCategory;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use App\Http\Response\Forms\FormMetaFields;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductType extends AbstractType
{
    use FormMetaFields;

    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExisted($builder->getData()),
                'required' => true
            ])
            ->add(
                'vendorCode',
                TextType::class,
                [
                    'label' => $this->translator->trans('Vendor code'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Vendor code')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'description',
                TextType::class,
                [
                    'label' => $this->translator->trans('Description'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Description')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'slug',
                TextType::class,
                [
                    'label' => $this->translator->trans('Slug'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Slug')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'orderIndex',
                IntegerType::class,
                [
                    'label' => $this->translator->trans('Order index'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Order index')
                    ],
                    'help' => $this->translator->trans('Sort priority'),
                    'required' => false,
                    'empty_data' => 0
                ]
            )
            ->add(
                'isVisible',
                CheckboxType::class,
                [
                    'label' => $this->translator->trans('Is visible'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Is visible')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'price',
                MoneyType::class,
                [
                    'label' => $this->translator->trans('Price'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Price')
                    ],
                    'required' => true,
                    'currency' => 'UAH'
                ]
            )
            ->add(
                'salePrice',
                MoneyType::class,
                [
                    'label' => $this->translator->trans('Sale price'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Sale price')
                    ],
                    'required' => false,
                    'currency' => 'UAH'
                ]
            )
            ->add(
                'marking',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Special marking'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Special marking')
                    ],
                    'required' => false,
                    'choices' => [
                        $this->translator->trans(Product::MARKING_NONE) => Product::MARKING_NONE,
                        $this->translator->trans(Product::MARKING_NEW) => Product::MARKING_NEW,
                        $this->translator->trans(Product::MARKING_SALE) => Product::MARKING_SALE,
                        $this->translator->trans(Product::MARKING_TOP) => Product::MARKING_TOP,
                        $this->translator->trans(Product::MARKING_SOON) => Product::MARKING_SOON,
                        $this->translator->trans(Product::MARKING_PRE_ORDER) => Product::MARKING_PRE_ORDER
                    ]
                ]
            )
            ->add('categories', EntityType::class, [
                'label' => $this->translator->trans('Parent category'),
                'class' => ProductCategory::class,
                'choice_label' => function (ProductCategory $productCategory) {
                    return $productCategory->getName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'multiple' => true,
                'attr' => [
                    'class' => 'js-multiple-selectize'
                ]
            ])
            ->add('brand', EntityType::class, [
                'label' => $this->translator->trans('Brand'),
                'class' => Brand::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('b')
                        ->orderBy('b.name', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ]);

        $builder = $this->addMetaFields($builder, $this->translator);

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => Product::class
            ]
        );
    }
}