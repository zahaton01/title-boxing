<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Product\ProductSpecification;
use App\Domain\Entity\Product\Specification\Specification;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProductSpecificationType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('relatedSpecification', EntityType::class, [
                'label' => $this->translator->trans('Related specification'),
                'class' => Specification::class,
                'choice_label' => function (Specification $specification) {
                    return $specification->getName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC'); // in alphabetical order
                },
                'required' => true,
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ]);


        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => ProductSpecification::class,
            ]
        );
    }
}