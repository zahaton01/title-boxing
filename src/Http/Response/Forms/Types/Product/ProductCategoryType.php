<?php

namespace App\Http\Response\Forms\Types\Product;

use App\Domain\Entity\Product\ProductCategory;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use App\Http\Response\Forms\FormMetaFields;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ProductCategoryType extends AbstractType
{
    use FormMetaFields;

    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExisted($builder->getData()),
                'required' => true
            ])
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'slug',
                TextType::class,
                [
                    'label' => $this->translator->trans('Slug'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Slug')
                    ],
                    'required' => true
                ]
            )
            ->add('category', EntityType::class, [
                'label' => $this->translator->trans('Parent category'),
                'class' => ProductCategory::class,
                'choice_label' => function (ProductCategory $productCategory) {
                    return $productCategory->getName();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-selectize'
                ]
            ])
            ->add(
                'isVisible',
                CheckboxType::class,
                [
                    'label' => $this->translator->trans('Is visible'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Is visible')
                    ],
                    'required' => false
                ]
            );

        $builder = $this->addMetaFields($builder, $this->translator);

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => $this->translator->trans('Save'),
                'attr' => ['class' => 'btn btn-dark']
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => ProductCategory::class,
            ]
        );
    }
}