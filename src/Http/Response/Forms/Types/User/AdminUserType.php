<?php

namespace App\Http\Response\Forms\Types\User;

use App\Domain\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class AdminUserType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('Name'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Name')
                ]
            ])
            ->add('surname', TextType::class, [
                'label' => $this->translator->trans('Surname'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Surname')
                ],
                'required' => false
            ])
            ->add('login', TextType::class, [
                'label' => $this->translator->trans('user_login'),
                'attr' => [
                    'placeholder' => $this->translator->trans('user_login')
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => $this->translator->trans('Email'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Email')
                ]
            ])
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => $this->translator->trans('Phone'),
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('Phone')
                    ]
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('City')
                    ]
                ]
            )
            ->add(
                'address',
                TextType::class,
                [
                    'label' => $this->translator->trans('Address'),
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('Address')
                    ]
                ]
            )
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Gender'),
                    'choices' => [
                        $this->translator->trans('Male') => User::GENDER_MALE,
                        $this->translator->trans('Female') => User::GENDER_FEMALE,
                        $this->translator->trans('Unknown') => User::GENDER_UNKNOWN,
                    ]
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => User::class
            ]
        );
    }
}