<?php

namespace App\Http\Response\Forms\Types\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResetPasswordType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('new_password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => $this->translator->trans('The password fields must match'),
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => [
                    'label' => $this->translator->trans('New password'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('New password'),
                    ]
                ],
                'second_options' => [
                    'label' => $this->translator->trans('Repeat password'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Repeat password')
                    ]
                ],
            ])
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Reset password'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true
            ]
        );
    }
}