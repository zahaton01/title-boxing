<?php

namespace App\Http\Response\Forms\Types\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('Name'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Name')
                ]
            ])
            ->add('surname', TextType::class, [
                'label' => $this->translator->trans('Surname'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Surname')
                ],
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => $this->translator->trans('Email'),
                'attr' => [
                    'placeholder' => $this->translator->trans('Email')
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => $this->translator->trans('The password fields must match'),
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => [
                    'label' => $this->translator->trans('Password'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Password')
                    ]
                ],
                'second_options' => [
                    'label' => $this->translator->trans('Repeat password'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Repeat password')
                    ]
                ],
            ])
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Sign Up'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true
            ]
        );
    }
}