<?php

namespace App\Http\Response\Forms\Types\User;

use App\Domain\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProfileType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'surname',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'required' => false,
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ]
                ]
            )
            ->add(
                'birthday',
                TextType::class,
                [
                    'label' => $this->translator->trans('Birthday'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('Birthday')
                    ]
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => $this->translator->trans('Email'),
                    'label_attr' => ['class' => 'sub-title'],
                    'attr' => [
                        'placeholder' => $this->translator->trans('Email')
                    ]
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => $this->translator->trans('Phone'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('Phone')
                    ]
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => $this->translator->trans('City'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('City')
                    ]
                ]
            )
            ->add(
                'address',
                TextType::class,
                [
                    'label' => $this->translator->trans('Address'),
                    'label_attr' => ['class' => 'sub-title'],
                    'required' => false,
                    'attr' => [
                        'placeholder' => $this->translator->trans('Address')
                    ]
                ]
            )
            ->add(
                'gender',
                ChoiceType::class,
                [
                    'label' => $this->translator->trans('Gender'),
                    'label_attr' => ['class' => 'sub-title'],
                    'choices' => [
                        $this->translator->trans('Male') => User::GENDER_MALE,
                        $this->translator->trans('Female') => User::GENDER_FEMALE,
                        $this->translator->trans('Unknown') => User::GENDER_UNKNOWN,
                    ]
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        $builder->get('birthday')
            ->addModelTransformer(
                new CallbackTransformer(
                    function ($birthdayAsDate) {
                        return $birthdayAsDate ? $birthdayAsDate->format('d-m-Y') : null;
                    },
                    function ($birthdayAsString) {
                        return new \DateTimeImmutable($birthdayAsString);
                    }
                )
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => User::class
            ]
        );
    }
}