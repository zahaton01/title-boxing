<?php

namespace App\Http\Response\Forms\Types\Brand;

use App\Domain\Entity\Commerce\Brand;
use App\Domain\Entity\Media\MediaFile;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class BrandType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExisted($builder->getData()),
                'required' => true
            ])
            ->add(
                'name',
                TextType::class,
                [
                    'label' => $this->translator->trans('Name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Name')
                    ],
                    'required' => true
                ]
            )
            ->add(
                'altText',
                TextType::class,
                [
                    'label' => $this->translator->trans('Alternative text'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Alternative text')
                    ],
                    'required' => false
                ]
            )
            ->add('image', EntityType::class, [
                'label' => $this->translator->trans('Image'),
                'class' => MediaFile::class,
                'choice_label' => function (MediaFile $file) {
                    return $file->getRelativePath();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.relativePath', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-file-selectize'
                ]
            ])
            ->add(
                'isVisible',
                CheckboxType::class,
                [
                    'label' => $this->translator->trans('Is visible'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Is visible')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => Brand::class,
            ]
        );
    }
}