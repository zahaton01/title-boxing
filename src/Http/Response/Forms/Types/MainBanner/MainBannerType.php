<?php

namespace App\Http\Response\Forms\Types\MainBanner;

use App\Domain\Entity\Media\MediaFile;
use App\Domain\Entity\Site\MainBanner;
use App\Http\Response\Forms\ChoiceBuilders\LocaleChoices;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MainBannerType extends AbstractType
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locale', ChoiceType::class, [
                'label' => 'Locale',
                'choices' => LocaleChoices::filterExisted($builder->getData()),
                'required' => true
            ])
            ->add(
                'altText',
                TextType::class,
                [
                    'label' => $this->translator->trans('Alternative text'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Alternative text')
                    ],
                    'required' => false
                ]
            )
            ->add('file', EntityType::class, [
                'label' => $this->translator->trans('File'),
                'class' => MediaFile::class,
                'choice_label' => function (MediaFile $file) {
                    return $file->getRelativePath();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('f')
                        ->orderBy('f.relativePath', 'ASC'); // in alphabetical order
                },
                'required' => false,
                'empty_data' => null,
                'attr' => [
                    'class' => 'js-file-selectize'
                ]
            ])
            ->add(
                'orderIndex',
                IntegerType::class,
                [
                    'label' => $this->translator->trans('Order index'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Order index')
                    ],
                    'help' => $this->translator->trans('Sort priority'),
                    'required' => false,
                    'empty_data' => 0
                ]
            )
            ->add(
                'isVisible',
                CheckboxType::class,
                [
                    'label' => $this->translator->trans('Is visible'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('Is visible')
                    ],
                    'required' => false
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => $this->translator->trans('Save'),
                    'attr' => ['class' => 'btn btn-dark']
                ]
            );

        return $builder;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => MainBanner::class,
            ]
        );
    }
}