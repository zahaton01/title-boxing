<?php

namespace App\Http\Response\Forms\ChoiceBuilders;


use App\Infrastructure\Locales;

class LocaleChoices
{
    public static function filterExistedForTranslation($translationEntity, $parent)
    {
        $existingLocales = [$parent->getLocale()];
        foreach ($parent->getTranslations() as $translation) {
            if ($translationEntity->getId() === $translation->getId()) {
                continue;
            }

            $existingLocales[] = $translation->getLocale();
        }

        $notExistedLocales = array_diff([Locales::UK, Locales::RU, Locales::UA], $existingLocales);
        $locales = [];
        foreach ($notExistedLocales as $notExistedLocale) {
            $locales[Locales::NAMES[$notExistedLocale]] = $notExistedLocale;
        }

        return $locales;
    }

    public static function filterExisted($entity)
    {
        $existingLocales = [];
        foreach ($entity->getTranslations() as $translation) {
            $existingLocales[] = $translation->getLocale();
        }

        $notExistedLocales = array_diff([Locales::UK, Locales::RU, Locales::UA], $existingLocales);
        $locales = [];
        foreach ($notExistedLocales as $notExistedLocale) {
            $locales[Locales::NAMES[$notExistedLocale]] = $notExistedLocale;
        }

        return $locales;
    }

    public static function all()
    {
        return [
            Locales::NAMES[Locales::RU] => Locales::RU,
            Locales::NAMES[Locales::UK] => Locales::UK,
            Locales::NAMES[Locales::UA] => Locales::UA
        ];
    }
}