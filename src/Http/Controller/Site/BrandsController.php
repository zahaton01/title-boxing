<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Site\Page;
use App\Domain\Repository\BrandRepository;
use App\Domain\Repository\PageRepository;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/brands", name="site_brands_index")
 */
class BrandsController extends AbstractSiteController
{
    public function __invoke(Request $request, PageRepository $pageRepository, BrandRepository $brandRepository)
    {
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_BRANDS]);
        $page = (new EntityTranslator($request->getLocale()))->trans($page);
        $brands = $brandRepository->findBy(['isVisible' => true]);

        return $this->renderTemplate('site/pages/brands/index.html.twig', [
            'page' => $page,
            'brands' => $brands
        ]);
    }
}