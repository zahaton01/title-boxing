<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Entity\Site\Page;
use App\Domain\Entity\User\Admin;
use App\Domain\Repository\OrderRepository;
use App\Domain\Repository\PageRepository;
use App\Http\Response\Forms\Types\Order\UserOrderType;
use App\Infrastructure\ExternalService\NovaPoshtaClient;
use App\Infrastructure\Locales;
use App\Infrastructure\Mailer\Email\AdminOrderCreatedEmail;
use App\Infrastructure\Mailer\SimpleMailer;
use App\Infrastructure\Service\Cart\SessionCart;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/checkout", name="site_checkout_")
 */
class CheckoutController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     */
    public function index(
        Request $request,
        SessionCart $sessionCart,
        NovaPoshtaClient $novaPoshtaClient,
        PageRepository $pageRepository,
        SessionInterface $session,
        SimpleMailer $mailer
    ) {
        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_CHECKOUT]);

        $cart = $sessionCart->get();
        if ($cart->isEmpty()) {
            return $this->redirectToRoute('site_cart_index');
        }

        $form = $this->createForm(UserOrderType::class, new Order());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Order $order */
            $order = $form->getData();
            $order->setOrderNumber(bin2hex(random_bytes(3)));
            $order->setCart($sessionCart->getProductBag());
            $order->setUser($this->getUser() instanceof Admin ? null : $this->getUser());

            if ($order->getDeliveryType() === Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE) {
                $novaCity = current($novaPoshtaClient->getCityByRef($order->getNovaPoshtaCity()));

                $novaCityName = $novaCity['Description'];
                if ($request->getLocale() === Locales::RU) {
                    $novaCity['DescriptionRu'];
                }

                $order->setNovaPoshtaCity($novaCityName);
            }

            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            $sessionCart->clear();
            $session->set('order', $order->getId());

            $email = new AdminOrderCreatedEmail($order);
            $mailer->send($email);

            return $this->redirectToRoute('site_checkout_order_created');
        }

        return $this->renderTemplate(
            'site/pages/checkout/index.html.twig',
            [
                'cart' => $cart,
                'form' => $form->createView(),
                'meta' => $translator->trans($page)
            ]
        );
    }

    /**
     * @Route("/order-created", name="order_created")
     */
    public function orderCreated(
        Request $request,
        PageRepository $pageRepository,
        OrderRepository $orderRepository,
        SessionInterface $session
    ) {
        $translator = new EntityTranslator($request->getLocale());

        /** @var Order $order */
        $order = $orderRepository->find($session->get('order', 0));
        if (null === $order) {
            throw new NotFoundHttpException();
        }

        $page = $order->isOnlinePayment() ? Page::STATIC_TYPE_CHECKOUT_ONLINE_ORDER_THANK_YOU : Page::STATIC_TYPE_CHECKOUT_THANK_YOU;
        $page = $pageRepository->findOneBy(['staticType' => $page]);

        return $this->renderTemplate(
            'site/pages/checkout/thank_you.html.twig',
            [
                'page' => $translator->trans($page),
                'order' => $order
            ]
        );
    }

    /**
     * @Route("/nova/cities/{cityRef}/warehouses", name="get_nova_warehouses", methods={"GET"})
     */
    public function getNovaWarehouses(string $cityRef, NovaPoshtaClient $novaPoshtaClient)
    {
        return $this->json($novaPoshtaClient->getWarehouses($cityRef));
    }
}