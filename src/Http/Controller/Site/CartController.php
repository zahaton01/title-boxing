<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Site\Page;
use App\Domain\Repository\PageRepository;
use App\Http\Response\Normalizer\CartNormalizer;
use App\Infrastructure\Service\Cart\SessionCart;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/cart", name="site_cart_")
 */
class CartController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository)
    {
        $translator = new EntityTranslator($request->getLocale());
        $cartPage = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_CART]);

        return $this->renderTemplate('site/pages/cart/index.html.twig', [
            'meta' => $translator->trans($cartPage)
        ]);
    }

    /**
     * @Route("/get", name="get")
     */
    public function getCart(SessionCart $cart, CartNormalizer $cartNormalizer)
    {
        return $this->json($cartNormalizer->normalize($cart->get()));
    }

    /**
     * @Route("/{id}", name="remove", methods={"DELETE"})
     */
    public function removeItem(string $id, SessionCart $cart)
    {
        $cart->remove($id);

        return $this->json([], 204);
    }

    /**
     * @Route("/{id}/change-quantity", name="change_quantity", methods={"PATCH"})
     */
    public function changeQuantity(string $id, Request $request, SessionCart $sessionCart)
    {
        $data = json_decode($request->getContent(), true);
        $sessionCart->changeQuantity($id, $data['new_quantity']);

        return $this->json([], 204);
    }
}