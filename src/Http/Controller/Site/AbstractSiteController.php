<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Menu\MenuLink;
use App\Domain\Repository\MenuLinkRepository;
use App\Domain\Repository\StaticVarRepository;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
abstract class AbstractSiteController extends AbstractController
{
    private MenuLinkRepository $menuLinkRepository;
    private StaticVarRepository $staticVarRepository;

    public function __construct(MenuLinkRepository $menuLinkRepository, StaticVarRepository $staticVarRepository)
    {
        $this->menuLinkRepository = $menuLinkRepository;
        $this->staticVarRepository = $staticVarRepository;
    }

    public function renderTemplate(string $template, array $params = [])
    {
        $menuLinks = $this->menuLinkRepository->getHighLevelMenuLinks();

        $headerLinks = array_filter(
            $menuLinks,
            function ($link) {
                return $link->getStaticParent() === MenuLink::STATIC_PARENT_DESKTOP_HEADER;
            }
        );

        $categoriesLinks = array_filter(
            $menuLinks,
            function ($link) {
                return $link->getStaticParent() === MenuLink::STATIC_PARENT_DESKTOP_HEADER_CATEGORIES;
            }
        );

        $footerLinks = array_filter(
            $menuLinks,
            function ($link) {
                return $link->getStaticParent() === MenuLink::STATIC_PARENT_FOOTER;
            }
        );

        $mobileLinks = array_filter(
            $menuLinks,
            function ($link) {
                return $link->getStaticParent() === MenuLink::STATIC_PARENT_MOBILE_DROPDOWN;
            }
        );

        $params = array_merge($params, [
           'headerLinks' => $headerLinks,
           'footerLinks' => $footerLinks,
           'mobileLinks' => $mobileLinks,
           'categoriesLinks' => $categoriesLinks
        ]);

        return $this->render($template, $params);
    }
}