<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Site\Page;
use App\Domain\Repository\BrandRepository;
use App\Domain\Repository\MainBannerRepository;
use App\Domain\Repository\PageRepository;
use App\Domain\Repository\ProductRepository;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/", name="site_index")
 */
class HomeController extends AbstractSiteController
{
    public function __invoke(
        Request $request,
        ProductRepository $productRepository,
        BrandRepository $brandRepository,
        MainBannerRepository $mainBannerRepository,
        PageRepository $pageRepository
    ) {
        $saleProducts = $productRepository->findBy(['marking' => Product::MARKING_SALE, 'isVisible' => true], [], 8);
        $topProducts = $productRepository->findBy(['marking' => Product::MARKING_TOP, 'isVisible' => true], [], 8);
        $newProducts = $productRepository->findBy(['marking' => Product::MARKING_NEW, 'isVisible' => true], [], 8);
        $soonProducts = $productRepository->findBy(['marking' => Product::MARKING_SOON, 'isVisible' => true], [], 8);
        $brands = $brandRepository->findBy(['isVisible' => true], [], 16);
        $banners = $mainBannerRepository->findBy(['isVisible' => true]);

        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_HOME]);

        return $this->renderTemplate('site/pages/index.html.twig', [
            'saleProducts' => $saleProducts,
            'topProducts' => $topProducts,
            'newProducts' => $newProducts,
            'soonProducts' => $soonProducts,
            'brands' => $brands,
            'banners' => $banners,
            'meta' => $translator->trans($page)
        ]);
    }
}