<?php

namespace App\Http\Controller\Site;

use App\Domain\Repository\PageRepository;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/page", name="site_pages_")
 */
class PageController extends AbstractSiteController
{
    /**
     * @Route("/{slug}", name="view")
     */
    public function index(string $slug, PageRepository $pageRepository, Request $request)
    {
        $page = $pageRepository->findOneBy(['slug' => $slug]);

        if (null === $page || !$page->isVisible()) {
            throw new NotFoundHttpException();
        }

        $entityTranslator = new EntityTranslator($request->getLocale());
        return $this->renderTemplate('site/pages/custom_page/index.html.twig', ['page' => $entityTranslator->trans($page)]);
    }
}