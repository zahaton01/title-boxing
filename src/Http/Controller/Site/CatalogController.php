<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Site\Page;
use App\Domain\Repository\BrandRepository;
use App\Domain\Repository\MenuLinkRepository;
use App\Domain\Repository\PageRepository;
use App\Domain\Repository\ProductCategoryRepository;
use App\Domain\Repository\ProductRepository;
use App\Domain\Repository\SpecificationRepository;
use App\Domain\Repository\StaticVarRepository;
use App\Infrastructure\Service\EntityTranslator;
use App\Infrastructure\Service\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/catalog", name="site_catalog_")
 */
class CatalogController extends AbstractSiteController
{
    private ProductCategoryRepository $productCategoryRepository;
    private ProductRepository $productRepository;
    private Paginator $paginator;
    private SpecificationRepository $specificationRepository;
    private BrandRepository $brandRepository;

    public function __construct(
        MenuLinkRepository $menuLinkRepository,
        StaticVarRepository $staticVarRepository,
        ProductCategoryRepository $productCategoryRepository,
        ProductRepository $productRepository,
        Paginator $paginator,
        SpecificationRepository $specificationRepository,
        BrandRepository $brandRepository
    ) {
        parent::__construct($menuLinkRepository, $staticVarRepository);

        $this->productCategoryRepository = $productCategoryRepository;
        $this->productRepository = $productRepository;
        $this->paginator = $paginator;
        $this->specificationRepository = $specificationRepository;
        $this->brandRepository = $brandRepository;
    }

    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository): Response
    {
        $translator = new EntityTranslator($request->getLocale());
        $meta = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_CATALOG]);

        return $this->catalogResponse($translator->trans($meta), $request);
    }

    /**
     * @Route("/{category}", name="category")
     */
    public function category(Request $request, string $category): Response
    {
        $category = $this->productCategoryRepository->findOneBy(['slug' => $category]);
        $translator = new EntityTranslator($request->getLocale());

        return $this->catalogResponse($translator->trans($category), $request, $category);
    }

    /**
     * @Route("/{category}/{subCategory}", name="category_and_sub_category")
     */
    public function subCategory(Request $request, string $category, string $subCategory): Response
    {
        $category = $this->productCategoryRepository->findOneBy(['slug' => $category]);
        $subCategory = $this->productCategoryRepository->findOneBy(['slug' => $subCategory]);
        $translator = new EntityTranslator($request->getLocale());

        return $this->catalogResponse($translator->trans($subCategory), $request, $category, $subCategory);
    }

    private function catalogResponse(
        $meta,
        Request $request,
        ?ProductCategory $category = null,
        ?ProductCategory $subCategory = null
    ): Response {
        $categories = $this->productCategoryRepository->findBy(['isVisible' => true, 'category' => null]);
        $brands = $this->brandRepository->findBy(['isVisible' => true]);

        $specifications = [];
        if ($category || $subCategory) {
            $specifications = $this->specificationRepository->getListByCategories(
                [
                    $subCategory ? $subCategory->getId() : $category->getId()
                ]
            );
        }

        $filters = json_decode($request->query->get('filters', '{}'), true);
        $filterSpecifications = [];
        $filterSpecificationValues = [];
        if (isset($filters['specifications'])) {
            foreach ($filters['specifications'] as $specification) {
                $filterSpecifications[] = $specification['id'];
                $filterSpecificationValues = [...$filterSpecificationValues, ...$specification['values']];
            }
        }

        $products = $this->productRepository->getListBy(
            $subCategory ?: $category,
            $filters['min_price'] ?? null,
            $filters['max_price'] ?? null,
            $filterSpecifications,
            $filterSpecificationValues,
            $filters['brands'] ?? [],
            $filters['search'] ?? null,
            $filters['marking'] ?? null,
            true
        );

        return $this->renderTemplate(
            'site/pages/catalog/index.html.twig',
            [
                'products' => $this->paginator->paginate($products),
                'categories' => $categories,
                'currentCategory' => $category,
                'currentSubCategory' => $subCategory,
                'specifications' => $specifications,
                'filters' => $filters,
                'brands' => $brands,
                'meta' => $meta
            ]
        );
    }
}