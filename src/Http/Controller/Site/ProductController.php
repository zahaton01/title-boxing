<?php

namespace App\Http\Controller\Site;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductComment;
use App\Domain\Entity\User\FavouriteProduct;
use App\Domain\Repository\FavouriteProductRepository;
use App\Domain\Repository\ProductCommentRepository;
use App\Domain\Repository\ProductRepository;
use App\Http\Response\Normalizer\ProductCommentNormalizer;
use App\Http\Response\Normalizer\ProductSpecificationNormalizer;
use App\Infrastructure\Service\Cart\SessionCart;
use App\Infrastructure\Service\EntityTranslator;
use App\Infrastructure\Service\Paginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Contracts\Translation\TranslatorInterface;

use function Symfony\Component\String\b;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/product", name="site_product_")
 */
class ProductController extends AbstractSiteController
{
    /**
     * @Route("/{slug}", name="view")
     */
    public function index(
        string $slug,
        Request $request,
        ProductRepository $productRepository,
        SessionCart $sessionCart,
        FavouriteProductRepository $favouriteProductRepository
    ) {
        $translator = new EntityTranslator($request->getLocale());

        /** @var Product $product */
        $product = $productRepository->findOneBy(['slug' => $slug, 'isVisible' => true]);
        if (null === $product) {
            throw new NotFoundHttpException('Product does not exist');
        }

        $recommendedProductsQuery = $productRepository->getRecommendedProducts($product);

        $recommendedProducts = [];
        foreach ($product->getCategories() as $category) {
            foreach ($category->getProducts() as $categoryProduct) {
                if (!in_array($categoryProduct->getMarking(), [Product::MARKING_PRE_ORDER, Product::MARKING_SOON])) {
                    $recommendedProducts[$categoryProduct->getId()] = $categoryProduct;
                }
            }
        }

        foreach ($recommendedProductsQuery as $item) {
            $recommendedProducts[$item->getId()] = $item;
        }

        $inCart = false;
        foreach ($sessionCart->get()->getProducts() as $cartProduct) {
            if ($cartProduct->getProduct()->getId() === $product->getId()) {
                $inCart = true;
                break;
            }
        }

        $inFav = $favouriteProductRepository->findOneBy(['product' => $product, 'user' => $this->getUser()]) ? true : false;

        return $this->renderTemplate(
            'site/pages/product/index.html.twig',
            [
                'product' => $translator->trans($product),
                'recommendedProducts' => array_values($recommendedProducts),
                'inCart' => $inCart,
                'inFav' => $inFav
            ]
        );
    }

    /**
     * @Route("/{id}/comments", name="comments", methods={"GET"})
     */
    public function listComments(
        string $id,
        Paginator $paginator,
        ProductCommentRepository $productCommentRepository,
        ProductCommentNormalizer $productCommentNormalizer
    ): JsonResponse {
        $comments = $paginator->paginate($productCommentRepository->getList($id));
        return $this->json($productCommentNormalizer->normalizeMany(...$comments));
    }

    /**
     * @Route("/{id}/comments", name="comments_add", methods={"POST"})
     */
    public function addComment(
        Request $request,
        Product $product,
        ProductCommentNormalizer $productCommentNormalizer
    ): JsonResponse {
        $data = json_decode($request->getContent(), true);

        $comment = new ProductComment();
        $comment->setStars((int)$data['stars']);
        $comment->setProduct($product);
        $comment->setText($data['text']);
        $comment->setAuthorName($data['author_name']);
        $comment->setAuthor($this->getUser());

        $this->getDoctrine()->getManager()->persist($comment);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($productCommentNormalizer->normalize($comment));
    }

    /**
     * @Route("/{id}/specifications", name="specifications_list", methods={"GET"})
     */
    public function listSpecifications(
        Product $product,
        ProductSpecificationNormalizer $productSpecificationNormalizer
    ): JsonResponse {
        return $this->json($productSpecificationNormalizer->normalizeMany(...$product->getSpecifications()));
    }

    /**
     * @Route("/{id}/add-to-cart", name="add_to_cart", methods={"POST"})
     */
    public function addToCart(Request $request, SessionCart $sessionCart)
    {
        $data = json_decode($request->getContent(), true);
        $sessionCart->add($data);

        return $this->json([], 204);
    }

    /**
     * @Route("/{id}/add-to-favourites", name="add_to_favourites", methods={"POST"})
     */
    public function addToFavourites(
        Product $product,
        TranslatorInterface $translator,
        FavouriteProductRepository $favouriteProductRepository
    ) {
        if (!$this->getUser()) {
            return $this->json([$translator->trans('Please authorize!')], 403);
        }

        if ($favouriteProductRepository->findOneBy(['product' => $product, 'user' => $this->getUser()])) {
            return $this->json([], 204);
        }

        $favourite = new FavouriteProduct();
        $favourite->setProduct($product);
        $favourite->setUser($this->getUser());

        $this->getDoctrine()->getManager()->persist($favourite);
        $this->getDoctrine()->getManager()->flush();

        return $this->json([], 204);
    }
}