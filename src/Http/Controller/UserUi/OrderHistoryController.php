<?php

namespace App\Http\Controller\UserUi;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Entity\Site\Page;
use App\Domain\Repository\PageRepository;
use App\Http\Controller\Site\AbstractSiteController;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}/user/orders", name="user_orders_")
 */
class OrderHistoryController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository): Response
    {
        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_PROFILE_ORDERS]);

        return $this->renderTemplate('user_ui/pages/orders/index.html.twig', ['meta' => $translator->trans($page)]);
    }

    /**
     * @Route("/{id}/re-order", name="re_order")
     */
    public function reOrder(Order $order, TranslatorInterface $translator): Response
    {
        $cloned = clone $order;
        $cloned->setStatus(Order::STATUS_NEW);

        $this->getDoctrine()->getManager()->persist($cloned);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('user_orders_index', ['toast_success' => $translator->trans('Order created successfully')]);
    }
}