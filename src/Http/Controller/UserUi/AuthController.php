<?php

namespace App\Http\Controller\UserUi;

use App\Domain\Entity\Site\Page;
use App\Domain\Entity\User\ConfirmEmailAddressCode;
use App\Domain\Entity\User\ResetPasswordCode;
use App\Domain\Entity\User\User;
use App\Domain\Repository\ConfirmEmailAddressCodeRepository;
use App\Domain\Repository\PageRepository;
use App\Domain\Repository\ResetPasswordCodeRepository;
use App\Domain\Repository\UserRepository;
use App\Http\Controller\Site\AbstractSiteController;
use App\Http\Response\Forms\Types\User\RegistrationType;
use App\Http\Response\Forms\Types\User\ResetPasswordType;
use App\Http\Response\Forms\Types\User\SendResetPasswordCodeType;
use App\Infrastructure\Mailer\Email\ConfirmEmailEmail;
use App\Infrastructure\Mailer\Email\ResetPasswordEmail;
use App\Infrastructure\Mailer\SimpleMailer;
use App\Infrastructure\Security\Roles;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}", name="user_")
 */
class AuthController extends AbstractSiteController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils, PageRepository $pageRepository)
    {
        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_LOGIN]);

        return $this->renderTemplate(
            'user_ui/login.html.twig',
            [
                'error' => $authenticationUtils->getLastAuthenticationError(),
                'meta' => $translator->trans($page)
            ]
        );
    }

    /**
     * @Route("/registration", name="registration")
     */
    public function register(
        Request $request,
        EncoderFactoryInterface $encoder,
        PageRepository $pageRepository,
        SimpleMailer $mailer,
        TranslatorInterface $translator
    ) {
        $entityTranslator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_REGISTRATION]);

        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $user = new User();
            $user->setName($data['name']);
            $user->setSurname($data['surname']);
            $user->setEmail($data['email']);
            $user->setLogin($data['email']);
            $user->setRoles([Roles::USER]);
            $user->setPassword($encoder->getEncoder($user)->encodePassword($data['password'], null));

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $code = new ConfirmEmailAddressCode($user, bin2hex(random_bytes(16)));
            $this->getDoctrine()->getManager()->persist($code);
            $this->getDoctrine()->getManager()->flush();

            $email = new ConfirmEmailEmail($user, $code, $translator);
            $mailer->send($email);

            return $this->redirectToRoute('user_profile_index');
        }

        return $this->renderTemplate(
            'user_ui/registration.html.twig',
            [
                'form' => $form->createView(),
                'meta' => $entityTranslator->trans($page)
            ]
        );
    }

    /**
     * @Route("/registration/confirm-email/{code}", name="confirm_email", methods={"GET"})
     */
    public function confirmEmail(
        string $code,
        ConfirmEmailAddressCodeRepository $confirmEmailAddressCodeRepository,
        TranslatorInterface $translator
    ): Response {
        /** @var ConfirmEmailAddressCode $code */
        $code = $confirmEmailAddressCodeRepository->findOneBy(['code' => $code]);

        if (null === $code) {
            return $this->redirectToRoute('user_login', ['toast_error' => 'Code was not found']);
        }

        $code->getUser()->setIsEmailConfirmed(true);

        $this->getDoctrine()->getManager()->remove($code);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('user_login', ['toast_success' => $translator->trans('Email is confirmed')]);
    }

    /**
     * @Route("/reset-password", name="send_reset_password_code")
     */
    public function sendResetPasswordCode(
        Request $request,
        TranslatorInterface $translator,
        UserRepository $userRepository,
        SimpleMailer $mailer,
        PageRepository $pageRepository
    ): Response {
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_RESET_PASSWORD]);
        $entityTranslator = new EntityTranslator($request->getLocale());

        $form = $this->createForm(SendResetPasswordCodeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $user = $userRepository->findOneBy(['email' => $data['email']]);
            if (null === $user) {
                return $this->redirectToRoute(
                    'user_send_reset_password_code',
                    ['toast_error' => $translator->trans('User was not found')]
                );
            }

            $code = new ResetPasswordCode($user, bin2hex(random_bytes(16)));

            $this->getDoctrine()->getManager()->persist($code);
            $this->getDoctrine()->getManager()->flush();

            $email = new ResetPasswordEmail($user, $code, $translator);
            $mailer->send($email);

            return $this->redirectToRoute(
                'user_send_reset_password_code',
                ['toast_success' => $translator->trans('Code is sent to your email')]
            );
        }

        return $this->renderTemplate('user_ui/send_reset_password_code.html.twig', [
            'form' => $form->createView(),
            'meta' => $entityTranslator->trans($page)
        ]);
    }

    /**
     * @Route("/reset-password/{code}", name="reset_password")
     */
    public function resetPasswordByCode(
        Request $request,
        string $code,
        ResetPasswordCodeRepository $resetPasswordCodeRepository,
        EncoderFactoryInterface $encoderFactory,
        TranslatorInterface $translator,
        PageRepository $pageRepository
    ): Response {
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_RESET_PASSWORD]);
        $entityTranslator = new EntityTranslator($request->getLocale());

        /** @var ResetPasswordCode $code */
        $code = $resetPasswordCodeRepository->findOneBy(['code' => $code]);
        $user = $code->getUser();

        if (null === $code) {
            return $this->redirectToRoute('user_login', ['toast_error' => 'Code was not found']);
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $encoder = $encoderFactory->getEncoder($user);

            $user->setPassword($encoder->encodePassword($data['new_password'], null));

            $this->getDoctrine()->getManager()->remove($code);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_login', ['toast_success' => $translator->trans('Password is changed')]);
        }

        return $this->renderTemplate('user_ui/reset_password.html.twig', [
            'form' => $form->createView(),
            'meta' => $entityTranslator->trans($page)
        ]);
    }
}