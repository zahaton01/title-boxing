<?php

namespace App\Http\Controller\UserUi;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Site\Page;
use App\Domain\Repository\FavouriteProductRepository;
use App\Domain\Repository\PageRepository;
use App\Http\Controller\Site\AbstractSiteController;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}/user/liked-products", name="user_liked_products_")
 */
class LikedProductsController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository): Response
    {
        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_PROFILE_LIKED_PRODUCTS]);

        $products = [];
        foreach ($this->getUser()->getFavouriteProducts() as $favouriteProduct) {
            $products[] = $favouriteProduct->getProduct();
        }

        return $this->renderTemplate(
            'user_ui/pages/liked_products/index.html.twig',
            [
                'products' => $products,
                'meta' => $translator->trans($page)
            ]
        );
    }

    /**
     * @Route("/{id}/clear", name="clear_favourite")
     */
    public function clearFavourite(
        Product $product,
        FavouriteProductRepository $favouriteProductRepository,
        TranslatorInterface $translator
    ) {
        $product = $favouriteProductRepository->findOneBy(['user' => $this->getUser(), 'product' => $product]);
        $em = $this->getDoctrine()->getManager();

        if (null !== $product) {
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute(
            'user_liked_products_index',
            ['toast_success' => $translator->trans('Changes are saved!')]
        );
    }

    /**
     * @Route("/clear", name="clear")
     */
    public function clearFavourites(
        FavouriteProductRepository $favouriteProductRepository,
        TranslatorInterface $translator
    ) {
        $products = $favouriteProductRepository->findBy(['user' => $this->getUser()]);
        $em = $this->getDoctrine()->getManager();

        foreach ($products as $product) {
            $em->remove($product);
        }

        $em->flush();

        return $this->redirectToRoute(
            'user_liked_products_index',
            ['toast_success' => $translator->trans('Changes are saved!')]
        );
    }
}