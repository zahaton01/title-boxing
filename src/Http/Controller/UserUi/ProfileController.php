<?php

namespace App\Http\Controller\UserUi;

use App\Domain\Entity\Site\Page;
use App\Domain\Repository\PageRepository;
use App\Http\Controller\Site\AbstractSiteController;
use App\Http\Response\Forms\Types\User\PasswordChangeType;
use App\Http\Response\Forms\Types\User\ProfileType;
use App\Infrastructure\Service\EntityTranslator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/{_locale}/user/profile", name="user_profile_")
 */
class ProfileController extends AbstractSiteController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository): Response
    {
        $translator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_PROFILE_INFO]);

        return $this->renderTemplate('user_ui/pages/profile/index.html.twig', ['meta' => $translator->trans($page)]);
    }

    /**
     * @Route("/edit", name="edit_index")
     */
    public function editIndex(
        Request $request,
        TranslatorInterface $translator,
        PageRepository $pageRepository
    ): Response {
        $entityTranslator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_PROFILE_CHANGE_INFO]);

        $form = $this->createForm(ProfileType::class, $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(
                'user_profile_edit_index',
                ['toast_success' => $translator->trans('Changes are saved!')]
            );
        }

        return $this->renderTemplate(
            'user_ui/pages/profile/change_info.html.twig',
            [
                'form' => $form->createView(),
                'meta' => $entityTranslator->trans($page)
            ]
        );
    }

    /**
     * @Route("/change-password", name="change_password_index")
     */
    public function changePasswordIndex(
        Request $request,
        EncoderFactoryInterface $encoder,
        TranslatorInterface $translator,
        PageRepository $pageRepository
    ): Response {
        $entityTranslator = new EntityTranslator($request->getLocale());
        $page = $pageRepository->findOneBy(['staticType' => Page::STATIC_TYPE_PROFILE_CHANGE_PASSWORD]);

        $form = $this->createForm(PasswordChangeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $this->getUser();
            $encoder = $encoder->getEncoder($user);

            if (!$encoder->isPasswordValid($user->getPassword(), $data['old_password'], null)) {
                return $this->redirectToRoute(
                    'user_profile_change_password_index',
                    ['toast_error' => $translator->trans('Old password is invalid')]
                );
            }

            $user->setPassword($encoder->encodePassword($data['new_password'], null));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute(
                'user_profile_change_password_index',
                [
                    'toast_success' => $translator->trans('Changes are saved!')
                ]
            );
        }

        return $this->renderTemplate(
            'user_ui/pages/profile/change_password.html.twig',
            [
                'form' => $form->createView(),
                'meta' => $entityTranslator->trans($page)
            ]
        );
    }
}