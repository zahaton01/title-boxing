<?php

namespace App\Http\Controller\Admin\Commerce;

use App\Domain\Entity\Commerce\Brand;
use App\Domain\Entity\Commerce\BrandTranslation;
use App\Domain\Repository\BrandRepository;
use App\Http\Response\Forms\Types\Brand\BrandTranslationType;
use App\Http\Response\Forms\Types\Brand\BrandType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/brands", name="admin_brands_")
 */
class BrandController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(BrandRepository $brandRepository)
    {
        $brands = $brandRepository->findBy([], ['id' => 'DESC']);

        return $this->render('admin/pages/brand/index.html.twig', ['brands' => $brands]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(BrandType::class, new Brand());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_brands_index');
        }

        return $this->render('admin/pages/brand/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, Brand $brand)
    {
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_brands_index');
        }

        return $this->render(
            'admin/pages/brand/edit.html.twig',
            [
                'form' => $form->createView(),
                'brand' => $brand
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(Brand $brand)
    {
        $this->getDoctrine()->getManager()->remove($brand);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_brands_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, Brand $brand)
    {
        $form = $this->createForm(BrandTranslationType::class, new BrandTranslation(), ['brand' => $brand]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setBrand($brand);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_brands_edit', ['id' => $brand->getId()]);
        }

        return $this->render('admin/pages/brand/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, Brand $brand, BrandTranslation $translation)
    {
        $form = $this->createForm(BrandTranslationType::class, $translation, ['brand' => $brand]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_brands_edit', ['id' => $brand->getId()]);
        }

        return $this->render('admin/pages/brand/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(Brand $brand, BrandTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_brands_edit', ['id' => $brand->getId()]);
    }
}