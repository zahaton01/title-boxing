<?php

namespace App\Http\Controller\Admin\Commerce;

use App\Domain\Entity\Commerce\Order;
use App\Domain\Repository\OrderRepository;
use App\Http\Response\Forms\Types\Order\AdminOrderType;
use App\Infrastructure\ExternalService\NovaPoshtaClient;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Locales;
use App\Infrastructure\Service\Cart\SessionCart;
use App\Infrastructure\Service\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/orders", name="admin_orders_")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, OrderRepository $orderRepository, Paginator $paginator)
    {
        if ($request->query->has('start_date') && !empty($request->query->get('start_date'))) {
            $startDate = QueryUtil::nullOrDate($request->query->get('start_date'));
        }

        if ($request->query->has('end_date') && !empty($request->query->get('end_date'))) {
            $endDate = QueryUtil::nullOrDate($request->query->get('end_date'));
        }

        if ($request->query->has('search')) {
            $search = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        if ($request->query->has('status')) {
            $status = $request->query->get('status');
        }

        if ($request->query->has('order_number')) {
            $orderNumber = $request->query->get('order_number');
        }

        $orders = $orderRepository->getListBy(
            $search ?? null,
            $startDate ?? null,
            $endDate ?? null,
            $status ?? null,
            $orderNumber ?? null
        );

        return $this->render('admin/pages/orders/index.html.twig', ['orders' => $paginator->paginate($orders)]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request, NovaPoshtaClient $novaPoshtaClient)
    {
        $form = $this->createForm(AdminOrderType::class, new Order());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Order $order */
            $order = $form->getData();
            $order->setCart([]);

            if ($order->getDeliveryType() === Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE) {
                $novaCity = current($novaPoshtaClient->getCityByRef($order->getNovaPoshtaCity()));

                $novaCityName = $novaCity['Description'];
                if ($request->getLocale() === Locales::RU) {
                    $novaCity['DescriptionRu'];
                }

                $order->setNovaPoshtaCity($novaCityName);
            }

            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_orders_index');
        }

        return $this->render('admin/pages/orders/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(Request $request, Order $order, NovaPoshtaClient $novaPoshtaClient, SessionCart $sessionCart)
    {
        $form = $this->createForm(AdminOrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Order $order */
            $order = $form->getData();
            $order->setCart([]);

            if ($order->getDeliveryType() === Order::DELIVERY_TYPE_NOVA_POSHTA_WAREHOUSE) {
                $novaCity = current($novaPoshtaClient->getCityByRef($order->getNovaPoshtaCity()));

                $novaCityName = $novaCity['Description'];
                if ($request->getLocale() === Locales::RU) {
                    $novaCity['DescriptionRu'];
                }

                $order->setNovaPoshtaCity($novaCityName);
            }

            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_orders_index');
        }

        $sessionCart->setProductBag($order->getCart());

        return $this->render(
            'admin/pages/orders/edit.html.twig',
            [
                'form' => $form->createView(),
                'order' => $order,
                'cart' => $sessionCart->get()
            ]
        );
    }

    /**
     * @Route("/{id}/remove", name="remove")
     */
    public function remove(Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();

        return $this->redirectToRoute('admin_orders_index');
    }
}