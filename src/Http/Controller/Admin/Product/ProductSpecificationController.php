<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductSpecification;
use App\Domain\Entity\Product\ProductSpecificationValue;
use App\Http\Response\Forms\Types\Product\ProductSpecificationType;
use App\Http\Response\Forms\Types\Product\ProductSpecificationValueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/admin/products/{id}/specifications", name="admin_product_specifications_")
 */
class ProductSpecificationController extends AbstractController
{
    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request, Product $product)
    {
        $form = $this->createForm(ProductSpecificationType::class, new ProductSpecification());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setProduct($product);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render('admin/pages/product/specifications/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{productSpecificationId}/edit", name="edit")
     *
     * @ParamConverter("productSpecification", options={"id" = "productSpecificationId"})
     */
    public function edit(Request $request, Product $product, ProductSpecification $productSpecification)
    {
        $form = $this->createForm(ProductSpecificationType::class, $productSpecification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render(
            'admin/pages/product/specifications/edit.html.twig',
            [
                'form' => $form->createView(),
                'product' => $product,
                'productSpecification' => $productSpecification
            ]
        );
    }

    /**
     * @Route("/{productSpecificationId}/remove", name="remove")
     *
     * @ParamConverter("productSpecification", options={"id" = "productSpecificationId"})
     */
    public function remove(Product $product, ProductSpecification $productSpecification)
    {
        $this->getDoctrine()->getManager()->remove($productSpecification);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
    }

    /**
     * @Route("/{productSpecificationId}/create-value", name="create_value")
     *
     * @ParamConverter("productSpecification", options={"id" = "productSpecificationId"})
     */
    public function createValue(
        Request $request,
        Product $product,
        ProductSpecification $productSpecification
    ) {
        $form = $this->createForm(
            ProductSpecificationValueType::class,
            new ProductSpecificationValue(),
            ['related_specification' => $productSpecification->getRelatedSpecification()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productSpecification->addValue($form->getData());

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_specifications_edit', [
                'id' => $product->getId(),
                'productSpecificationId' => $productSpecification->getId()
            ]);
        }

        return $this->render('admin/pages/product/specifications/create_value.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{productSpecificationId}/edit-value/{productSpecificationValueId}", name="edit_value")
     *
     * @ParamConverter("productSpecification", options={"id" = "productSpecificationId"})
     * @ParamConverter("productSpecificationValue", options={"id" = "productSpecificationValueId"})
     */
    public function editValue(
        Request $request,
        Product $product,
        ProductSpecification $productSpecification,
        ProductSpecificationValue $productSpecificationValue
    ) {
        $form = $this->createForm(
            ProductSpecificationValueType::class,
            $productSpecificationValue,
            ['related_specification' => $productSpecification->getRelatedSpecification()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_specifications_edit', [
                'id' => $product->getId(),
                'productSpecificationId' => $productSpecification->getId()
            ]);
        }

        return $this->render('admin/pages/product/specifications/edit_value.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{productSpecificationId}/remove-value/{productSpecificationValueId}", name="remove_value")
     *
     * @ParamConverter("productSpecification", options={"id" = "productSpecificationId"})
     * @ParamConverter("productSpecificationValue", options={"id" = "productSpecificationValueId"})
     */
    public function removeValue(
        Product $product,
        ProductSpecification $productSpecification,
        ProductSpecificationValue $productSpecificationValue
    ) {
        $this->getDoctrine()->getManager()->remove($productSpecificationValue);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_product_specifications_edit', [
            'id' => $product->getId(),
            'productSpecificationId' => $productSpecification->getId()
        ]);
    }
}