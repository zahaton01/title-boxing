<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductTranslation;
use App\Domain\Repository\BrandRepository;
use App\Domain\Repository\ProductCategoryRepository;
use App\Domain\Repository\ProductRepository;
use App\Http\Response\Forms\Types\Product\ProductTranslationType;
use App\Http\Response\Forms\Types\Product\ProductType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/product", name="admin_products_")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(
        Request $request,
        ProductRepository $productRepository,
        Paginator $paginator,
        ProductCategoryRepository $productCategoryRepository,
        BrandRepository $brandRepository
    ) {
        if ($request->query->has('search')) {
            $searchString = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        if ($request->query->has('category') && !empty($request->query->get('category'))) {
            $category = $productCategoryRepository->find($request->query->get('category'));
        }

        if ($request->query->has('min_price') && !empty($request->query->get('min_price'))) {
            $minPrice = (int) $request->query->get('min_price');
        }

        if ($request->query->has('max_price') && !empty($request->query->get('max_price'))) {
            $maxPrice = (int) $request->query->get('max_price');
        }

        if ($request->query->has('brands')) {
            $brands = $request->query->get('brands', []);
        }

        $products = $productRepository->getListBy(
            $category ?? null,
            $minPrice ?? null,
            $maxPrice ?? null,
            [],
            [],
            $brands ?? [],
            $searchString ?? null
        );

        return $this->render('admin/pages/product/index.html.twig', [
            'products' => $paginator->paginate($products),
            'categories' => $productCategoryRepository->findAll(),
            'brands' => $brandRepository->findAll()
        ]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(ProductType::class, new Product());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_index');
        }

        return $this->render('admin/pages/product/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, Product $product)
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_index');
        }

        return $this->render(
            'admin/pages/product/edit.html.twig',
            [
                'form' => $form->createView(),
                'product' => $product
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(Product $product)
    {
        $this->getDoctrine()->getManager()->remove($product);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_products_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, Product $product)
    {
        $form = $this->createForm(ProductTranslationType::class, new ProductTranslation(), ['product' => $product]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setProduct($product);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render('admin/pages/product/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, Product $product, ProductTranslation $translation)
    {
        $form = $this->createForm(ProductTranslationType::class, $translation, ['product' => $product]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render('admin/pages/product/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(Product $product, ProductTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
    }
}