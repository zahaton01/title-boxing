<?php

namespace App\Http\Controller\Admin\Product;

use App\Infrastructure\DataFlow\Import\ProductXlsxImporter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/admin/products/import", name="admin_products_import_")
 */
class ProductImportController extends AbstractController
{
    /**
     * @Route("/xlsx", name="xlsx")
     */
    public function fromXlsx(Request $request, ProductXlsxImporter $importer)
    {
        $importer->setFile($request->files->get('file'));
        $importer->import();

        return $this->redirectToRoute('admin_products_index');
    }
}