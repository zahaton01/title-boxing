<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Product\ProductCategoryTranslation;
use App\Domain\Repository\ProductCategoryRepository;
use App\Http\Response\Forms\Types\Product\ProductCategoryTranslationType;
use App\Http\Response\Forms\Types\Product\ProductCategoryType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/product-category", name="admin_product_categories_")
 */
class ProductCategoryController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, ProductCategoryRepository $productCategoryRepository, Paginator $paginator)
    {
        if ($request->query->has('search')) {
            $searchString = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        $categories = $productCategoryRepository->getList($searchString ?? null);
        return $this->render(
            'admin/pages/product_category/index.html.twig',
            ['categories' => $paginator->paginate($categories)]
        );
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(ProductCategoryType::class, new ProductCategory());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_categories_index');
        }

        return $this->render('admin/pages/product_category/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, ProductCategory $productCategory)
    {
        $form = $this->createForm(ProductCategoryType::class, $productCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_categories_index');
        }

        return $this->render(
            'admin/pages/product_category/edit.html.twig',
            [
                'form' => $form->createView(),
                'category' => $productCategory
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(ProductCategory $productCategory)
    {
        $this->getDoctrine()->getManager()->remove($productCategory);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_product_categories_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, ProductCategory $productCategory)
    {
        $form = $this->createForm(
            ProductCategoryTranslationType::class,
            new ProductCategoryTranslation(),
            ['category' => $productCategory]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setCategory($productCategory);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_categories_edit', ['id' => $productCategory->getId()]);
        }

        return $this->render(
            'admin/pages/product_category/create_translation.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(
        Request $request,
        ProductCategory $productCategory,
        ProductCategoryTranslation $translation
    ) {
        $form = $this->createForm(
            ProductCategoryTranslationType::class,
            $translation,
            ['category' => $productCategory]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_categories_edit', ['id' => $productCategory->getId()]);
        }

        return $this->render(
            'admin/pages/product_category/edit_translation.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(ProductCategory $productCategory, ProductCategoryTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_product_categories_edit', ['id' => $productCategory->getId()]);
    }
}