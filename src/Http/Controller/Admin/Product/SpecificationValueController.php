<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationTranslation;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Domain\Entity\Product\Specification\SpecificationValueTranslation;
use App\Http\Response\Forms\Types\Specification\SpecificationTranslationType;
use App\Http\Response\Forms\Types\Specification\SpecificationType;
use App\Http\Response\Forms\Types\Specification\SpecificationValueTranslationType;
use App\Http\Response\Forms\Types\Specification\SpecificationValueType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/specification/{id}/values", name="admin_specification_values_")
 */
class SpecificationValueController extends AbstractController
{
    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request, Specification $specification)
    {
        $form = $this->createForm(SpecificationValueType::class, new SpecificationValue());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setSpecification($specification);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_edit', ['id' => $specification->getId()]);
        }

        return $this->render('admin/pages/specification/values/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{valueId}/edit", name="edit")
     *
     * @ParamConverter("specificationValue", options={"id" = "valueId"})
     */
    public function edit(Request $request, Specification $specification, SpecificationValue $specificationValue)
    {
        $form = $this->createForm(SpecificationValueType::class, $specificationValue);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_edit', ['id' => $specification->getId()]);
        }

        return $this->render(
            'admin/pages/specification/values/edit.html.twig',
            [
                'form' => $form->createView(),
                'specification' => $specification,
                'specificationValue' => $specificationValue
            ]
        );
    }

    /**
     * @Route("/{valueId}/remove", name="remove")
     *
     * @ParamConverter("specificationValue", options={"id" = "valueId"})
     */
    public function remove(Specification $specification, SpecificationValue $specificationValue)
    {
        $this->getDoctrine()->getManager()->remove($specificationValue);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_specifications_edit', ['id' => $specification->getId()]);
    }

    /**
     * @Route("/{valueId}/create-translation", name="create_translation")
     *
     * @ParamConverter("specificationValue", options={"id" = "valueId"})
     */
    public function createTranslation(
        Request $request,
        Specification $specification,
        SpecificationValue $specificationValue
    ) {
        $form = $this->createForm(
            SpecificationValueTranslationType::class,
            new SpecificationValueTranslation(),
            ['specification_value' => $specificationValue]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setValue($specificationValue);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specification_values_edit', [
                'id' => $specification->getId(),
                'valueId' => $specificationValue->getId()
            ]);
        }

        return $this->render('admin/pages/specification/values/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{valueId}/edit-translation/{transId}", name="edit_translation")
     *
     * @ParamConverter("specificationValue", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(
        Request $request,
        Specification $specification,
        SpecificationValue $specificationValue,
        SpecificationValueTranslation $translation
    ) {
        $form = $this->createForm(
            SpecificationValueTranslationType::class,
            $translation,
            ['specification_value' => $specificationValue]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specification_values_edit', [
                'id' => $specification->getId(),
                'valueId' => $specificationValue->getId()
            ]);
        }

        return $this->render('admin/pages/specification/values/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{valueId}/remove-translation/{transId}", name="remove_translation")
     *
     * @ParamConverter("specificationValue", options={"id" = "valueId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(
        Specification $specification,
        SpecificationValue $specificationValue,
        SpecificationValueTranslation $translation
    ) {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_specification_values_edit', [
            'id' => $specification->getId(),
            'valueId' => $specificationValue->getId()
        ]);
    }
}