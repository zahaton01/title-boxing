<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationTranslation;
use App\Domain\Repository\SpecificationRepository;
use App\Http\Response\Forms\Types\Specification\SpecificationTranslationType;
use App\Http\Response\Forms\Types\Specification\SpecificationType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/specifications", name="admin_specifications_")
 */
class SpecificationController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, SpecificationRepository $specificationRepository, Paginator $paginator)
    {
        if ($request->query->has('search')) {
            $searchString = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        $specifications = $specificationRepository->getList($searchString ?? null);

        return $this->render('admin/pages/specification/index.html.twig', ['specifications' => $paginator->paginate($specifications)]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(SpecificationType::class, new Specification());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_index');
        }

        return $this->render('admin/pages/specification/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, Specification $specification)
    {
        $form = $this->createForm(SpecificationType::class, $specification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_index');
        }

        return $this->render(
            'admin/pages/specification/edit.html.twig',
            [
                'form' => $form->createView(),
                'specification' => $specification
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(Specification $specification)
    {
        $this->getDoctrine()->getManager()->remove($specification);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_specifications_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, Specification $specification)
    {
        $form = $this->createForm(SpecificationTranslationType::class, new SpecificationTranslation(), ['specification' => $specification]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setSpecification($specification);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_edit', ['id' => $specification->getId()]);
        }

        return $this->render('admin/pages/specification/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, Specification $specification, SpecificationTranslation $translation)
    {
        $form = $this->createForm(SpecificationTranslationType::class, $translation, ['specification' => $specification]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_specifications_edit', ['id' => $specification->getId()]);
        }

        return $this->render('admin/pages/specification/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(Specification $page, SpecificationTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_specifications_edit', ['id' => $page->getId()]);
    }
}