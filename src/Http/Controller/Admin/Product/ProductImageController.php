<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductImage;
use App\Domain\Entity\Product\ProductImageTranslation;
use App\Http\Response\Forms\Types\Product\ProductImageTranslationType;
use App\Http\Response\Forms\Types\Product\ProductImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/admin/products/{id}/images", name="admin_product_images_")
 */
class ProductImageController extends AbstractController
{
    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request, Product $product)
    {
        $form = $this->createForm(ProductImageType::class, new ProductImage(), ['product' => $product]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setProduct($product);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render('admin/pages/product/images/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{imageId}/edit", name="edit")
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     */
    public function edit(Request $request, Product $product, ProductImage $image)
    {
        $form = $this->createForm(ProductImageType::class, $image, ['product' => $product]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
        }

        return $this->render(
            'admin/pages/product/images/edit.html.twig',
            [
                'form' => $form->createView(),
                'product' => $product,
                'image' => $image
            ]
        );
    }

    /**
     * @Route("/{imageId}/remove", name="remove")
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     */
    public function remove(Product $product, ProductImage $image)
    {
        $this->getDoctrine()->getManager()->remove($image);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_products_edit', ['id' => $product->getId()]);
    }

    /**
     * @Route("/{imageId}/create-translation", name="create_translation")
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     */
    public function createTranslation(
        Request $request,
        Product $product,
        ProductImage $image
    ) {
        $form = $this->createForm(
            ProductImageTranslationType::class,
            new ProductImageTranslation(),
            ['image' => $image]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setImage($image);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_images_edit', [
                'id' => $product->getId(),
                'imageId' => $image->getId()
            ]);
        }

        return $this->render('admin/pages/product/images/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{imageId}/edit-translation/{transId}", name="edit_translation")
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(
        Request $request,
        Product $product,
        ProductImage $image,
        ProductImageTranslation $translation
    ) {
        $form = $this->createForm(
            ProductImageTranslationType::class,
            $translation,
            ['image' => $image]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_product_images_edit', [
                'id' => $product->getId(),
                'imageId' => $image->getId()
            ]);
        }

        return $this->render('admin/pages/product/images/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/{imageId}/remove-translation/{transId}", name="remove_translation")
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(
        Product $product,
        ProductImage $image,
        ProductImageTranslation $translation
    ) {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_product_images_edit', [
            'id' => $product->getId(),
            'imageId' => $image->getId()
        ]);
    }
}