<?php

namespace App\Http\Controller\Admin\Product;

use App\Domain\Repository\ProductRepository;
use App\Infrastructure\DataFlow\Export\ProductXlsxExporter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/admin/products/export", name="admin_products_export_")
 */
class ProductExportController extends AbstractController
{
    /**
     * @Route("/xlsx", name="xlsx")
     */
    public function asXlsx(ProductRepository $productRepository, ProductXlsxExporter $exporter): Response
    {
        $products = $productRepository->findAll();
        $exporter->setProducts(...$products);

        return $exporter->export();
    }
}