<?php

namespace App\Http\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class AuthController extends AbstractController
{
    /**
     * @Route("/{_locale}/admin", name="admin_login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        return $this->render(
            'admin/pages/login.html.twig',
            ['error' => $authenticationUtils->getLastAuthenticationError()]
        );
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logout()
    {
        return true;
    }
}