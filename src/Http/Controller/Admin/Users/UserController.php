<?php

namespace App\Http\Controller\Admin\Users;

use App\Domain\Entity\User\User;
use App\Domain\Repository\UserRepository;
use App\Http\Response\Forms\Types\User\AdminUserType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/users", name="admin_users_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, UserRepository $userRepository, Paginator $paginator)
    {
        if ($request->query->has('search')) {
            $search = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        $users = $userRepository->getListBy($search ?? null);

        return $this->render('admin/pages/users/index.html.twig', ['users' => $paginator->paginate($users)]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(Request $request, User $user)
    {
        $form = $this->createForm(AdminUserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_orders_index');
        }

        return $this->render(
            'admin/pages/users/edit.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user
            ]
        );
    }

    /**
     * @Route("/{id}/remove", name="remove")
     */
    public function remove(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin_users_index');
    }
}