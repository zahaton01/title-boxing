<?php

namespace App\Http\Controller\Admin\Site;

use App\Domain\Entity\Site\Page;
use App\Domain\Entity\Site\PageTranslation;
use App\Domain\Repository\PageRepository;
use App\Http\Response\Forms\Types\Page\PageTranslationType;
use App\Http\Response\Forms\Types\Page\PageType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/pages", name="admin_pages_")
 */
class PageController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, PageRepository $pageRepository, Paginator $paginator)
    {
        if ($request->query->has('search')) {
            $searchString = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        $pages = $pageRepository->getList($searchString ?? null);

        return $this->render('admin/pages/pages/index.html.twig', ['pages' => $paginator->paginate($pages)]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(PageType::class, new Page());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_pages_index');
        }

        return $this->render('admin/pages/pages/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, Page $page)
    {
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_pages_index');
        }

        return $this->render(
            'admin/pages/pages/edit.html.twig',
            [
                'form' => $form->createView(),
                'page' => $page
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(Page $page)
    {
        $this->getDoctrine()->getManager()->remove($page);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_pages_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, Page $page)
    {
        $form = $this->createForm(PageTranslationType::class, new PageTranslation(), ['page' => $page]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setPage($page);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_pages_edit', ['id' => $page->getId()]);
        }

        return $this->render('admin/pages/pages/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, Page $page, PageTranslation $translation)
    {
        $form = $this->createForm(PageTranslationType::class, $translation, ['page' => $page]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_pages_edit', ['id' => $page->getId()]);
        }

        return $this->render('admin/pages/pages/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(Page $page, PageTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_pages_edit', ['id' => $page->getId()]);
    }
}