<?php

namespace App\Http\Controller\Admin\Site;

use App\Domain\Entity\Site\StaticVar;
use App\Domain\Repository\StaticVarRepository;
use App\Http\Response\Forms\Types\StaticVar\StaticVarType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/static-var", name="admin_static_var_")
 */
class StaticVarController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(StaticVarRepository $staticVarRepository)
    {
        return $this->render('admin/pages/static_var/index.html.twig', ['vars' => $staticVarRepository->findAll()]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(Request $request, StaticVar $staticVar)
    {
        $form = $this->createForm(StaticVarType::class, $staticVar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_static_var_index');
        }

        return $this->render('admin/pages/static_var/edit.html.twig', ['form' => $form->createView()]);
    }
}