<?php

namespace App\Http\Controller\Admin\Site;

use App\Domain\Entity\Site\MainBanner;
use App\Domain\Entity\Site\MainBannerTranslation;
use App\Domain\Repository\MainBannerRepository;
use App\Http\Response\Forms\Types\MainBanner\MainBannerTranslationType;
use App\Http\Response\Forms\Types\MainBanner\MainBannerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/main-banner", name="admin_main_banner_")
 */
class MainBannerController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(MainBannerRepository $mainBannerRepository)
    {
        return $this->render(
            'admin/pages/main_banner/index.html.twig',
            ['banners' => $mainBannerRepository->findAll()]
        );
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(MainBannerType::class, new MainBanner());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_main_banner_index');
        }

        return $this->render('admin/pages/main_banner/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, MainBanner $banner)
    {
        $form = $this->createForm(MainBannerType::class, $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_main_banner_index');
        }

        return $this->render(
            'admin/pages/main_banner/edit.html.twig',
            [
                'form' => $form->createView(),
                'banner' => $banner
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(MainBanner $banner)
    {
        $this->getDoctrine()->getManager()->remove($banner);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_main_banner_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, MainBanner $banner)
    {
        $form = $this->createForm(MainBannerTranslationType::class, new MainBannerTranslation(), ['banner' => $banner]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setBanner($banner);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_main_banner_edit', ['id' => $banner->getId()]);
        }

        return $this->render('admin/pages/main_banner/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, MainBanner $banner, MainBannerTranslation $translation)
    {
        $form = $this->createForm(MainBannerTranslationType::class, $translation, ['banner' => $banner]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_main_banner_edit', ['id' => $banner->getId()]);
        }

        return $this->render('admin/pages/main_banner/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(MainBanner $banner, MainBannerTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_main_banner_edit', ['id' => $banner->getId()]);
    }
}