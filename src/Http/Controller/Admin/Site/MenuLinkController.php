<?php

namespace App\Http\Controller\Admin\Site;

use App\Domain\Entity\Menu\MenuLink;
use App\Domain\Entity\Menu\MenuLinkImage;
use App\Domain\Entity\Menu\MenuLinkImageTranslation;
use App\Domain\Entity\Menu\MenuLinkTranslation;
use App\Domain\Repository\MenuLinkRepository;
use App\Http\Response\Forms\Types\MenuLink\MenuLinkImageTranslationType;
use App\Http\Response\Forms\Types\MenuLink\MenuLinkImageType;
use App\Http\Response\Forms\Types\MenuLink\MenuLinkTranslationType;
use App\Http\Response\Forms\Types\MenuLink\MenuLinkType;
use App\Infrastructure\Helpers\QueryUtil;
use App\Infrastructure\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/menu-link", name="admin_menu_links_")
 */
class MenuLinkController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, MenuLinkRepository $menuLinkRepository, Paginator $paginator)
    {
        if ($request->query->has('search')) {
            $searchString = QueryUtil::prepareSearchString($request->query->get('search'));
        }

        if ($request->query->has('static_parent') && !empty($request->query->get('static_parent'))) {
            $staticParent = $request->query->get('static_parent');
        }

        $links = $menuLinkRepository->getList($searchString ?? null, $staticParent ?? null);

        return $this->render('admin/pages/menu_link/index.html.twig', ['links' => $paginator->paginate($links)]);
    }

    /**
     * @Route("/create", name="create")
     */
    public function create(Request $request)
    {
        $form = $this->createForm(MenuLinkType::class, new MenuLink());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_index');
        }

        return $this->render('admin/pages/menu_link/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request, MenuLink $menuLink)
    {
        $form = $this->createForm(MenuLinkType::class, $menuLink);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_index');
        }

        return $this->render(
            'admin/pages/menu_link/edit.html.twig',
            [
                'form' => $form->createView(),
                'link' => $menuLink
            ]
        );
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(MenuLink $menuLink)
    {
        $this->getDoctrine()->getManager()->remove($menuLink);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_menu_links_index');
    }

    /**
     * @Route("/edit/{id}/create-translation", name="create_translation")
     */
    public function createTranslation(Request $request, MenuLink $menuLink)
    {
        $form = $this->createForm(
            MenuLinkTranslationType::class,
            new MenuLinkTranslation(),
            ['menu_link' => $menuLink]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setLink($menuLink);

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render('admin/pages/menu_link/create_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/edit/{transId}", name="edit_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editTranslation(Request $request, MenuLink $menuLink, MenuLinkTranslation $translation)
    {
        $form = $this->createForm(MenuLinkTranslationType::class, $translation, ['menu_link' => $menuLink]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render('admin/pages/menu_link/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/translations/remove/{transId}", name="remove_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeTranslation(MenuLink $menuLink, MenuLinkTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
    }

    /**
     * @Route("/{id}/create-image", name="create_image")
     */
    public function createImage(Request $request, MenuLink $menuLink)
    {
        $form = $this->createForm(MenuLinkImageType::class, new MenuLinkImage());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var MenuLinkImage $image */
            $image = $form->getData();
            $menuLink->setImage($image);

            $this->getDoctrine()->getManager()->persist($menuLink);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render('admin/pages/menu_link/create_image.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/edit-image", name="edit_image")
     */
    public function editImage(Request $request, MenuLink $menuLink)
    {
        $form = $this->createForm(MenuLinkImageType::class, $menuLink->getImage());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render('admin/pages/menu_link/edit_image.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/remove-image", name="remove_image")
     */
    public function removeImage(MenuLink $menuLink)
    {
        $this->getDoctrine()->getManager()->remove($menuLink->getImage());
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
    }

    /**
     * @Route("/edit/{id}/create-image-translation", name="create_image_translation")
     */
    public function createImageTranslation(Request $request, MenuLink $menuLink)
    {
        $form = $this->createForm(
            MenuLinkImageTranslationType::class,
            new MenuLinkImageTranslation(),
            ['image' => $menuLink->getImage()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $form->getData()->setImage($menuLink->getImage());

            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render(
            'admin/pages/menu_link/create_image_translation.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/edit/{id}/edit-image-translation/{transId}", name="edit_image_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function editImageTranslation(Request $request, MenuLink $menuLink, MenuLinkImageTranslation $translation)
    {
        $form = $this->createForm(
            MenuLinkImageTranslationType::class,
            $translation,
            ['image' => $menuLink->getImage()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
        }

        return $this->render('admin/pages/menu_link/edit_translation.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/edit/{id}/remove-image-translation/{transId}", name="remove_image_translation")
     *
     * @ParamConverter("translation", options={"id" = "transId"})
     */
    public function removeImageTranslation(MenuLink $menuLink, MenuLinkImageTranslation $translation)
    {
        $this->getDoctrine()->getManager()->remove($translation);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_menu_links_edit', ['id' => $menuLink->getId()]);
    }
}