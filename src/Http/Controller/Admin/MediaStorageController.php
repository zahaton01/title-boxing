<?php

namespace App\Http\Controller\Admin;

use App\Domain\Entity\Media\MediaFile;
use App\Domain\Repository\MediaFileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin/media-storage", name="admin_media_storage_")
 */
class MediaStorageController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(): Response
    {
        return $this->render('admin/pages/media_storage/index.html.twig');
    }

    /**
     * @Route("/sync-db-and-file-system", name="sync_db_and_file_system")
     */
    public function syncDbAndFileSystem(MediaFileRepository $mediaFileRepository)
    {
        $projectDir = $this->getParameter('kernel.project_dir');
        $mediaPath = "$projectDir/public/media";
        if (!is_dir($mediaPath)) {
            mkdir($mediaPath);
        }

        $this->scanDir($mediaPath, $mediaFileRepository, $projectDir);

        return $this->json([], 204);
    }

    private function scanDir(string $path, MediaFileRepository $mediaFileRepository, string $projectDir)
    {
        $filesAndDirs = scandir($path);
        foreach ($filesAndDirs as $item) {
            if ($item === '.' || $item === '..') {
                continue;
            }

            $pieces = explode('.', $item);
            if (count($pieces) > 1) {
                if (in_array($pieces[1], ['png', 'jpg', 'jpeg', 'gif', 'svg'])) {
                    $filePath = "$path/$item";
                    $filePath = explode($projectDir . '/public', $filePath)[1];
                    $image = $mediaFileRepository->findOneBy(['relativePath' => $filePath]);

                    if (null == $image) {
                        $em = $this->getDoctrine()->getManager();
                        $image = MediaFile::image();
                        $image->setRelativePath($filePath);

                        $em->persist($image);
                        $em->flush();
                    }
                }
            } else {
                $this->scanDir($path . "/$item", $mediaFileRepository, $projectDir);
            }
        }
    }

    /**
     * @Route("/list", name="list")
     */
    public function listFiles(Request $request): JsonResponse
    {
        $currentPath = $request->query->get('current_path', '');
        $projectDir = $this->getParameter('kernel.project_dir');
        if (!is_dir("$projectDir/public/media")) {
            mkdir("$projectDir/public/media");
        }

        $filesAndDirs = scandir($projectDir . "/public/media/{$currentPath}");

        $response = [];
        foreach ($filesAndDirs as $item) {
            if ($item === '.' || $item === '..') {
                continue;
            }

            $pieces = explode('.', $item);
            if (count($pieces) > 1) {
                if (in_array($pieces[1], ['png', 'jpg', 'jpeg', 'gif', 'svg'])) {
                    $response[] = [
                        'path_name' => $item,
                        'is_dir' => false,
                        'is_image' => true,
                        'src' => "/media{$currentPath}/{$item}",
                        'creation_time' => filectime(
                            $this->getParameter('kernel.project_dir') . "/public/media/{$currentPath}/{$item}"
                        )
                    ];
                }
            } else {
                $response[] = [
                    'path_name' => $item,
                    'is_dir' => true,
                    'is_image' => false,
                    'creation_time' => filectime(
                        $this->getParameter('kernel.project_dir') . "/public/media/{$currentPath}/{$item}"
                    )
                ];
            }
        }

        return $this->json($response);
    }

    /**
     * @Route("/create-directory", name="create_directory")
     */
    public function createDirectory(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['name'])) {
            return $this->json(['msg' => 'Name cannot be blank'], 400);
        }

        $directoryPath = $this->getParameter(
                'kernel.project_dir'
            ) . "/public/media/{$data['current_path']}/{$data['name']}";

        if (is_dir($directoryPath)) {
            return $this->json(['msg' => 'Directory already exists'], 400);
        }

        mkdir($directoryPath);

        return $this->json(
            [
                'path_name' => $data['name'],
                'is_dir' => true
            ]
        );
    }

    /**
     * @Route("/remove-file", name="remove_file", methods={"PATCH"})
     */
    public function removeFile(Request $request, MediaFileRepository $mediaFileRepository): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['name'])) {
            return $this->json(['msg' => 'Name cannot be blank'], 400);
        }

        $projectDir = $this->getParameter('kernel.project_dir');
        $directoryPath = $projectDir . "/public/media/{$data['current_path']}/{$data['name']}";

        if (!file_exists($directoryPath)) {
            return $this->json(['msg' => 'File was not found'], 400);
        }

        unlink($directoryPath);

        $em = $this->getDoctrine()->getManager();
        $mediaFile = $mediaFileRepository->findOneBy(
            ['relativePath' => "/media{$data['current_path']}/{$data['name']}"]
        );
        $em->remove($mediaFile);
        $em->flush();

        return $this->json([], 204);
    }

    /**
     * @Route("/remove-directory", name="remove_directory", methods={"PATCH"})
     */
    public function removeDirectory(Request $request, MediaFileRepository $mediaFileRepository): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['name'])) {
            return $this->json(['msg' => 'Name cannot be blank'], 400);
        }

        $projectDir = $this->getParameter('kernel.project_dir');
        $directoryPath = $projectDir . "/public/media/{$data['current_path']}/{$data['name']}";

        if (!is_dir($directoryPath)) {
            return $this->json(['msg' => 'Directory does not exist'], 400);
        }

        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($directoryPath, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileInfo) {
            if (!$fileInfo->isDir()) {
                $pieces = explode("$projectDir/public", $fileInfo->getRealPath());

                $em = $this->getDoctrine()->getManager();
                $mediaFile = $mediaFileRepository->findOneBy(['relativePath' => $pieces[1]]);
                $em->remove($mediaFile);
                $em->flush();
            }

            $todo = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
            $todo($fileInfo->getRealPath());
        }

        rmdir($directoryPath);

        return $this->json([], 204);
    }

    /**
     * @Route("/upload-file", name="upload_file", methods={"POST"})
     */
    public function uploadFile(Request $request): JsonResponse
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        $currentPath = $request->request->get('current_path');

        $directory = $this->getParameter('kernel.project_dir') . "/public/media$currentPath/";
        $newPath = $directory . $file->getClientOriginalName();

        if (file_exists($newPath)) {
            return $this->json(['msg' => 'File exists'], 400);
        }

        $file->move(
            $directory,
            $file->getClientOriginalName()
        );

        $em = $this->getDoctrine()->getManager();
        if (in_array($file->guessClientExtension(), ['png', 'jpg', 'jpeg', 'gif', 'svg'])) {
            $image = MediaFile::image();
            $image->setRelativePath("/media$currentPath/{$file->getClientOriginalName()}");

            $em->persist($image);
            $em->flush();
        }

        return $this->json(
            [
                'path_name' => $file->getClientOriginalName(),
                'is_dir' => false,
                'is_image' => true,
                'src' => "/media{$currentPath}/{$file->getClientOriginalName()}"
            ]
        );
    }
}