<?php

namespace App\Http\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @Route("/{_locale}/admin", name="admin_dashboard_")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="index")
     */
    public function index()
    {
        return $this->render('admin/pages/dashboard/index.html.twig');
    }
}