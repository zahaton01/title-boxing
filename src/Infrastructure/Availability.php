<?php

namespace App\Infrastructure;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
interface Availability
{
    public const AVAILABLE = 'available';
    public const NOT_AVAILABLE = 'not_available';
}