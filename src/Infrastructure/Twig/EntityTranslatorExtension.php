<?php

namespace App\Infrastructure\Twig;

use App\Infrastructure\Service\EntityTranslator;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class EntityTranslatorExtension extends AbstractExtension
{
    private ?Request $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('transEntity', [$this, 'transEntity']),
            new TwigFunction('transMultipleEntities', [$this, 'transMultipleEntities']),
        ];
    }

    public function transEntity($entity)
    {
        $translator = new EntityTranslator($this->request->getLocale());

        return $translator->trans($entity);
    }

    public function transMultipleEntities($entities)
    {
        if ($entities instanceof Collection) {
            $entities = $entities->toArray();
        }

        $translator = new EntityTranslator($this->request->getLocale());

        $result = [];
        foreach ($entities as $entity) {
            $result[] = $translator->trans($entity);
        }

        return $result;
    }
}