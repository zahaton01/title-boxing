<?php

namespace App\Infrastructure\Twig;

use App\Domain\Repository\StaticVarRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class StaticVarResolverExtension extends AbstractExtension
{
    private StaticVarRepository $staticVarRepository;

    public function __construct(StaticVarRepository $staticVarRepository)
    {
        $this->staticVarRepository = $staticVarRepository;
    }

    public function getFunctions()
    {
        return [new TwigFunction('sVar', [$this, 'sVar'])];
    }

    public function sVar(string $key)
    {
        return $this->staticVarRepository->findOneBy(['externalKey' => $key]);
    }
}