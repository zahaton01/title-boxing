<?php

namespace App\Infrastructure\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class TextUtilsExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [new TwigFunction('sliceString', [$this, 'sliceString'])];
    }

    public function sliceString(string $input, int $length = 70)
    {
        if (mb_strlen($input) > $length) {
            return trim(substr($input, 0, $length)) . '...';
        }

        return $input;
    }
}