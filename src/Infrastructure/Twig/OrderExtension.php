<?php

namespace App\Infrastructure\Twig;

use App\Domain\Entity\Commerce\Order;
use App\Infrastructure\Service\Cart\Cart;
use App\Infrastructure\Service\Cart\SessionCart;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class OrderExtension extends AbstractExtension
{
    private SessionCart $sessionCart;
    private TranslatorInterface $translator;

    public function __construct(
        SessionCart $sessionCart,
        TranslatorInterface $translator
    ) {
        $this->sessionCart = $sessionCart;
        $this->translator = $translator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('orderCart', [$this, 'orderCart']),
            new TwigFunction('badgeStatus', [$this, 'badgeStatus']),
        ];
    }

    public function orderCart(Order $order): Cart
    {
        $this->sessionCart->setProductBag($order->getCart());

        return $this->sessionCart->get();
    }

    public function badgeStatus(Order $order)
    {
        if ($order->getStatus() === Order::STATUS_CONFIRMED) {
            return "<span class='badge bg-warning'>{$this->translator->trans($order->getStatus())}</span>";
        }

        if ($order->getStatus() === Order::STATUS_DELIVERING) {
            return "<span class='badge bg-secondary'>{$this->translator->trans($order->getStatus())}</span>";
        }

        if ($order->getStatus() === Order::STATUS_DONE) {
            return "<span class='badge bg-success'>{$this->translator->trans($order->getStatus())}</span>";
        }

        return "<span class='badge bg-primary'>{$this->translator->trans($order->getStatus())}</span>";
    }
}