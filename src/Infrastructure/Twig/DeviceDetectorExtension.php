<?php

namespace App\Infrastructure\Twig;

use DeviceDetector\DeviceDetector;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class DeviceDetectorExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [new TwigFunction('isDeviceMobile', [$this, 'isDeviceMobile'])];
    }

    public function isDeviceMobile()
    {
        $dd = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);
        $dd->parse();

        return !$dd->isBot() && $dd->isMobile();
    }
}