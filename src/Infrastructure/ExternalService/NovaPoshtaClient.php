<?php

namespace App\Infrastructure\ExternalService;

use Symfony\Component\HttpClient\HttpClient;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class NovaPoshtaClient
{
    private string $apiKey;
    private string $host;

    public function __construct()
    {
        $this->apiKey = $_ENV['NOVA_POSHTA_API_KEY'];
        $this->host = $_ENV['NOVA_POSHTA_HOST'];
    }

    public function getCities()
    {
        $modelName        = 'Address';
        $calledMethod     = 'getCities';
        $methodProperties = ['' => '']; // Nova's api needs this

        $response = $this->request($modelName, $calledMethod, $methodProperties);
        $data = $response->toArray(true);

        return $data['data'];
    }

    public function getCityByRef(string $ref)
    {
        $modelName        = 'Address';
        $calledMethod     = 'getCities';
        $methodProperties = ['Ref' => $ref];

        $response = $this->request($modelName, $calledMethod, $methodProperties);
        $data = $response->toArray(true);

        return $data['data'];
    }

    public function getWarehouses(string $ref)
    {
        $modelName        = 'AddressGeneral';
        $calledMethod     = 'getWarehouses';
        $methodProperties = ['CityRef' => $ref];

        $response = $this->request($modelName, $calledMethod, $methodProperties);
        $data = $response->toArray(true);

        return $data['data'];
    }

    private function request($modelName, $calledMethod, $methodProperties)
    {
        $postFields = [
            'apiKey' => $this->apiKey,
            'modelName' => $modelName,
            'calledMethod' => $calledMethod,
            'methodProperties' => $methodProperties
        ];

        return HttpClient::create()->request(
            'POST',
            $this->host,
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($postFields)
            ]
        );
    }
}