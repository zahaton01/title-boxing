<?php

namespace App\Infrastructure;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
interface Locales
{
    public const UK = 'uk';
    public const EN = 'en';
    public const RU = 'ru';
    public const UA = 'ua';

    public const NAMES = [
        self::EN => 'English',
        self::RU => 'Russian',
        self::UK => 'Ukrainian',
        self::UA => 'Ukrainian (ua)'
    ];

    public const ALL = [
        self::UK,
        self::RU
    ];
}