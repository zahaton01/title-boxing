<?php

namespace App\Infrastructure\DataFlow\Import;

use App\Domain\Entity\Commerce\Brand;
use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductCategory;
use App\Domain\Entity\Product\ProductSpecification;
use App\Domain\Entity\Product\ProductSpecificationValue;
use App\Domain\Entity\Product\ProductTranslation;
use App\Domain\Entity\Product\Specification\Specification;
use App\Domain\Entity\Product\Specification\SpecificationValue;
use App\Domain\Repository\BrandRepository;
use App\Domain\Repository\ProductCategoryRepository;
use App\Domain\Repository\ProductRepository;
use App\Domain\Repository\SpecificationRepository;
use App\Domain\Repository\SpecificationValueRepository;
use App\Infrastructure\DataFlow\CellDataResolver;
use App\Infrastructure\Locales;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductXlsxImporter
{
    private ?UploadedFile $file = null;
    private EntityManagerInterface $em;
    private ProductRepository $productRepository;
    private ProductCategoryRepository $productCategoryRepository;
    private SpecificationRepository $specificationRepository;
    private SpecificationValueRepository $specificationValueRepository;
    private BrandRepository $brandRepository;

    public function __construct(
        EntityManagerInterface $em,
        ProductRepository $productRepository,
        ProductCategoryRepository $productCategoryRepository,
        SpecificationRepository $specificationRepository,
        SpecificationValueRepository $specificationValueRepository,
        BrandRepository $brandRepository
    ) {
        $this->em = $em;
        $this->productRepository = $productRepository;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->specificationRepository = $specificationRepository;
        $this->specificationValueRepository = $specificationValueRepository;
        $this->brandRepository = $brandRepository;
    }

    public function setFile(UploadedFile $file): void
    {
        $this->file = $file;
    }

    public function import()
    {
        /**
         * In case if import fails we remove all of these entities
         */
        $createdEntitiesStack = [];

        try {
            $spreadsheet = IOFactory::load($this->file);
            $sheet = $spreadsheet->getActiveSheet();

            for ($i = 2; $i <= $sheet->getHighestRow(); $i++) {
                $vendorCode = $sheet->getCell("A$i")->getValue();
                $name = $sheet->getCell("B$i")->getValue();
                $description = $sheet->getCell("C$i")->getValue();
                $metaTitle = $sheet->getCell("D$i")->getValue();
                $metaDescription = $sheet->getCell("E$i")->getValue();
                $metaKeywords = $sheet->getCell("F$i")->getValue();
                $price = (float) $sheet->getCell("G$i")->getValue();
                $isOnSale = $sheet->getCell("H$i")->getValue();
                $salePrice = (float) $sheet->getCell("I$i")->getValue();
                $categoryName = $sheet->getCell("J$i")->getValue();
                $brandName = $sheet->getCell("K$i")->getValue();
                $specifications = $sheet->getCell("L$i")->getValue() ?: '';
                $images = $sheet->getCell("M$i")->getValue();
                $ukName = $sheet->getCell("N$i")->getValue();
                $ukDescription = $sheet->getCell("O$i")->getValue();
                $ukMetaTitle = $sheet->getCell("P$i")->getValue();
                $ukMetaDescription = $sheet->getCell("Q$i")->getValue();
                $ukMetaKeywords = $sheet->getCell("R$i")->getValue();

                $isOnSale = CellDataResolver::boolean($isOnSale);
                $specifications = CellDataResolver::productSpecifications($specifications);
                $brand = $this->brandRepository->findOneBy(['name' => $brandName]);

                if ($categoryName !== 'Не указано' && !empty($categoryName)) {
                    $category = $this->productCategoryRepository->findOneBy(['name' => $categoryName]);

                    if (null === $category) {
                        $category = new ProductCategory();
                        $category->setLocale(Locales::RU);
                        $category->setName($categoryName);
                        $category->setSlug(uniqid('category_'));

                        $this->em->persist($category);
                        $this->em->flush();

                        $createdEntitiesStack[] = $category;
                    }
                }

                if (null === $brand && $brandName !== 'Не указано') {
                    $brand = new Brand();
                    $brand->setLocale(Locales::RU);
                    $brand->setName($brandName);

                    $this->em->persist($brand);
                    $this->em->flush();

                    $createdEntitiesStack[] = $brand;
                }


                foreach ($specifications as $specificationName => $valuesNames) {
                    $specification = $this->specificationRepository->findOneBy(['name' => $specificationName]);
                    if (null === $specification) {
                        $specification = new Specification();
                        $specification->setLocale(Locales::RU);
                        $specification->setName($specificationName);

                        $this->em->persist($specification);
                        $this->em->flush();

                        $createdEntitiesStack[] = $specification;
                    }

                    foreach ($valuesNames as $valuesName) {
                        $specificationValue = $this->specificationValueRepository->findOneBy(['name' => $valuesName]);

                        if (null === $specificationValue) {
                            $specificationValue = new SpecificationValue();
                            $specificationValue->setLocale(Locales::RU);
                            $specificationValue->setName($valuesName);
                            $specificationValue->setSpecification($specification);

                            $this->em->persist($specificationValue);
                            $this->em->flush();

                            $specification->addValue($specificationValue);
                            $createdEntitiesStack[] = $specificationValue;
                        }
                    }
                }

                $product = new Product();
                $product->setLocale(Locales::RU);
                $product->setVendorCode($vendorCode);
                $product->setName($name);
                $product->setDescription($description);
                $product->setMetaTitle($metaTitle);
                $product->setMetaDescription($metaDescription);
                $product->setMetaKeywords($metaKeywords);
                $product->setPrice($price);
                $product->setBrand($brand);

                if (isset($category)) {
                    $product->addCategory($category ?? null);
                }

                $product->setSlug(uniqid('product_'));

                if ($isOnSale) {
                    $product->setMarking(Product::MARKING_SALE);
                    $product->setSalePrice($salePrice);
                }

                $this->em->persist($product);

                if (!empty($ukName)) {
                    $productTranslation = new ProductTranslation();
                    $productTranslation->setLocale(Locales::UK);
                    $productTranslation->setProduct($product);
                    $productTranslation->setName($ukName);
                    $productTranslation->setDescription($ukDescription);
                    $productTranslation->setMetaTitle($ukMetaTitle);
                    $productTranslation->setMetaDescription($ukMetaDescription);
                    $productTranslation->setMetaKeywords($ukMetaKeywords);

                    $this->em->persist($productTranslation);
                }

                foreach ($specifications as $specificationName => $valuesNames) {
                    $specification = $this->specificationRepository->findOneBy(['name' => $specificationName]);
                    $preparedValues = [];

                    foreach ($valuesNames as $valuesName) {
                        $preparedValues[] = $this->specificationValueRepository->findOneBy(['name' => $valuesName]);
                    }

                    if (count($preparedValues) > 0) {
                        $productSpecification = new ProductSpecification();
                        $productSpecification->setProduct($product);
                        $productSpecification->setRelatedSpecification($specification);

                        $this->em->persist($productSpecification);

                        foreach ($preparedValues as $value) {
                            $productSpecificationValue = new ProductSpecificationValue();
                            $productSpecificationValue->setRelatedSpecificationValue($value);

                            $productSpecification->addValue($productSpecificationValue);
                            $this->em->persist($productSpecificationValue);
                        }
                    }
                }

                $this->em->flush();
            }
        } catch (\Exception $e) {
            foreach ($createdEntitiesStack as $item) {
                $this->em->remove($item);
            }

            $this->em->flush();
            var_dump($e->getMessage());
            var_dump($e->getTraceAsString());
            exit;
        }
    }
}