<?php

namespace App\Infrastructure\DataFlow\Export;

use App\Domain\Entity\Product\Product;
use App\Infrastructure\DataFlow\CellDataNormalizer;
use App\Infrastructure\Locales;
use App\Infrastructure\Service\EntityTranslator;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ProductXlsxExporter
{
    /** @var Product[] */
    private array $products;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->products = [];
        $this->em = $em;
    }

    public function setProducts(Product ...$products)
    {
        $this->products = $products;
    }

    public function export(): Response
    {
        $ukTranslator = new EntityTranslator(Locales::UK);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // setting titles
        $sheet->setCellValue('A1', 'Артикул');
        $sheet->setCellValue('B1', 'Название');
        $sheet->setCellValue('C1', 'Описание');
        $sheet->setCellValue('D1', 'МетаНазвание');
        $sheet->setCellValue('E1', 'МетаОписание');
        $sheet->setCellValue('F1', 'МетаКлючевыеСлова');
        $sheet->setCellValue('G1', 'Цена');
        $sheet->setCellValue('H1', 'Товар со скидкой');
        $sheet->setCellValue('I1', 'Цена со скидкой');
        $sheet->setCellValue('J1', 'Категория');
        $sheet->setCellValue('K1', 'Бренд');
        $sheet->setCellValue('L1', 'Спецификации');
        $sheet->setCellValue('M1', 'Картинки');
        $sheet->setCellValue('N1', 'НазваниеУкр');
        $sheet->setCellValue('O1', 'ОписаниеУкр');
        $sheet->setCellValue('P1', 'МетаНазваниеУкр');
        $sheet->setCellValue('Q1', 'МетаОписаниеУкр');
        $sheet->setCellValue('R1', 'МетаКлючевыеСловаУкр');

        foreach ($this->products as $index => $product) {
            foreach ($product->getTranslations() as $translation) {
                if ($translation->getLocale() === Locales::UK) {
                    /** @var Product $ukProduct */
                    $ukProduct = $ukTranslator->trans($product);
                }
            }

            $cellIndex = $index + 2;

            $categoryName = $product->getCategories()->isEmpty() ? 'Не указано' : $product->getCategories()->get(0)->getName();
            $brandName = $product->getBrand() ? $product->getBrand()->getName() : 'Не указано';

            $sheet->setCellValue("A$cellIndex", $product->getVendorCode());
            $sheet->setCellValue("B$cellIndex", $product->getName());
            $sheet->setCellValue("C$cellIndex", $product->getDescription());
            $sheet->setCellValue("D$cellIndex", $product->getMetaTitle());
            $sheet->setCellValue("E$cellIndex", $product->getMetaDescription());
            $sheet->setCellValue("F$cellIndex", $product->getMetaKeywords());
            $sheet->setCellValue("G$cellIndex", $product->getPrice());
            $sheet->setCellValue("H$cellIndex", $product->isOnSale() ? 'Да' : 'Нет');
            $sheet->setCellValue("I$cellIndex", $product->getSalePrice());
            $sheet->setCellValue("J$cellIndex", $categoryName);
            $sheet->setCellValue("K$cellIndex", $brandName);
            $sheet->setCellValue("L$cellIndex", CellDataNormalizer::productSpecifications(...$product->getSpecifications()));
            $sheet->setCellValue("M$cellIndex", CellDataNormalizer::productImages(...$product->getImages()));
            $sheet->setCellValue("N$cellIndex", isset($ukProduct) ? $ukProduct->getName() : '');
            $sheet->setCellValue("O$cellIndex", isset($ukProduct) ? $ukProduct->getDescription() : '');
            $sheet->setCellValue("P$cellIndex", isset($ukProduct) ? $ukProduct->getMetaTitle() : '');
            $sheet->setCellValue("Q$cellIndex", isset($ukProduct) ? $ukProduct->getMetaDescription() : '');
            $sheet->setCellValue("R$cellIndex", isset($ukProduct) ? $ukProduct->getMetaKeywords() : '');
        }

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Product_export_' . (new \DateTimeImmutable('now'))->format('d-m-Y_H:i:s') . '.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        $writer->save($temp_file);

        $response = new BinaryFileResponse($temp_file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $fileName);

        return $response;
    }
}