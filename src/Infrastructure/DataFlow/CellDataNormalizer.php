<?php

namespace App\Infrastructure\DataFlow;

use App\Domain\Entity\Product\ProductImage;
use App\Domain\Entity\Product\ProductSpecification;

class CellDataNormalizer
{
    public static function productSpecifications(ProductSpecification ...$productSpecifications): string
    {
        $mapped = [];
        foreach ($productSpecifications as $productSpecification) {
            if (!array_key_exists($productSpecification->getRelatedSpecification()->getName(), $mapped)) {
                $mapped[$productSpecification->getRelatedSpecification()->getName()] = [];
            }

            foreach ($productSpecification->getValues() as $productSpecificationValue) {
                $mapped[$productSpecification->getRelatedSpecification()->getName()][] = $productSpecificationValue->getRelatedSpecificationValue()->getName();
            }
        }

        $mappedSpecifications = [];
        foreach ($mapped as $specification => $values) {
            $valuesString = implode(',', $values);
            $mappedSpecifications[] = "$specification:$valuesString";
        }

        return implode(';', $mappedSpecifications);
    }

    public static function productImages(ProductImage ...$images): string
    {
        $mapped = [];
        foreach ($images as $image) {
            if ($image->getFile()) {
                $mapped[] = $_ENV['SITE_HOST'] . $image->getFile()->getRelativePath();
            }
        }

        return implode(';', $mapped);
    }
}