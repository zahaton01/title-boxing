<?php

namespace App\Infrastructure\DataFlow;

class CellDataResolver
{
    public static function boolean(string $value)
    {
        if ($value === 'Да') {
            return true;
        }

        return false;
    }

    public static function productSpecifications(string $cellData): array
    {
        if (empty($cellData)) {
            return [];
        }

        $mapped = [];

        $specifications = explode(';', $cellData);
        foreach ($specifications as $specification) {
            $specificationPieces = explode(':', $specification);
            $specificationName = trim($specificationPieces[0] ?? '');

            $specificationValues = explode(',', $specificationPieces[1] ?? '');
            $preparedValues = [];
            foreach ($specificationValues as $specificationValue) {
                if ($valueName = trim($specificationValue)) {
                    $preparedValues[] = $valueName;
                }
            }

            if (count($preparedValues) > 0 && !empty($specificationName)) {
                $mapped[$specificationName] = $preparedValues;
            }
        }

        return $mapped;
    }
}