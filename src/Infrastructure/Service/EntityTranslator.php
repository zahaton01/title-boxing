<?php

namespace App\Infrastructure\Service;

use App\Domain\Entity\Media\MediaFile;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class EntityTranslator
{
    private string $locale;

    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }

    public function trans($entity)
    {
        /**
         * Cloning entity to prevent changing fields by accident
         * For ex. by using $em->flush()
         */
        if (!is_object($entity)) {
            return $entity;
        }

        $entity = clone $entity;
        $reflectionEntity = new \ReflectionClass($entity);

        if (!$reflectionEntity->hasProperty('locale')) {
            throw new \LogicException('Entity does not support localization');
        }

        if ($entity->getLocale() === $this->locale) {
            return $entity;
        }

        $translations = $entity->getTranslations();
        $translatedEntity = null;

        /**
         * Finding translation by locale
         */
        foreach ($translations as $translation) {
            if ($translation->getLocale() === $this->locale) {
                $translatedEntity = $translation;
            }
        }

        if (null === $translatedEntity) {
            return $entity;
        }

        foreach ($reflectionEntity->getProperties() as $property) {
            $entity = $this->translateProperty($entity, $translatedEntity, $property);
        }

        // Iterating parent classes
        $entityParent = $reflectionEntity->getParentClass();
        while ($entityParent) {
            foreach ($entityParent->getProperties() as $property) {
                $entity = $this->translateProperty($entity, $translatedEntity, $property);
            }

            $entityParent = $entityParent->getParentClass();
        }

        return $entity;
    }

    private function translateProperty($entity, $translatedEntity, \ReflectionProperty $property)
    {
        $type = $property->getType() ? $property->getType()->getName() : null;

        if ($type === 'string' && $this->hasTranslatedEntityThisProperty($translatedEntity, $property)) {
            $entity = $this->rewriteEntityStringProperty($entity, $translatedEntity, $property);
        }

        if ($type === MediaFile::class && $this->hasTranslatedEntityThisProperty($translatedEntity, $property)) {
            $entity = $this->rewriteEntityMediaFileProperty($entity, $translatedEntity, $property);
        }

        return $entity;
    }

    private function rewriteEntityStringProperty($entity, $translatedEntity, \ReflectionProperty $property)
    {
        $setter = $this->getSetter($property->getName());
        $getter = $this->getGetter($property->getName());
        $translatedValue = $translatedEntity->{$getter}();

        // do noting if translation is empty
        if (empty($translatedValue)) {
            return $entity;
        }

        $entity->{$setter}($translatedValue);

        return $entity;
    }

    private function rewriteEntityMediaFileProperty($entity, $translatedEntity, \ReflectionProperty $property)
    {
        $setter = $this->getSetter($property->getName());
        $getter = $this->getGetter($property->getName());
        $translatedValue = $translatedEntity->{$getter}();

        // do noting if translation is empty
        if (empty($translatedValue)) {
            return $entity;
        }

        $entity->{$setter}($translatedValue);

        return $entity;
    }

    private function hasTranslatedEntityThisProperty($translatedEntity, \ReflectionProperty $property)
    {
        $reflectionTranslation = new \ReflectionClass($translatedEntity);
        $entityHasProperty = $reflectionTranslation->hasProperty($property->getName());

        if ($entityHasProperty) {
            return true;
        }

        $parent = $reflectionTranslation->getParentClass();
        while ($parent) {
            $entityHasProperty = $parent->hasProperty($property->getName());

            if ($entityHasProperty) {
                break;
            }

            $parent = $parent->getParentClass();
        }

        return $entityHasProperty;
    }

    private function getSetter(string $property)
    {
        return 'set' . ucfirst($property);
    }

    private function getGetter(string $property) {
        return 'get' . ucfirst($property);
    }
}