<?php

namespace App\Infrastructure\Service\Cart;

use App\Domain\Entity\Product\Product;
use App\Domain\Entity\Product\ProductSpecificationValue;

class CartProduct
{
    private Product $product;
    private array $specificationValues;
    private int $quantity;

    public function __construct(Product $product, int $quantity, ProductSpecificationValue ...$specificationValues)
    {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->specificationValues = $specificationValues;
    }

    public function __toString(): string
    {
        return (string) $this->product->getId();
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getSpecificationValues(): array
    {
        return $this->specificationValues;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getTotalPrice(): int
    {
        return $this->getOneItemPrice() * $this->quantity;
    }

    public function getOneItemPrice(): int
    {
        $price = $this->product->isOnSale() ? $this->product->getSalePrice() : $this->product->getPrice();

        foreach ($this->specificationValues as $productSpecificationValue) {
            if (!empty($productSpecificationValue->getPriceDifference())) {
                $price += $productSpecificationValue->getPriceDifference();
            }
        }

        return (int) $price;
    }
}