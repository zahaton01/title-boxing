<?php

namespace App\Infrastructure\Service\Cart;

use App\Domain\Repository\ProductRepository;
use App\Domain\Repository\ProductSpecificationValueRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionCart
{
    private array $productBag;
    private ProductRepository $productRepository;
    private SessionInterface $session;
    private ProductSpecificationValueRepository $productSpecificationValueRepository;

    public function __construct(
        ProductRepository $productRepository,
        SessionInterface $session,
        ProductSpecificationValueRepository $productSpecificationValueRepository
    ) {
        $this->productRepository = $productRepository;
        $this->session = $session;
        $this->productSpecificationValueRepository = $productSpecificationValueRepository;
        $this->productBag = $session->get('cart', []);
    }

    public function add(array $productItem): void
    {
        $contains = array_filter(
            $this->productBag,
            function ($product) use ($productItem) {
                return $product['id'] === $productItem['id'];
            }
        );

        if (count($contains) === 0) {
            $this->productBag[] = $productItem;
        }

        $this->session->set('cart', $this->productBag);
    }

    public function remove(string $productId)
    {
        foreach ($this->productBag as $index => $item) {
            if ($item['id'] === (int) $productId) {
                unset($this->productBag[$index]);
                break;
            }
        }

        $this->session->set('cart', $this->productBag);
    }

    public function changeQuantity(string $productId, int $newQuantity): void
    {
        foreach ($this->productBag as $index => $item) {
            if ($item['id'] === (int) $productId) {
                $this->productBag[$index]['quantity'] = $newQuantity;
                break;
            }
        }

        $this->session->set('cart', $this->productBag);
    }

    public function clear(): void
    {
        $this->productBag = [];
        $this->session->set('cart', $this->productBag);
    }

    public function get(): Cart
    {
        $cartProducts = [];
        foreach ($this->productBag as $item) {
            $product = $this->productRepository->find($item['id']);
            $productSpecificationValues = $this->productSpecificationValueRepository->findBy(
                ['id' => $item['product_specification_values']]
            );

            $cartProducts[] = new CartProduct($product, $item['quantity'], ...$productSpecificationValues);
        }

        return new Cart(...$cartProducts);
    }

    public function getProductBag(): array
    {
        return $this->productBag;
    }

    /**
     * Used to get cart from order
     */
    public function setProductBag(array $data): void
    {
        $this->productBag = $data;
    }
}