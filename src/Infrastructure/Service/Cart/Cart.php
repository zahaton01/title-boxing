<?php

namespace App\Infrastructure\Service\Cart;

class Cart
{
    private array $products;

    public function __construct(
        CartProduct ...$products
    ) {
        $this->products = $products;
    }

    public function getTotalPrice(): int
    {
        $price = 0;
        foreach ($this->products as $cartProduct) {
            $price += $cartProduct->getTotalPrice();
        }

        return $price;
    }

    /**
     * @return CartProduct[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function isEmpty()
    {
        return count($this->products) === 0;
    }
}