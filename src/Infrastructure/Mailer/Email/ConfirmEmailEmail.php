<?php

namespace App\Infrastructure\Mailer\Email;

use App\Domain\Entity\User\ConfirmEmailAddressCode;
use App\Domain\Entity\User\User;
use App\Infrastructure\Mailer\EmailInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfirmEmailEmail implements EmailInterface
{
    private User $user;
    private ConfirmEmailAddressCode $code;
    private TranslatorInterface $translator;

    public function __construct(User $user, ConfirmEmailAddressCode $code, TranslatorInterface $translator)
    {
        $this->user = $user;
        $this->code = $code;
        $this->translator = $translator;
    }

    public function getEnvelope(): Email
    {
        return (new TemplatedEmail())
            ->to(new Address($this->user->getEmail()))
            ->subject($this->translator->trans('Please confirm your email!'))
            ->htmlTemplate('email/confirm_email_email.html.twig')
            ->context(['code' => $this->code, 'user' => $this->user]);
    }
}