<?php

namespace App\Infrastructure\Mailer\Email;

use App\Domain\Entity\Commerce\Order;
use App\Infrastructure\Mailer\EmailInterface;
use App\Infrastructure\Mailer\SimpleMailer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Email;

class AdminOrderCreatedEmail implements EmailInterface
{
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getEnvelope(): Email
    {
        return (new TemplatedEmail())
            ->to(...SimpleMailer::getAdminEmails())
            ->subject('Заказ сформирован')
            ->htmlTemplate('email/admin_order_created.html.twig')
            ->context(['order' => $this->order]);
    }
}