<?php

namespace App\Infrastructure\Mailer\Email;

use App\Domain\Entity\User\ResetPasswordCode;
use App\Domain\Entity\User\User;
use App\Infrastructure\Mailer\EmailInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResetPasswordEmail implements EmailInterface
{
    private User $user;
    private ResetPasswordCode $code;
    private TranslatorInterface $translator;

    public function __construct(
        User $user,
        ResetPasswordCode $code,
        TranslatorInterface $translator
    ) {
        $this->user = $user;
        $this->code = $code;
        $this->translator = $translator;
    }

    public function getEnvelope(): Email
    {
        return (new TemplatedEmail())
            ->to(new Address($this->user->getEmail()))
            ->subject($this->translator->trans('Password recovery'))
            ->htmlTemplate('email/reset_password.html.twig')
            ->context(['code' => $this->code, 'user' => $this->user]);
    }
}