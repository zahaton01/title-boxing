<?php

namespace App\Infrastructure\Mailer;

use Symfony\Component\Mime\Email;

interface EmailInterface
{
    public function getEnvelope(): Email;
}