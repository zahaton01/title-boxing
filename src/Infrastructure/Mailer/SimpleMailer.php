<?php

namespace App\Infrastructure\Mailer;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class SimpleMailer
{
    private MailerInterface $mailer;
    private ContainerInterface $container;

    public function __construct(
        MailerInterface $mailer,
        ContainerInterface $container
    ) {
        $this->mailer = $mailer;
        $this->container = $container;
    }

    public function send(EmailInterface $email): void
    {
        $envelope = $email->getEnvelope();
        $envelope->from(new Address($_ENV['MAILER_FROM'], 'Title-Boxing'));

        $this->mailer->send($envelope);
    }

    public static function getAdminEmails(): array
    {
        $emails = [];
        foreach (explode(',', $_ENV['ADMIN_EMAILS']) as $item) {
            $emails[] = new Address($item);
        }

        return $emails;
    }
}