<?php

namespace App\Infrastructure\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class ExceptionListener
{
    private RouterInterface $router;
    private SessionInterface $session;
    private Security $security;

    public function __construct(
        RouterInterface $router,
        SessionInterface $session,
        Security $security
    ) {
        $this->router = $router;
        $this->session = $session;
        $this->security = $security;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        // Disabling listener in dev env (but not in ajax requests) to get detailed error messages
        if ($_ENV['APP_ENV'] === 'dev' && !$this->isApiException($event)) {
            return;
        }

        if ($this->isApiException($event)) {
            $this->handleApiException($event);
        }

        var_dump($event->getThrowable()->getMessage());
        var_dump($event->getThrowable()->getTraceAsString());
        exit;
    }

    private function handleApiException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse(['msg' => $exception->getMessage()], 500);

        $event->setResponse($response);
    }

    private function isApiException(ExceptionEvent $event)
    {
        return strpos($event->getRequest()->getRequestUri(), 'api') !== false;
    }
}