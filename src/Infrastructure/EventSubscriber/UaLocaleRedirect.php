<?php

namespace App\Infrastructure\EventSubscriber;

use App\Infrastructure\Locales;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UaLocaleRedirect implements EventSubscriberInterface
{
    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $pathInfo = $requestEvent->getRequest()->getPathInfo();
        $pieces = explode('/', $pathInfo);
        if ($pieces[1] === Locales::UA || !in_array($pieces[1], Locales::ALL)) {
            $pieces[1] = Locales::UK;

            $requestEvent->setResponse(new RedirectResponse(implode('/', $pieces)));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 33]],
        ];
    }
}