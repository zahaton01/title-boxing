<?php

namespace App\Infrastructure\EventSubscriber;

use App\Domain\Repository\PageRepository;
use App\Infrastructure\Locales;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;

class LocaleSubscriber implements EventSubscriberInterface
{
    private RouterInterface $router;
    private PageRepository $pageRepository;

    public function __construct(RouterInterface $router, PageRepository $pageRepository)
    {
        $this->router = $router;
        $this->pageRepository = $pageRepository;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($locale = $request->attributes->get('_locale')) {
            if (!in_array($locale, [Locales::RU, Locales::UK])) {
                $locale = Locales::UK;
            }

            $request->getSession()->set('_locale', $locale);
        } else {
            $locale = explode('_', $request->getPreferredLanguage())[0];
            if (!in_array($locale, [Locales::RU, Locales::UK])) {
                $locale = Locales::UK;
            }

            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', $locale));
        }

        $routeExists = false;
        foreach($this->router->getRouteCollection() as $routeObject) {
            $routePath = $routeObject->getPath();

            if (str_contains($request->getPathInfo(), '/page')) {
                $pathInfo = $request->getPathInfo();
                $pieces = explode('/', $pathInfo);

                if ($pieces[1] === 'page') {
                    $routeExists = true;
                    break;
                }
            }

            if ($routePath === "/{_locale}{$request->getPathInfo()}") {
                $routeExists = true;
                break;
            }
        }

        if($routeExists) {
            $event->setResponse(new RedirectResponse("/{$request->getLocale()}{$request->getPathInfo()}"));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 34]],
        ];
    }
}