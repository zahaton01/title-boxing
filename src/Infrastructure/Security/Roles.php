<?php

namespace App\Infrastructure\Security;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
interface Roles
{
    public const ADMIN = 'ROLE_ADMIN';
    public const USER = 'ROLE_USER';
}