<?php

namespace App\Infrastructure\Helpers;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class QueryUtil
{
    public static function getMultipleFilter(string $value)
    {
        return explode(',', $value);
    }

    public static function prepareSearchString(string $search)
    {
        $search = str_replace('+', ' ', $search);
        return mb_strtolower($search);
    }

    public static function nullOrDate(string $dateString)
    {
        try {
            $date = new \DateTimeImmutable($dateString);
        } catch (\Exception $e) {
            return null;
        }

        return $date;
    }
}