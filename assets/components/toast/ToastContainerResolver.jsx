import React from "react";
import {ToastContainer} from "react-toastify";
import {toastFromQuery} from "./toast";

class ToastContainerResolver extends React.Component {
    componentDidMount() {
        toastFromQuery();
    }

    render() {
        return (
            <ToastContainer/>
        )
    }
}

export default ToastContainerResolver;