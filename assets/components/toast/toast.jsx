import {toast} from 'react-toastify';
import {MdError, FaThumbsUp} from 'react-icons/all';
import React from 'react';

const errorToast = (content) => {
    const options = {
        position: 'bottom-right',
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <MdError/>
            <span style={{'margin-left': '5px', 'font-size': '16px', 'font-weight': '500'}}>{content}</span>
        </>
    );

    toast.error(body, options);
};

const successToast = (content) => {
    const options = {
        position: 'bottom-right',
        autoClose: 1500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <FaThumbsUp/>
            <span style={{'margin-left': '5px', 'font-size': '16px', 'font-weight': '500'}}>{content}</span>
        </>
    );

    toast.success(body, options);
};

const toastFromQuery = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);

    if (urlSearchParams.has('toast_success')) {
        successToast(urlSearchParams.get('toast_success'));
    }

    if (urlSearchParams.has('toast_error')) {
        errorToast(urlSearchParams.get('toast_error'));
    }
}

export {
    errorToast,
    successToast,
    toastFromQuery
};