import React from 'react';
import NumericInput from 'react-numeric-input';
import siteClient from "../../../../../infrastructure/api/client/siteClient";

class CartItem extends React.Component {
    removeItem = () => {
        const id = this.props.cartProduct['related_product'].id;

        siteClient().delete(`/cart/${id}`).then(res => {
            this.props.removeItem(id);
        });
    }

    onChangeQuantity = (newQuantity) => {
        const id = this.props.cartProduct['related_product'].id;

        siteClient().patch(`/cart/${id}/change-quantity`, {new_quantity: newQuantity}).then(res => {
            this.props.changeQuantity(id, newQuantity);
        });
    }

    render() {
        return (
            <div className="card shadow my-2">
                <div className="card-body">
                    <div className="row justify-content-center border-bottom" style={{paddingBottom: '10px'}}>
                        <div className="col-xl-1 col-lg-1 col-3">
                            <img className="img-fluid" src={this.props.cartProduct['related_product'].img} />
                        </div>
                        <div className="col-xl-8 col-lg-8 col-6">
                            <a target={'_blank'} href={`/${APP_LOCALE}/product/${this.props.cartProduct['related_product'].slug}`} className="sub-title">
                                {this.props.cartProduct['related_product'].name}
                            </a>
                        </div>
                        <div className="col-xl-2 col-lg-2 col-3 text-center">
                            <button onClick={this.removeItem} className="btn btn-danger btn-sm">{REMOVE_TEXT}</button>
                        </div>
                    </div>
                    <div className="row justify-content-center mt-1 text-center">
                        {
                            this.props.cartProduct['specification_values'].map(productSpecificationValue => {
                                return (
                                    <div key={productSpecificationValue.id} className="col-xl-3 col-lg-3 col-md-12 col-12 px-0 d-flex justify-content-center align-items-center">
                                        <div className="sub-title">
                                            {productSpecificationValue['related_specification'].name}: {productSpecificationValue['related_value'].name}
                                        </div>
                                    </div>
                                );
                            })
                        }
                        <div className="col-xl-3 col-lg-3 col-md-6 col-6 px-0 d-flex justify-content-center align-items-center">
                            <div className="sub-title">
                                <span>{QUANTITY_TEXT}: </span>
                                <NumericInput
                                    className="form-control"
                                    min={1}
                                    max={99}
                                    value={this.props.cartProduct['quantity']}
                                    onChange={this.onChangeQuantity}
                                />
                            </div>
                        </div>
                        <div className="col-xl-3 col-lg-3 col-md-6 col-6 px-0 d-flex justify-content-center align-items-center">
                            <div className="sub-title">
                                {PRICE_TEXT}: {this.props.cartProduct['total_price']} {UAH_TEXT}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
    }
}

export default CartItem;