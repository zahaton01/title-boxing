import React from 'react';
import CartItem from "./CartItem";
import siteClient from "../../../../../infrastructure/api/client/siteClient";
import {ToastContainer} from "react-toastify";

class Cart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cart_products: [],
            total_price: 0
        }
    }

    componentDidMount() {
        siteClient().get('/cart/get').then(res => {
            this.setState({...this.state, ...res.data});
        })
    }

    removeItem = (id) => {
        let items = this.state['cart_products'];

        for (let i = 0; i < items.length; i++) {
            if (items[i]['related_product'].id === id) {
                items.splice(i, 1);
            }
        }

        let price = 0;
        for (const item of items) {
            price += item['total_price'];
        }

        this.setState({cart_products: items, total_price: price});
    }

    changeQuantity = (id, newQuantity) => {
        let items = this.state['cart_products'];

        for (let i = 0; i < items.length; i++) {
            if (items[i]['related_product'].id === id) {
                items[i]['quantity'] = Number(newQuantity);
                items[i]['total_price'] = items[i]['one_item_price'] * items[i]['quantity'];
            }
        }

        let price = 0;
        for (const item of items) {
            price += item['total_price'];
        }

        this.setState({cart_products: items, total_price: price});
    }

    render() {
        return (
            <>
                {
                    this.state.cart_products.length === 0 &&
                        <h3>{EMPTY_CART_TEXT}</h3>
                }
                {
                    this.state.cart_products.map(cartProduct => {
                        return <CartItem
                            key={cartProduct['related_product'].id}
                            cartProduct={cartProduct}
                            removeItem={this.removeItem}
                            changeQuantity={this.changeQuantity}
                        />
                    })
                }
                <div className="row justify-content-end mt-3">
                    <div className="col-xl-4 col-lg-6 col-md-12 col-12 text-end">
                        <div className="sub-title">
                            {TOTAL_PRICE_TEXT}: {this.state.total_price} {UAH_TEXT}
                        </div>
                        <div className="mt-3">
                            <a href={ROUTE_CHECKOUT} className="btn btn-dark">
                                <span className="sub-title">{CHECKOUT_TEXT}</span>
                            </a>
                        </div>
                    </div>
                </div>
                <ToastContainer/>
            </>
        )
    }
}

export default Cart;