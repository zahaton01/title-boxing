import '../../assets/styles/main.scss';
import './cart.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import Cart from "./components/Cart";

ReactDOM.render(
    <Cart />,
    document.getElementById('cart'),
);
