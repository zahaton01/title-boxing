import './checkout.scss';
import '../../assets/styles/main.scss';
import 'selectize/dist/js/selectize';
import 'selectize/dist/css/selectize.bootstrap3.css';

$(document).ready(function () {
   const toggleDeliveryFields = () => {
      const courierFields = $('#courier-delivery-fields');
      const novaFields = $('#nova-poshta-delivery-fields');
      const deliveryType = $('#user_order_deliveryType').val();

      if (deliveryType === 'nova_poshta_warehouse') {
         courierFields.hide();
         novaFields.show();
      }

      if (deliveryType === 'courier') {
         courierFields.show();
         novaFields.hide();
      }
   }

   toggleDeliveryFields();
   $('#user_order_deliveryType').change(toggleDeliveryFields);

   const setNovaWarehouses = (cityRef) => {
      if ('' === cityRef) {
         return;
      }

      $.ajax({
         url: `/${APP_LOCALE}/checkout/nova/cities/${cityRef}/warehouses`,
         method: 'GET',
         success: function (res) {
            const novaWarehouseSelect = $('#user_order_novaPoshtaWarehouse');
            novaWarehouseSelect.html('');

            for (const novaWarehouse of res) {
               let name = novaWarehouse['Description'];

               if (APP_LOCALE === 'ru') {
                  name = novaWarehouse['DescriptionRu'];
               }

               novaWarehouseSelect.append(`<option value="${name}">${name}</option>`);
            }
         }
      })
   }

   const novaCitySelect = $('#user_order_novaPoshtaCity');

   if ('' !== novaCitySelect.val()) {
      setNovaWarehouses(novaCitySelect.val());
   }

   novaCitySelect.change(function () {
      setNovaWarehouses($(this).val());
   });

   novaCitySelect.selectize({
      sortField: 'text'
   });
});