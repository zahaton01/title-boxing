import './product.scss';
import 'bootstrap/js/src/carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick';
import '../../assets/styles/main.scss';
import 'magnific-popup/dist/jquery.magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';
import React from 'react';
import ReactDOM from 'react-dom';
import ProductComments from "./comments/components/ProductComments";
import AddToCart from "./add-to-cart/AddToCart";

$(document).ready(function () {
    $('.product-images-slick-main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-images-slick'
    });

    $('.product-images-slick').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-images-slick-main',
        focusOnSelect: true
    });

    $('.slider-img-main').magnificPopup({
        type: 'image'
    });

    $('.product-row-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});

ReactDOM.render(
    <ProductComments />,
    document.getElementById('product-comments'),
);

ReactDOM.render(
    <AddToCart />,
    document.getElementById('add-to-cart'),
);
