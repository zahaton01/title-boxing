import React from 'react';
import SpecificationsList from "./SpecificationsList";
import {errorToast, successToast} from "../../../../../components/toast/toast";
import siteClient from "../../../../../infrastructure/api/client/siteClient";
import {ToastContainer} from "react-toastify";

class AddToCart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            specifications: [],
            isOnSale: PRODUCT_IS_ON_SALE,
            price: PRODUCT_PRICE,
            salePrice: PRODUCT_SALE_PRICE,
            initialPrice: PRODUCT_PRICE,
            initialSalePrice: PRODUCT_SALE_PRICE,
            inCart: IS_PRODUCT_IN_CART,
            inFav: IS_PRODUCT_IN_FAV
        }
    }

    componentDidMount() {
        siteClient().get(`/product/${PRODUCT}/specifications`).then(res => {
            for (const spec of res.data) {
                spec.activeValue = Number(spec['specification_values'][0]['id']);
            }

            this.setState({specifications: res.data});
            this.changePrice();
        });
    }

    setSpecificationValueActive = (specId, valueId) => {
        const specifications = this.state.specifications;

        for (const spec of specifications) {
            if (specId === spec['id']) {
                spec.activeValue = Number(valueId);

                this.setState({specifications: specifications})
                break;
            }
        }
    }

    changePrice = () => {
        let initialPrice = this.state.initialPrice;
        let initialSalePrice = this.state.initialSalePrice;

        for (const spec of this.state.specifications) {
            for (const val of spec['specification_values']) {
                if (spec.activeValue === val['id']) {
                    initialPrice += Number(val['price_difference']);
                    initialSalePrice += Number(val['price_difference']);
                    break;
                }
            }
        }

        this.setState({
            price: initialPrice,
            salePrice: initialSalePrice
        })
    }

    addToCart = () => {
        const data = {
            id: PRODUCT,
            quantity: 1,
            product_specification_values: []
        };

        for (const specification of this.state.specifications) {
            data.product_specification_values.push(specification.activeValue);
        }

        siteClient().post(`/product/${PRODUCT}/add-to-cart`, data).then(res => {
            this.setState({inCart: true})
            successToast(IN_CART);
        });
    }

    addToFav = () => {
        siteClient().post(`/product/${PRODUCT}/add-to-favourites`, {}).then(res => {
            this.setState({inFav: true});
            successToast(IN_FAVOURITES);
        }).catch(err => {
            errorToast(err.response.data);
        });
    }

    render() {
        return (
            <>
                {this.state.isOnSale ?
                    <>
                        <span className={'old-price'}>{this.state.price} {UAH}</span>
                        <span className={'new-price'}>{this.state.salePrice} {UAH}</span>
                    </> :
                    <span className={'price'}>{this.state.price} {UAH}</span>
                }
                <SpecificationsList
                    specifications={this.state.specifications}
                    setSpecificationValueActive={this.setSpecificationValueActive}
                    changePrice={this.changePrice}
                />
                <div className="mt-1 text-center mt-3 mb-3">
                    <>
                        <button disabled={this.state.inFav} onClick={this.addToFav} className="btn btn-transparent add-to-fav">
                            {this.state.inFav
                                ? <i className="fas fa-heart" style={{fontSize: '1.8rem'}}/>
                                : <i className="far fa-heart" style={{fontSize: '1.8rem'}}/>
                            }
                        </button>
                        <button
                            disabled={this.state.inCart || IS_PRE_ORDER}
                            onClick={this.addToCart}
                            className="btn btn-dark"
                        >{this.state.inCart ? IN_CART : ADD_TO_CART}
                        </button>
                    </>
                </div>
                <ToastContainer/>
            </>
        )
    }
}

export default AddToCart;