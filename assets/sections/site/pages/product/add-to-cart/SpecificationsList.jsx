import React from 'react';

class SpecificationsList extends React.Component {
    onSpecificationChange = (event) => {
        const specificationId = Number(event.currentTarget.name.split('-')[1]);
        const valueId = Number(event.currentTarget.value);

        this.props.setSpecificationValueActive(specificationId, valueId);
        this.props.changePrice();

        const relatedSlide = $(`.product-images-slick div[data-related-specification-value="${valueId}"]`);
        if (relatedSlide.length > 0 ) {
            $('.product-images-slick').slick('slickGoTo', relatedSlide.data('slide'));
        }
    }

    render() {
        return (
            this.props.specifications.map(productSpecification => {
                    return (
                        <div key={productSpecification.id} className="mt-1 variety">
                            <h6 className="sub-title">{productSpecification['related_specification'].name}</h6>
                            {
                                productSpecification['specification_values'].map(productSpecificationValue => {
                                    return (
                                        <div key={productSpecificationValue.id} className="form-check form-check-inline">
                                            <input
                                                onChange={this.onSpecificationChange}
                                                name={`specification-${productSpecification.id}`}
                                                className="form-check-input"
                                                type="radio"
                                                id={`specification-${productSpecification.id}-value-${productSpecificationValue.id}`}
                                                checked={productSpecificationValue.id === productSpecification.activeValue}
                                                value={productSpecificationValue.id}
                                                disabled={productSpecificationValue['availability'] !== 'available'}
                                            />
                                            <label
                                                className="form-check-label"
                                                htmlFor={`specification-${productSpecification.id}-value-${productSpecificationValue.id}`}
                                            >
                                                {productSpecificationValue['related_value'].name}
                                            </label>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    )
                })
        );
    }
}

export default SpecificationsList;