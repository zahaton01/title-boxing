import React from 'react';
import {errorToast, successToast} from "../../../../../../components/toast/toast";
import ReactStars from "react-rating-stars-component";
import siteClient from "../../../../../../infrastructure/api/client/siteClient";

class AddProductComment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            rating: 3,
            comment: ''
        }
    }

    publishComment = () => {
        if (this.state.name === '') {
            errorToast(NAME_CANNOT_BE_EMPTY);
            return;
        }

        if (this.state.comment === '') {
            errorToast(COMMENT_CANNOT_BE_EMPTY);
            return;
        }

        siteClient().post(`/product/${PRODUCT}/comments`, {
            author_name: this.state.name,
            stars: this.state.rating,
            text: this.state.comment
        }).then((res) => {
            this.props.addComment(res.data);
            successToast(COMMENT_ADDED_MESSAGE);

            this.setState({
                name: '',
                comment: '',
                stars: 3
            })
        })
    }

    handleNameChange = (event) => {
        this.setState({
            name: event.target.value
        });
    }

    handleTextChange = (event) => {
        this.setState({
            comment: event.target.value
        });
    }

    ratingChanged = (newRating) => {
        this.setState({rating: newRating});
    }

    render() {
        return (
            <div className="col-xl-3 col-lg-4 col-md-6 col-12 mob-mt">
                <div className="card shadow">
                    <div className="card-body">
                        <div className="sub-title mb-3">
                            {ADD_COMMENT_HEADER}
                        </div>
                        <div className="mb-3">
                            <label htmlFor="exampleFormControlInput1" className="form-label">{WHAT_IS_YOUR_NAME}</label>
                            <input onChange={this.handleNameChange} value={this.state.name} type="text" className="form-control" placeholder="Thomas..."/>
                        </div>
                        <div className="mb-3">
                            <div>{YOUR_RATING}</div>
                            <div className="hearts">
                                <ReactStars
                                    onChange={this.ratingChanged}
                                    count={5}
                                    size={15}
                                    value={this.state.rating}
                                    activeColor="#9E0909"
                                    color="#9E0909"
                                    emptyIcon={<i className="far fa-heart"></i>}
                                    filledIcon={<i className="fa fa-heart"></i>}
                                    isHalf={false}
                                />
                            </div>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="comment" className="form-label">{YOUR_TEXT}</label>
                            <textarea onChange={this.handleTextChange} value={this.state.comment} className="form-control" name="comment" />
                        </div>
                        <div className="text-center">
                            <button className="btn btn-dark" onClick={this.publishComment}>{PUBLISH_BUTTON}</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddProductComment;