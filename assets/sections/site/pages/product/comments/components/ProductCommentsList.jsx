import React from "react";
import ReactStars from "react-rating-stars-component/dist/react-stars";

class ProductCommentsList extends React.Component {
    render() {
        return (
            <div className="col-xl-9 col-lg-8 col-md-6 col-12">
                {this.props.comments.length === 0 &&
                    <h6>{NO_COMMENTS}</h6>
                }
                {this.props.comments.map(comment => {
                    return (
                        <div className="card shadow mb-1" key={comment.id}>
                            <div className="card-body comment-text">
                                <div className="hearts">
                                    <ReactStars
                                        count={5}
                                        size={15}
                                        value={comment['stars']}
                                        activeColor="#9E0909"
                                        color="#9E0909"
                                        emptyIcon={<i className="far fa-heart"></i>}
                                        filledIcon={<i className="fa fa-heart"></i>}
                                        edit={false}
                                        isHalf={false}
                                    />
                                </div>
                                <div className="sub-title">{comment['author_name']}</div>
                                <div>{comment['text']}</div>
                            </div>
                        </div>
                    )
                })}
                {this.props.comments.length !== 0 &&
                    <div className={'row mt-2 justify-content-center'}>
                        <div className={'col-lg-3 col-md-3 col-12'}>
                            <button onClick={this.props.loadMore} className={'btn btn-dark'}>{LOAD_MORE}</button>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default ProductCommentsList;