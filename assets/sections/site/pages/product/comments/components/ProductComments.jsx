import React from 'react';
import ProductCommentsList from "./ProductCommentsList";
import AddProductComment from "./AddProductComment";
import siteClient from "../../../../../../infrastructure/api/client/siteClient";
import {ToastContainer} from "react-toastify";

class ProductComments extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            comments: []
        }
    }

    componentDidMount() {
        this.fetchComments();
    }

    fetchComments = () => {
        siteClient().get(`/product/${PRODUCT}/comments?page=${this.state.page}`).then(response => {
            this.setState({comments: [...this.state.comments, ...response.data]});
        });
    }

    loadMore = () => {
        this.state.page = ++this.state.page;
        this.fetchComments();
    }

    addComment = (comment) => {
        this.setState({comments: [comment, ...this.state.comments]})
    }

    render() {
        return (
            <div className="row">
                <ProductCommentsList
                    comments={this.state.comments}
                    loadMore={this.loadMore}
                />
                <AddProductComment addComment={this.addComment} />
                <ToastContainer/>
            </div>
    )
    }
}

export default ProductComments;