import 'nouislider/distribute/nouislider.css';
import './catalog.scss';
import '../../assets/styles/main.scss';
import noUiSlider from 'nouislider';

$(document).ready(function () {
    const createFilters = () => {
        let filters = {
            specifications: [],
            min_price: 0,
            max_price: 12499,
            brands: [],
            search: '',
            marking: ''
        };

        filters.search = $('#catalog-search').val();
        filters.marking = $('#catalog-marking').val();

        $('.js-brand').each(function () {
            if ($(this).is(':checked')) {
                filters.brands.push($(this).data('id'));
            }
        })

        $('.js-specification').each(function () {
            let specification = {};
            let values = [];

            $(this).find('input').each(function () {
                if ($(this).is(':checked')) {
                    values.push(Number($(this).data('id')));
                }
            });

            if (values.length > 0) {
                specification.id = Number($(this).data('id'));
                specification.values = values;
                filters.specifications.push(specification);
            }
        });

        filters['min_price'] = Number($('#price-from').val());
        filters['max_price'] = Number($('#price-to').val());

        return JSON.stringify(filters);
    }

    const urlSearchParams = new URLSearchParams(window.location.search);

    if (urlSearchParams.has('catalog_search')) {
        $('#catalog-search').val(urlSearchParams.get('catalog_search'));

        const filters = createFilters();
        window.location.href = `/${APP_LOCALE}/catalog?filters=${filters}`;
    }

    if (urlSearchParams.has('marking_search')) {
        $('#catalog-marking').val(urlSearchParams.get('marking_search'));

        const filters = createFilters();
        window.location.href = `/${APP_LOCALE}/catalog?filters=${filters}`;
    }

    const priceSlider = document.getElementById('price-slider');

    noUiSlider.create(priceSlider, {
        start: [0, 12499],
        connect: true,
        step: 1,
        range: {
            'min': 0,
            'max': 12499
        }
    });

    priceSlider.noUiSlider.set([Number($('#price-from').val()), Number($('#price-to').val())]);

    priceSlider.noUiSlider.on('slide', function () {
        const [min, max] = priceSlider.noUiSlider.get();
        $('#price-from').val(Number(min));
        $('#price-to').val(Number(max));
    });

    $('#price-from, #price-to').on('input change', function () {
        priceSlider.noUiSlider.set([Number($('#price-from').val()), Number($('#price-to').val())]);
    });

    $('#catalog-form').on('submit', function () {
        $('#catalog-form input[name="filters"]').val(createFilters());
    });
});

