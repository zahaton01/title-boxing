import './assets/styles/main.scss';
import './assets/styles/icons/font-awesome-icons.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'bootstrap/scss/bootstrap.scss';
import 'bootstrap/js/src/collapse';
import 'bootstrap/js/src/dropdown';
import 'bootstrap/js/src/modal';
import 'slick-carousel/slick/slick'
import 'react-toastify/dist/ReactToastify.css';
import ReactDOM from "react-dom";
import ToastContainerResolver from "../../components/toast/ToastContainerResolver";
import React from "react";

const toggleMenus = () => {
    // if (window.innerWidth > 1000) {
    //     $('#js-desktop-mobile-header').hide();
    //     $('#js-desktop-header').show();
    // } else {
    //     $('#js-desktop-mobile-header').show();
    //     $('#js-desktop-header').hide();
    // }

    return;
}

$(document).ready(function () {
    toggleMenus();

    $('#js-desktop-header .categories-dropdown-toggle').on('click', function () {
        const dropdown = $('#js-desktop-header .categories-dropdown');
        dropdown.is(':visible') ? dropdown.hide() : dropdown.show();
    });

    $('#js-desktop-mobile-header .menu-dropdown-toggle').on('click', function () {
        const dropdown = $('#js-desktop-mobile-header .mobile-dropdown');
        dropdown.is(':visible') ? dropdown.hide() : dropdown.show();
    });

    $('.sub-menu-parent').on('mouseover', function () {
        $('.sub-menu-parent').removeClass('active');
        $(this).addClass('active');

        $('.sub-menu-container').removeClass('active');
        $($(this).data('target')).addClass('active');

        return false;
    });

    $('.icon-holder').on('click', function () {
        if ($(this).hasClass('active')) {
            const icon = $(this).data('init-icon');
            $(this).html(`<i class="${icon}"></i>`);
            $(this).removeClass('active');
        } else {
            $(this).html(`<i class="fa fa-times-circle"></i>`)
            $(this).addClass('active');
        }
    });
});

window.addEventListener('resize', toggleMenus);

ReactDOM.render(
    <ToastContainerResolver />,
    document.getElementById('toast-container'),
);
