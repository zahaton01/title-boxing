import './assets/styles/user-ui.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick'
import React from 'react';
import ReactDOM from 'react-dom';
import 'react-toastify/dist/ReactToastify.css';
import ToastContainerResolver from "../../components/toast/ToastContainerResolver";
import 'jquery-ui-dist/jquery-ui.min.css';
import 'jquery-ui-dist/jquery-ui.min';
import 'jquery-ui-dist/jquery-ui.theme.min.css';
import './assets/js/datepicker-localization';
import {datepicker_uk, datepicker_ru} from "./assets/js/datepicker-localization";
import '../site/assets/styles/icons/font-awesome-icons.min.css';
import 'react-toastify/dist/ReactToastify.css';

if ($('.liked-products-slider').length > 0) {
    $('.liked-products-slider').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        centerMode: true,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}

if ($('#profile_birthday').length > 0) {
    let options = {
        changeYear: true,
        dateFormat: 'dd-mm-yy',
        yearRange: '1950:2021',
        changeMonth: true
    };

    if (APP_LOCALE !== 'en') {
        if (APP_LOCALE === 'uk') {
            options = {...options, ...datepicker_uk};
        }

        if (APP_LOCALE === 'ru') {
            options = {...options, ...datepicker_ru};
        }
    }

    $('#profile_birthday').datepicker(options);
}

ReactDOM.render(
    <ToastContainerResolver />,
    document.getElementById('toast-container'),
);
