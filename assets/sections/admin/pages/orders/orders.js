import 'selectize/dist/js/selectize';
import 'selectize/dist/css/selectize.bootstrap3.css';

$(document).ready(function () {
    const toggleDeliveryFields = () => {
        const courierFields = $('#courier-delivery-fields');
        const novaFields = $('#nova-poshta-delivery-fields');
        const deliveryType = $('#admin_order_deliveryType').val();

        if (deliveryType === 'nova_poshta_warehouse') {
            courierFields.hide();
            novaFields.show();
        }

        if (deliveryType === 'courier') {
            courierFields.show();
            novaFields.hide();
        }
    }

    toggleDeliveryFields();
    $('#admin_order_deliveryType').change(toggleDeliveryFields);

    $('#admin_order_user').selectize({
        sortField: 'text'
    });
});