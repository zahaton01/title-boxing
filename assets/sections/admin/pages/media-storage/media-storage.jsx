import './media-storage.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import MediaStorage from "./components/MediaStorage";

ReactDOM.render(
    <MediaStorage />,
    document.getElementById('media-storage'),
);