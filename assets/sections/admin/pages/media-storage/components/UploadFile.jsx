import React from 'react';
import {errorToast, successToast} from "../../../../../components/toast/toast";
import adminClient from "../../../../../infrastructure/api/client/adminClient";

class UploadFile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            onUpload: false,
            src: '',
            path: '',
            file: null
        }
    }

    toggleUpload = () => {
        const status = !this.state.onUpload;
        status ? this.props.setActive('upload-file') : this.props.setActive(null);

        this.setState({
            onUpload: status,
            src: '',
            path: '',
            file: null
        });
    }

    handleChange = (e) => {
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onload = (event) => {
            this.setState({src: event.target.result});
        }

        reader.readAsDataURL(file);

        this.setState({
            path: e.target.value,
            file: file
        })
    }

    upload = () => {
        const formData = new FormData();
        formData.append("file", this.state.file);
        formData.append('current_path', this.props.getCurrentPath());

        adminClient().post('/media-storage/upload-file', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            this.props.setMedia([res.data, ...this.props.getMedia()]);
            this.toggleUpload();
            successToast('Success');
        }).catch(err => {
            errorToast(err.response.data.msg);
        })
    }

    render() {
        return (
            <>
                {!this.state.onUpload &&
                <button className={'btn btn-info'} onClick={this.toggleUpload}>Upload file to this directory</button>}
                {this.state.onUpload &&
                <>
                    <h6>Choose file</h6>
                    <div className="custom-file">
                        <input accept="image/*" onChange={this.handleChange} type="file" className="custom-file-input" id="file"/>
                        <label className="custom-file-label"
                               htmlFor="file">{this.state.path === '' ? 'Browse files' : this.state.path}</label>
                    </div>
                    {this.state.src !== '' &&
                    <div className="mt-1 mb-3">
                        <img src={this.state.src} style={{'max-height': '512px'}} className="img-thumbnail"
                             id="image-preview"/>
                    </div>
                    }
                    <button style={{'margin-top': '25px'}} className={'btn btn-light'} onClick={this.upload}>Upload
                    </button>
                    <button style={{'margin-top': '25px'}} className={'btn btn-danger'}
                            onClick={this.toggleUpload}>Cancel
                    </button>
                </>
                }
            </>
        );
    }
}

export default UploadFile;