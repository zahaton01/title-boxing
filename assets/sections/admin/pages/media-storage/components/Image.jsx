import React from 'react';
import {MdDelete, FaHandPointer} from "react-icons/all";
import {errorToast, successToast} from "../../../../../components/toast/toast";
import adminClient from "../../../../../infrastructure/api/client/adminClient";

class Image extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: false
        }
    }

    toggleExpanded = () => {
        this.setState({expanded: !this.state.expanded})
    }

    delete = () => {
        adminClient().patch('/media-storage/remove-file', {current_path: this.props.getCurrentPath(), name: this.props.media['path_name']})
            .then(() => {
                const media = this.props.getMedia();

                media.forEach((item, index) => {
                    if (item['is_image'] && item['path_name'] === this.props.media['path_name']) {
                        media.splice(index, 1);
                    }
                })

                this.props.setMedia(media);

                successToast('Success');
            })
            .catch(err => {
                errorToast(err.response.data.msg);
            });
    }

    select = () => {
        document.getElementById(this.props.browseDataTarget).value = this.props.media['src'];
        document.getElementById(this.props.browseDataTarget + 'Img').setAttribute('src', this.props.media['src']);
    }

    render() {
        return (
            <>
                <li className="list-group-item image">
                    <button onClick={this.toggleExpanded} className={'btn btn-sm btn-link'}>
                        <img src={this.props.media['src']} width={'32px'} height={'32px'}/>
                        <span className={'image-title'}>{this.props.media['path_name']}</span>
                    </button>
                    <button onClick={this.delete} className={'btn btn-light delete-button'}>
                        <MdDelete/>
                    </button>
                    {this.props.browseDataTarget &&
                        <button onClick={this.select} className={'btn btn-light browse-select-button'}>
                            <FaHandPointer />
                            <span>Select</span>
                        </button>
                    }
                    {this.state.expanded &&
                        <>
                            <h6 style={{'margin-top': '10px'}}>Absolute url</h6>
                            <input className={'form-control'} value={process.env.SITE_HOST + this.props.media['src']}/>
                            <h6 style={{'margin-top': '10px'}}>Relative url</h6>
                            <input className={'form-control'} value={this.props.media['src']}/>
                        </>
                    }
                </li>
            </>
        )
    }
}

export default Image;