import React from 'react';
import Directory from "./Directory";
import Image from "./Image";
import {ToastContainer} from "react-toastify";
import ActionButtons from "./ActionButtons";
import adminClient from "../../../../../infrastructure/api/client/adminClient";

class MediaStorage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentPath: '',
            media: []
        }
    }

    componentDidMount() {
        this.getMediaList('');
    }

    getMediaList = (currentPath) => {
        adminClient().get(`/media-storage/list?current_path=${currentPath}`).then(res => {
            const dirs = res.data.filter(item => {
                return item['is_dir'];
            });

            dirs.sort((a, b) => {
                return a['creation_date'] - b['creation_date'];
            })

            let images = res.data.filter(item => {
                return item['is_image'];
            });

            images = images.sort((a, b) => {
                return a['creation_date'] - b['creation_date'];
            })

            this.setState({media: [...dirs, ...images]});
        })
    }

    openDir = (name) => {
        const newPath = this.state.currentPath + '/' + name;

        this.setState({currentPath: newPath});
        this.getMediaList(newPath);
    }

    openParentDir = () => {
        const pieces = this.state.currentPath.split('/');
        if (pieces.length > 0) {
            pieces.splice(-1, 1);
        }

        const newPath = pieces.join('/');

        this.setState({currentPath: newPath});
        this.getMediaList(newPath);
    }

    getMedia = () => {
        return this.state.media;
    }

    setMedia = (media) => {
        this.setState({media: media});
    }

    getCurrentPath = () => {
        return this.state.currentPath;
    }

    syncDbAndFilesystem = () => {
        adminClient().get('/media-storage/sync-db-and-file-system').then(() => {
            //
        })
    }

    render() {
        return (
            <>
                <h3>Current path: {this.state.currentPath === '' ? '/' : this.state.currentPath}</h3>
                {/*<button onClick={this.syncDbAndFilesystem} className={'btn btn-success'}>Sync db and filesystem</button>*/}
                <ActionButtons
                    getMedia={this.getMedia}
                    setMedia={this.setMedia}
                    getCurrentPath={this.getCurrentPath}
                />
                <ul className="list-group files-list">
                    {this.state.currentPath !== '' && <Directory openDir={this.openParentDir} media={{path_name: '..'}}/>}
                    {this.state.media.map(media => {
                        if (media['is_dir']) {
                            return <Directory
                                getCurrentPath={this.getCurrentPath}
                                openDir={this.openDir}
                                media={media}
                                getMedia={this.getMedia}
                                setMedia={this.setMedia}
                            />
                        }

                        if (media['is_image']) {
                            return <Image
                                getMedia={this.getMedia}
                                setMedia={this.setMedia}
                                getCurrentPath={this.getCurrentPath}
                                media={media}
                                browseDataTarget={this.props.browseDataTarget}
                            />
                        }
                    })}
                </ul>
                <ToastContainer/>
            </>
        )
    }
}

export default MediaStorage;