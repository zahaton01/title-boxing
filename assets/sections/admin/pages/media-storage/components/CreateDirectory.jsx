import React from 'react';
import {errorToast, successToast} from "../../../../../components/toast/toast";
import adminClient from "../../../../../infrastructure/api/client/adminClient";

class CreateDirectory extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            onCreate: false,
            name: ''
        }
    }

    toggleCreate = () => {
        const status = !this.state.onCreate;
        status ? this.props.setActive('create-directory') : this.props.setActive(null);

        this.setState({onCreate: status, name: ''});
    }

    handleNameChange = (e) => {
        this.setState({name: e.target.value});
    }

    createDirectory = () => {
        adminClient().post(`/media-storage/create-directory`, {current_path: this.props.getCurrentPath(), name: this.state.name})
            .then(res => {
                this.props.setMedia([res.data, ...this.props.getMedia()]);
                successToast('Success');
                this.toggleCreate();
            })
            .catch(err => {
                errorToast(err.response.data.msg);
            })
    }

    render() {
        return (
            <>
                {!this.state.onCreate &&
                <button className={'btn btn-light'} onClick={this.toggleCreate}>Create directory</button>}
                {this.state.onCreate &&
                <>
                    <h6>Name of the directory</h6>
                    <input onChange={this.handleNameChange} className={'form-control'} name={'name'}/>
                    <button style={{'margin-top': '25px'}} className={'btn btn-light'} onClick={this.createDirectory}>Save</button>
                    <button style={{'margin-top': '25px'}} className={'btn btn-danger'} onClick={this.toggleCreate}>Cancel</button>
                </>
                }
            </>
        );
    }
}

export default CreateDirectory;