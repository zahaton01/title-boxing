import CreateDirectory from "./CreateDirectory";
import UploadFile from "./UploadFile";
import React from "react";

class ActionButtons extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeButton: null
        }
    }

    setActiveButton = (button) => {
        this.setState({activeButton: button})
    }

    render() {
        return (
            <div className={'action-buttons'}>
                {(this.state.activeButton === null || this.state.activeButton === 'create-directory') &&
                    <CreateDirectory
                        setActive={this.setActiveButton}
                        getCurrentPath={this.props.getCurrentPath}
                        getMedia={this.props.getMedia}
                        setMedia={this.props.setMedia}
                    />
                }
                {(this.state.activeButton === null || this.state.activeButton === 'upload-file') &&
                    <UploadFile
                        setActive={this.setActiveButton}
                        getCurrentPath={this.props.getCurrentPath}
                        getMedia={this.props.getMedia}
                        setMedia={this.props.setMedia}
                    />
                }
            </div>
        )
    }
}

export default ActionButtons;