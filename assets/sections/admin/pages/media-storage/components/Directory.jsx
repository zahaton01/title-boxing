import React from 'react';
import {FaFolderOpen, MdDelete} from "react-icons/all";
import {errorToast, successToast} from "../../../../../components/toast/toast";
import adminClient from "../../../../../infrastructure/api/client/adminClient";

class Directory extends React.Component {
    openDir = () => {
        this.props.openDir(this.props.media['path_name']);
    }

    delete = () => {
        adminClient().patch('/media-storage/remove-directory', {current_path: this.props.getCurrentPath(), name: this.props.media['path_name']})
            .then(() => {
                const media = this.props.getMedia();

                media.forEach((item, index) => {
                    if (item['is_dir'] && item['path_name'] === this.props.media['path_name']) {
                        media.splice(index, 1);
                    }
                })

                this.props.setMedia(media);

                successToast('Success');
            })
            .catch(err => {
                errorToast(err.response.data.msg);
            });
    }

    render() {
        return (
            <li className="list-group-item directory">
                <button className={'btn btn-sm btn-link'} onClick={this.openDir}>
                    <FaFolderOpen/>
                    <span className={'directory-title'}>{this.props.media['path_name']}</span>
                </button>
                {this.props.media['path_name'] !== '..' &&
                <button onClick={this.delete} className={'btn btn-light delete-button'}>
                    <MdDelete/>
                </button>
                }
            </li>
        )
    }
}

export default Directory;