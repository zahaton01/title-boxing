import './assets/styles/css/bootstrap.min.css';
import './assets/styles/css/default-css.css';
import './assets/styles/css/font-awesome.min.css';
import './assets/styles/css/metisMenu.css';
import './assets/styles/css/responsive.css';
import './assets/styles/css/styles.css';
import './assets/styles/css/themify-icons.css';
import './assets/styles/css/typography.css';
import './assets/styles/css/custom.css';
import 'selectize/dist/js/selectize';
import 'selectize/dist/css/selectize.bootstrap3.css';
import 'react-toastify/dist/ReactToastify.css';

window.$ = require('jquery');

import './assets/js/bootstrap.min'
import './assets/js/popper.min'
import './assets/js/metisMenu.min';
import './assets/js/scripts';
import 'jquery-ui-dist/jquery-ui.min.css';
import 'jquery-ui-dist/jquery-ui.min';
import 'jquery-ui-dist/jquery-ui.theme.min.css';
import ReactDOM from "react-dom";
import ToastContainerResolver from "../../components/toast/ToastContainerResolver";
import React from "react";

if ($('#filters-start-date').length) {
    $('#filters-start-date').datepicker({
        changeYear: true,
        dateFormat: 'dd-mm-yy'
    });
}

if ($('#filters-end-date').length) {
    $('#filters-end-date').datepicker({
        changeYear: true,
        dateFormat: 'dd-mm-yy'
    });
}

$(document).ready(function () {
    $('.js-delete-modal').each(function () {
        $(this).on('click', function () {
            $('#delete-modal').modal('show');
            $('#delete-modal-link').attr('href', $(this).attr('href'));
            return false;
        });
    });

    $('.js-selectize').each(function () {
        $(this).selectize({
            sortField: 'text',
            allowEmptyOption: true
        });
    });

    $('.js-file-selectize').each(function () {
        $(this).selectize({
            sortField: 'text',
            render: {
                item: function(item) {
                    return `<div><img width="48px" height="48px" src="${item.text}" /><span style="margin-left: 6px">${item.text}</span></div>`;
                },
                option: function(item) {
                    return `<div style="height: 48px;display: flex;align-items: center"><img loading="lazy" width="48px" height="48px" src="${item.text}" /><span style="margin-left: 6px">${item.text}</span></div>`;
                }
            },
        });
    });

    $('.js-multiple-selectize').each(function () {
        $(this).selectize({
            sortField: 'text',
        });
    });
});

ReactDOM.render(
    <ToastContainerResolver />,
    document.getElementById('toast-container'),
);
