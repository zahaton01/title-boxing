import axios from 'axios'

const siteClient = () => {
    return axios.create({
        baseURL: `/${APP_LOCALE}`,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });
}

export default siteClient;