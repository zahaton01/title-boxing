import axios from 'axios'

const adminClient = () => {
    return axios.create({
        baseURL: `/${APP_LOCALE}/admin`,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });
}

export default adminClient;